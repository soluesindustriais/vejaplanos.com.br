<section class="mb-5"
    style="background:url(assets/img/banner-section-2.jpg) no-repeat center center fixed; background-size: cover;">
    <div class="my-4 py-5 w-100 h-100 section-2-bg">
        <div class="container py-4">
            <div class="row">
                <div class="col-12 text-center lazy">
                    <h2 class="mt-4 mb-3 text-uppercase text-white h1 " style="text-shadow: 2px 2px 3px #000000;">
                        O sonho do sorriso perfeito na palma das suas mãos
                    </h2>
                    <p class="text-white " style="font-size:20px;text-shadow: 1px 1px 2px #000000;">Para quem busca um
                        plano odontológico a Ideal Odonto lançou 3 (três) opções de planos para atender a todos os
                        públicos e necessidades.
                    </p>
                    <a class="btn btn-lg button-slider mt-2 " href="<?= $url ?>informacoes">Saiba mais</a>
                </div>
            </div>
        </div>
    </div>
</section>