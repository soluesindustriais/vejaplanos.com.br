<div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active one">
            <div class="d-flex justify-content-center align-items-center banner-bg" style="width:100%;height:100%;">
                <div class="col-md-10 col-12 m-auto text-center">
                    <h2>ODONTO KIDS</h2>
                    <p>Cuidado com a saúde bucal desde a infância</p>
                    <a href="<?= $url; ?>odonto-kids" class="btn btn-lg button-slider mt-2">Saiba mais</a>
                </div>
            </div>
        </div>
        <div class="carousel-item two">
            <div class="d-flex justify-content-center align-items-center banner-bg" style="width:100%;height:100%;">
                <div class="col-md-10 col-12 m-auto text-center">
                    <h2>ODONTO QUALITY</h2>
                    <p>Qualidade e segurança para sorrir</p>
                    <a href="<?= $url; ?>odonto-quality" class="btn btn-lg button-slider mt-2">Saiba mais</a>
                </div>
            </div>
        </div>
        <div class="carousel-item three">
            <div class="d-flex justify-content-center align-items-center banner-bg" style="width:100%;height:100%;">
                <div class="col-md-10 col-12 m-auto text-center">
                    <h2>ODONTO ORTO</h2>
                    <p>Se você precisa iniciar um tratamento ortodôntico, então é Odonto Orto</p>
                    <a href="<?= $url; ?>odonto-orto" class="btn btn-lg button-slider mt-2">Saiba mais</a>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>