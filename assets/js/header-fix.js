
           $(document).ready(function(){
               var infoHeight = $('.info-header').outerHeight();
               $('header').after('<div id="header-block"></div>');
               $('#header-block').css('height', infoHeight + 'px');
               $('#header-block').hide();
               if($(window).width() > 768){
                   $(window).scroll(function(){
                       if ($(window).scrollTop() >= infoHeight) {
                           $('header').addClass('sticky-top');
                           $('.info-header').hide();
                           $('#header-block').show();
                           
                       }
                       else {
                           $('header').removeClass('sticky-top');
                           $('.info-header').show();
                           $('#header-block').hide();
                       }
                   });
               }
           });
    $(window).scroll(function() {
    if ($(this).scrollTop() > $(document).height() - $(window).height() - 50) {
        $('#scroll-up').fadeIn();
    } else {
        $('#scroll-up').fadeOut();
    }
});