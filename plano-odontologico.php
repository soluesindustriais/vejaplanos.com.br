<?php
include('inc/vetKey.php');
$h1 = "plano odontológico";
$title = $h1;
$desc = "O plano odontológico é a melhor maneira de cuidar da saúde bucal com eficiência Até 2050, a população da terceira idade irá triplicar no Brasil. É o";
$key = "plano,odontológico";
$legendaImagem = "Foto ilustrativa de plano odontológico";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>O plano odontológico é a melhor maneira de cuidar da saúde bucal com eficiência</h2>
                <p>Até 2050, a população da terceira idade irá triplicar no Brasil. É o que mostra um levantamento
                    realizado pelo Instituto Brasileiro de Geografia e Estatística (IBGE). Segundo os dados apurados,
                    passará dos 19,6 milhões - registrados em 2010 - para 66,5 milhões de indivíduos, o equivalente a
                    29,3% da população. Ainda, conforme o IBGE, em 2030 o número de pessoas da terceira idade, com mais
                    de 60 anos, irá ultrapassar o de crianças, de zero a 13 anos.</p>
                <p>A elevação na população da terceira idade deixa evidente que os indivíduos estão vivendo mais,
                    contudo, ao contrário do que acontecia anos atrás, hoje em dia esses senhores e senhoras querem
                    viver até os 100 anos com qualidade de vida. A terceira idade dos tempos modernos tem levado muito a
                    sério a expressão "melhor idade" e investe cada vez mais em procedimentos de saúde bucal. Por isso,
                    até quem já passou dos 60 sabe que vale a pena investir em um plano odontológico.</p>
                <p>A elevação na expectativa de vida das pessoas impulsionou o setor de convênios dentários voltados
                    exclusivamente à terceira idade. Prova disso é o aumento no número de pacientes de plano
                    odontológico entre indivíduos com 59 anos ou mais. Segundo informações do Sindicato Nacional das
                    Empresas de Odontologia de Grupo (Sinog), de junho de 2017 a junho de 2018, o setor teve um
                    acréscimo de 17% nas adesões nesta faixa etária.</p>

                <p>O total de usuários de planos dentais no país gira em torno de 23 milhões de indivíduos e a
                    expectativa é que até 2020 a busca eleva consideravelmente. Sem dúvida, uma das faixas etárias
                    responsáveis pelo cenário positivo é a turma acima dos 60.</p>
                <h2>Vantagens do plano odontológico na velhice</h2>
                <p>Assim como ocorre na medicina, na odontologia também existem diferentes especialistas responsáveis
                    pela assistência de públicos específicos. No caso das crianças, a especialidade é a odontopediatria.
                    Já na melhor idade, o profissional é chamado de odontogeriatra. Este especialista cuida dos usuários
                    idosos, priorizando sua saúde bucal e qualidade de vida.</p>
                <p>Infelizmente, muitos senhores e senhoras chegam à melhor idade com as dentições bastante surradas.
                    Alguns, nem tem mais a arcada dentária completa e fazem utilização de próteses e implantes. No
                    entanto, ainda assim é muito importante investir em plano odontológico. Os profissionais assistirão
                    os idosos para que tenham uma melhora considerável na aparência do sorriso e na sua funcionalidade.
                </p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>Por outro lado, se o idoso cuidou muito bem do sorriso desde a infância e manteve os procedimentos na
                    fase adulta, com certeza chegou aos 60 com a saúde oral preservada. Até mesmo para estes o plano
                    odontológico é de suma essencialidade. Graças à rotina de consultas, o profissional poderá fazer o
                    acompanhamento, fazendo a prevenção, além de métodos paliativos e curativos.</p>
                <p>O foco da odontogeriatria é garantir que problemas orais não interfiram na saúde durante a velhice.
                    Nos idosos, as patologias mais populares são artrite, osteoporose, artrose, problemas vasculares,
                    diabetes e enfermidades endócrinas. Todas essas doenças sistêmicas podem ser oriundas de problemas
                    com o sorriso. Com acesso ao plano odontológico, os velhinhos mantêm uma frequência maior aos
                    consultórios e, diante de qualquer sinal, o dentista já inicia o tratamento evitando que a evolução
                    para casos mais graves.</p>
                <ul>
                    <li>
                        <p>estabelecem relação de confiança com o dentista;</p>
                    </li>
                    <li>
                        <p>a manutenção da saúde bucal interfere no bem-estar desta faixa etária.</p>
                    </li>
                    <li>
                        <p>na terceira idade, é preciso contar com apoio de um profissional especializado;</p>
                    </li>
                    <li>
                        <p>idosos necessitam de cuidados diferenciados da primeira consulta aos demais procedimentos.
                        </p>
                    </li>
                </ul>
                <h2>Como solucionar problemas bucais com plano odontológico</h2>
                <p><a href="<?= $url ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i++; ?>.jpg" data-fancybox="group1"
                        class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img
                            src="<?= $url; ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i - 1; ?>.jpg"
                            class="galeria centro" alt="<?= $h1; ?>"></a>Há um tempo atrás, a porcentagem de indivíduos
                    que completava 60 anos ou mais era considerado
                    pequeno. Boa parte da população falecia antes ou chegava à terceira idade muito debilitado. Nos dias
                    de hoje não é mais assim. Senhores e senhoras de 60 anos aparentam ter 50 e estão correndo atrás da
                    melhor forma física e da saúde. Para este público, é necessário envelhecer com autoestima e
                    qualidade de vida, e por que não, com um belo sorriso no rosto.</p>
                <p>Mas apesar dos cuidados e a vontade de viver mais e melhor, muitos indivíduos idosos se deparam com
                    problemas orais e necessitam encarar de frente e sem medo os tratamentos. O investimento em plano
                    odontológico tem este papel na vida dos idosos: recuperar a saúde bucal e apoiar na manutenção do
                    sorriso.</p>
                <p>Um dos empecilhos que mais aflige os idosos é a perda das dentições - vale indicar que nem sempre
                    este acontecimento está relacionado a fatores naturais. Às vezes, faltou higiene, escovação e
                    visitas periódicas ao dentista nos anos anteriores, ou mesmo, é um problema que iniciou por causa de
                    maus hábitos criados na terceira idade. Seja como for, o acompanhamento de um plano odontológico irá
                    diagnosticar o problema, que ocorre com mais gravidade nesta faixa etária devido a menor destreza
                    muscular e, consequentemente, higienização feita de forma inadequada. O resultado é maior incidência
                    de cáries e periodontite, que causam a perda dos dentes na velhice.</p>
                <p>Outro problema diagnosticado em grande quantidade pelos odontogeriatras é a fragilidade da língua.
                    Nos idosos, este órgão tem relevância nas tarefas mais simples, como mastigação e fala. Mas, a falta
                    de tonicidade compromete a qualidade de vida. Acima dos 60 também é comum que haja a redução de
                    saliva, gerando desgaste no esmalte dos dentes, problemas com mau hálito e dificuldade na
                    alimentação.</p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>Outro ponto que atrapalha a vida dos idosos é a perda do paladar. Com o passar dos anos, a
                    sensibilidade gustativa fica bem comprometida. Por isso, na terceira idade é comum o exagero na
                    quantidade de certas comidas, como temperos, açúcar e sal. Este consumo acima do normal prejudica a
                    saúde dos dentes e da boca.</p>
                <h2>Dentinas mais saudáveis e bonitas na terceira idade: é possível!</h2>
                <p>Para manter a saúde da dentição na velhice é essencial adotar costumes de limpeza e frequentar
                    assiduamente o odontologista. As consultas devem acontecer de seis em seis meses, ou sempre que o
                    especialista solicitar, dependendo do tratamento. Neste período da vida, vale muito a pena contratar
                    um plano odontológico, principalmente pelas coberturas diferenciadas.</p>
                <p>Exibir um sorriso sempre bonito e saudável na velhice é perfeitamente possível. Além de contar com um
                    plano odontológico e com apoio do profissional, o paciente precisa fazer sua parte. Na velhice, a
                    recomendação é o uso de escovas dentais elétricas. Por causa da falta de mobilidade, os idosos
                    sentem dificuldade na escovação, e essas escovas elétricas auxiliam na tarefa diária.</p>
                <p>A maioria das pessoas com mais de 60 anos possui implantes dentários ou próteses. Os cuidados com a
                    limpeza nestes casos precisam de atenção redobrada. O acúmulo de comidas continua a acontecer na
                    boca, independente de ser dente permanente ou não, sendo assim, a limpeza após as refeições, com fio
                    dental e escovação é fundamental. Os idosos também pode fazer uso de enxaguantes bucais com flúor,
                    que auxiliam na remoção dos resíduos.</p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>O uso de dentaduras é outro dilema na velhice. Isso porque, qualquer descuido com a limpeza pode
                    acarretar problemas graves e comprometer a saúde oral. Para garantir a qualidade de vida do idoso e
                    a durabilidade da dentadura, a limpeza do aparelho necessita ser caprichada. Do mesmo jeito que se
                    faz com as dentições naturais, a dentadura requer cuidados com a escovação. Geralmente, o dentista
                    passa aos pacientes o passo a passo para manter a limpeza e eliminar quaisquer resíduos.</p>
                <h2>Plano odontológico: investimento para todas as faixas etárias</h2>
                <p><a href="<?= $url ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i++; ?>.jpg" data-fancybox="group1"
                        class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img
                            src="<?= $url; ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i - 1; ?>.jpg"
                            class="galeria centro" alt="<?= $h1; ?>"></a>A saúde bucal está diretamente conectada à
                    qualidade de vida dos indivíduos e, quanto antes os
                    cuidados começarem, melhor serão os resultados colhidos ao longo da vida. O plano odontológico é uma
                    das táticas mais eficientes para manter a rotina de consultas ao dentista, por isso, o número de
                    pacientes tem elevado no país.</p>
                <p>Contudo, apesar dos bons índices do âmbito, muitos indivíduos não contratam o trabalho de assistência
                    odontológico por acreditarem que custa caro. O que não passa de um equívoco. Geralmente, acontece o
                    contrário: o atendimento dentário poderá pesar no orçamento quando o paciente menos esperar. Casos
                    de urgência e emergência em dentistas particulares costuma ter um alto custo agregado. Por isso,
                    vale muito a pena, em médio e longo prazo, investir na contratação do serviço e usufruir dos
                    benefícios.</p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>Saiba quais são as principais coberturas de um plano odontológico:</p>
                <ul>
                    <li>
                        <p>endodontia (tratamento de canal);</p>
                    </li>
                    <li>
                        <p>tratamento de cáries;</p>
                    </li>
                    <li>
                        <p>restaurações;</p>
                    </li>
                    <li>
                        <p>radiografias;</p>
                    </li>
                    <li>
                        <p>remoção de tártaro e placa bacteriana;</p>
                    </li>
                    <li>
                        <p>tratamento de gengiva;</p>
                    </li>
                    <li>
                        <p>cirurgias;</p>
                    </li>
                    <li>
                        <p>prevenção e limpeza.</p>
                    </li>
                </ul>
                <h2>Por que o plano odontológico é essencial?</h2>
                <p><a href="<?= $url ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i++; ?>.jpg" data-fancybox="group1"
                        class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img
                            src="<?= $url; ?>assets/img/img-mpi/<?= $urlPagina ?>-<?= $i - 1; ?>.jpg"
                            class="galeria centro" alt="<?= $h1; ?>"></a>Cerca de vinte e três milhões de brasileiros
                    possuem plano odontológico. A adesão ocorre
                    primordialmente pela praticidade e segurança do trabalho. Nos convênios dentários, os pacientes têm
                    acesso a profissionais especializados. A consulta pode ser agendada pela internet e, normalmente, o
                    usuário tem a alternativa de escolher entre centenas de dentistas credenciados à rede. Além do mais,
                    o plano odontológico garante atendimento rápido e eficaz, oferecendo coberturas diferenciadas para
                    atender públicos específicos. </p>
                <p>Mesmo com tantas vantagens, é fundamental pesquisar entre as operadoras a que mais se encaixa no
                    perfil desejado. Para os pacientes, o grande benefício do plano é a manutenção dos cuidados com o
                    sorriso. Ao contrário dos usuários que escolhem pelo atendimento particular, os que possuem plano
                    odontológico frequentam muito mais os consultórios dentários. Quem tem convênio paga uma
                    mensalidade, que pode ser mensal ou anual, para ter acesso a consultas e avaliações periódicas. Essa
                    frequência faz com que o odontologista perceba problemas ainda no começo, evitando doenças mais
                    sérias. </p>
                <p>Outro ponto positivo é a relação de confiança com o odontologista. Quanto mais consultas, mais
                    proximidade com o profissional e menos resistência aos tratamentos. Mas claro que apenas plano
                    odontológico não faz milagres. Para a saúde bucal ser completa, o paciente tem que fazer sua parte.
                    Em casa, os cuidados devem ser mantidos à risca. A escovação correta dos dentes (três vezes ao dia
                    após as refeições), o uso adequado do fio dental e alimentação balanceada são relevantes para
                    garantir uma dentição forte e bonita.</p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>Fique atento às principais doenças bucais:</p>
                <ul>
                    <li>
                        <p>cáries;</p>
                    </li>
                    <li>
                        <p>gengivite;</p>
                    </li>
                    <li>
                        <p>desgaste dentário;</p>
                    </li>
                    <li>
                        <p>retração gengival;</p>
                    </li>
                    <li>
                        <p>endodontia (infecção do canal);</p>
                    </li>
                    <li>
                        <p>problemas na articulação;</p>
                    </li>
                    <li>
                        <p>bruxismo;</p>
                    </li>
                    <li>
                        <p>mau hálito (halitose).</p>
                    </li>
                </ul>
                <p>Para não errar na contratação do serviço, escolha o plano odontológico ideal para você. No mercado
                    estão disponíveis os planos individuais e empresarial. A categoria para as operadoras podem ser
                    adquiridas a partir de dois usuários. A maioria das empresas atua com custos competitivos, carências
                    reduzidas e até planos sem carência. Outro bônus é a cobertura especial e o atendimento nacional,
                    fornecido por quase todas as operadoras.</p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>Já os planos individuais e familiares contam com uma grande rede de especialistas credenciados, nas
                    principais cidades do Brasil. As coberturas também são muito completas, e a contratação do plano
                    odontológico pode ser realizada sem carência, com abatimento no cartão de crédito ou boleto anual.
                </p>
                <h2>Sorrir faz bem à saúde mental de todos!</h2>
                <p>A frase é batida e com certeza você já deve ter escutado em algum momento que um belo sorriso é o
                    principal cartão de visitas dos indivíduos. E é mesmo. Pouco adianta estar bem-vestido ou com os
                    cabelos arrumados, se as dentições estão feios e mal cuidados. A saúde bucal tem função primordial
                    na manutenção do bem-estar dos indivíduos. Ter dentes brancos e saudáveis auxiliam homens e mulheres
                    a sentirem mais felizes. </p>
                <p>O sorriso é dos métodos responsáveis pela autoestima, melhorando as relações profissionais e
                    pessoais. Informações da Organização Mundial da Saúde (OMS) revelam que ao sorrir o vínculo social
                    se instala, e nos ambientes de trabalho, ter uma relação de confiança com os colegas está
                    diretamente atrelado a seu estado de humor. No entanto, o que impede muitos indivíduos de sorrirem é
                    a imperfeição dentária. Homens e mulheres relataram durante um estudo que deixam de sorrir, seja no
                    emprego ou em uma roda de amigos, por causa da vergonha de mostrar os dentes. </p>
                <p>A ausência de um sorriso saudável afasta as pessoas do convívio social, pelo medo do constrangimento
                    e piadas desagradáveis. Dentes tortos, amarelados e doentes são também desencadeadores da depressão.
                    Mas é possível transformar essa realidade. O primeiro passo é querer. Depois, o indicado é investir
                    em plano odontológico, assim a pessoa poderá recuperar o tempo perdido, retornando ao consultório
                    para fazer as avaliações importantes e começar os procedimentos corretivos. A reabilitação bucal irá
                    recuperar o sorriso, devolvendo ao paciente a vontade de sorrir novamente. O medo e a vergonha darão
                    lugar a sentimentos de orgulho e realização.</p>
                <h2>Custo-benefício e outros benefícios que o plano odontológico oferece</h2>
                <p>Ao contratar o investimento no plano odontológico, independente de qual seja a operadora, os
                    indivíduos já saem ganhando muito economia. Mesmo pagando um custo mensal, que varia conforme o tipo
                    do plano e de suas coberturas, a redução nos gastos é certa. Outro ponto positivo ao aderir o
                    convênio é o aumento da frequência entre uma consulta e outra. O dentista passa a ser um companheiro
                    e a rotina de atendimentos vira algo natural na vida dos beneficiários. Mais cuidado com a saúde
                    oral só pode desenvolver um sorriso mais bonito e mais dinheiro no bolso. </p>
                <p>Sem a necessidade de procedimentos invasivos, os pacientes economizam os recursos que gastariam no
                    atendimento de emergência (bem mais caro que o tradicional) para curtir a vida e passear com a
                    família. Ou então, investem naquele curso de especialização para melhorar o currículo profissional.
                </p>
                <h2>O custo alto de não cuidar do sorriso</h2>
                <p>Pouco se escuta falar de pessoas que morrem por falta de atendimento dentário. A maioria dos casos
                    extremos, que leva o usuário a óbito, se encontram na medicina. No entanto, apesar da baixa
                    incidência, a morte por empecilhos orais pode acontecer. Quando uma pessoa ignora as infecções
                    dentais, a gravidade da cárie, por exemplo, pode afetar a polpa ou nervo do dente, localizado na
                    parte central da saliência. </p>
                <p>Este problema evolui para um processo infeccioso grave, gerando contaminação que pode se alastrar
                    para outros órgãos do corpo humano. Sem o atendimento de um odontologista, o canal infeccionado pode
                    gerar um abcesso. O resultado é uma infecção que progride pela rede sanguínea trazendo malefícios
                    gravíssimos, que podem sim levar o paciente à morte. Outras doenças bucais, como periodontite
                    (sangramento da gengiva) podem desencadear diabetes e problemas cardíacos. </p>
                <p>Agora, pare e pense: vale mesmo a pena correr este risco? Com costumes básicos de limpeza, que devem
                    começar ainda na infância, é possível manter uma boca saudável. Além disso, com o avanço da
                    odontologia e a criação de várias especialidades, ficou muito mais fácil garantir um sorriso lindo e
                    bonito. Para melhorar ainda mais este cenário, surgiram os convênios dentários. O principal objetivo
                    do plano odontológico é preservar a saúde bucal dos beneficiários.</p>
                <p>O atendimento personalizado, com dentistas credenciados, dá a garantia de rapidez nos tratamentos.
                    Além do mais, a concorrência do setor é positiva, porque obriga que as empresas fiquem atentas à
                    qualidade dos tratamentos. Hoje em dia, muito se fala de saúde bucal: nas escolas, as crianças
                    recebem aplicação de flúor e aprendem lições importantes sobre os cuidados com os dentes. Depois, em
                    casa, junto das famílias, é hora de aplicar todo o conhecimento na prática.</p>
                <p>Na mídia, a temática está sempre em pauta, apontando ao público em geral a essencialidade de manter a
                    limpeza e a escovação. Um dos melhores modos de aplicar os cuidados no dia a dia com naturalidade é
                    fortalecer os costumes. Assim como acordamos e já pensamos no que vamos comer no café da manhã, na
                    roupa que vamos colocar para ir ao trabalho ou no ritual de beleza e maquiagem, também temos que
                    incluir a proteção com a dentição. Gaste um tempo para tratar do seu sorriso. Opte por cremes
                    dentais de qualidade, prefira escovas com cerdas extra macias e faça a escovação sem pressa. Este
                    também é seu momento.</p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>Confira como escovar os dentes corretamente:</p>
                <ul>
                    <li>
                        <p>depois, é hora de escovar as superfícies da mastigação;</p>
                    </li>
                    <li>
                        <p>na sequência, faça os mesmos movimentos com os dentes inferiores;</p>
                    </li>
                    <li>
                        <p>escove as superfícies da bochecha dos dentes superiores;</p>
                    </li>
                    <li>
                        <p>para finalizar escove a língua retirando as bactérias que ficam alojadas.</p>
                    </li>
                </ul>
                <p>Mas a limpeza e cuidados com o sorriso não param por aí. Antes da escovação é indicado passar o fio
                    dental. Não exagere na força, passe o fio com delicadeza, evitando ferimentos na gengiva. Muitos
                    indivíduos também fazem uso dos enxaguantes bucais; estes produtos melhoram a resistência das
                    dentinas, deixam o costume mais fresco e ajudam na eliminação dos resíduos.</p>
                <p>Nesta gama de cuidados com os dentes, não pode ficar de fora o investimento em plano odontológico.
                    Este é talvez, um dos recursos que vão fortalecer de maneira mais eficiente a manutenção do sorriso.
                    Com acesso ao plano, os pacientes mantêm uma rotina de consultas que, em médio e longo prazo trarão
                    economia e muita qualidade de vida. Lógico, que um sorriso saudável não é construído apenas com
                    plano odontológico. Como mencionamos, este é um caminho para ajudar as pessoas a se sentirem seguras
                    e felizes para exibir saúde bucal e ainda inspirarem todos a sua volta.</p>
                <p>E o melhor: não há tempo certo para ter um plano odontológico. Os benefícios se encaixam nas diversas
                    idades, disponibilizando vantagens que se adequam à realidade de cada idade e gênero, ou seja, nunca
                    é tarde para melhorar um sorriso. O plano odontológico irá auxiliar neste processo, trazendo ao
                    consultório pessoas que se afastaram dos dentistas por um motivo ou outro. Aos poucos, as crenças
                    que aprendemos desde criança de que "dentista é muito caro" ou "tenho medo do tratamento" deixam de
                    existir.</p>
                <p>Fortalecer o vínculo entre odontologista e beneficiário é uma das metas do plano odontológico. Com o
                    tempo, a cadeira do dentista se transforma até em divã, e todos os receios dão lugar à confiança e
                    amizade. Cuidar dos dentes deve ser um prazer e essa prática só é adquirida com exercício diário. A
                    saúde bucal é fundamental em todas as fases da vida. Na infância, se formam os hábitos que devemos
                    manter na adolescência, quando somos adultos e principalmente na velhice. A boca faz parte do corpo
                    humano e cada detalhe é relevante para manter sua beleza e funcionalidade.</p>
                <p>
                    <!--StartFragment-->
                    <!--EndFragment-->
                </p>
                <p>Não deixe para amanhã: saúde e qualidade de vida são essenciais em qualquer faixa etária. Pesquise as
                    melhores condições de pagamento e qual plano odontológico se enquadra melhor na sua realidade e de
                    sua família. No caso das empresas, também é possível realizar o investimento focando nos convênios
                    empresariais. Uma equipe com saúde rende mais e trabalha com amor.</p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>