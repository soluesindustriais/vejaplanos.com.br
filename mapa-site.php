<?php
$h1 = "Mapa do site";
$title = $h1;
$desc = "O mapa do site com todos os atalhos para todas as páginas deste site. Qualquer dúvida estamos a disposição por email ou telefone. Clicando aqui";
include('inc/head.php');
?>

    <body>
        <?php include 'inc/header.php' ?>
        <main>

            <div class="container mb-5">
                <div class="row">
                    <div class="col-md-12">
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-md-12">
                        <ul class="sitemap">
                            <?php include("inc/menu.php"); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <?php include 'inc/footer.php' ?>
        <script>
            $(document).ready(function() {
                $(function() {                
                    $(".sitemap .dropdown-menu").removeClass("dropdown-menu");
                    $(".sitemap .btn-area-azul").removeClass("btn-area-azul");
                    $(".sitemap .btn-area").removeClass("btn-area");
                });
            });
        </script>
    </body>
    

    </html>
