<footer class="footer-bs p-0" style="background:#333">
    <a href="#topo" class="scrollsuave"><i class="fa fa-chevron-up text-center scrolltop"></i></a>
    <div class="container footer pt-5">
        <div class="row align-items-center">
            <div class="col-md-3 text-md-left text-center">
                <img src="assets/img/logo-branco.png" alt="logo" class="logo-footer mb-2">
            </div>
            <div class="col-md-9">
                <ul class="pages text-center m-0 w-100  mt-3 d-md-flex d-block pl-0">                    
                    <li class="px-3 mr-0"><a title="Home" href="<?=$url?>" style="font-size: 13px;">Home</a></li>
                    <?php
                        $pages = array();
                        $pages[0] = array("title" => "Quem Somos");
                        $pages[1] = array("title" => "Planos");
                        foreach ($pages as $key => $value) {
                            echo "<li class='px-3 mr-0'><a rel=\"nofollow\" title=\"".$value['title']."\" href=\"".$url.titletourl($value['title'])."\" style='font-size: 13px;'>".$value['title']."</a></li>";
                        }
                        echo "<li class=\"px-3 mr-0\"><a rel=\"nofollow\" title=\"Rede Credenciada\" href=\"https://ssl.datanext.com.br/sistema-datasys/mod-app/1032/busca.html?search_type=av\" style=\"font-size: 13px;\" target=\"_blank\">Rede Credenciada</a></li>";
                        $pages[0] = array("title" => "Informações");
                        $pages[1] = array("title" => "Dúvidas");
                        $pages[2] = array("title" => "Contato");
                        
                        foreach ($pages as $key => $value) {
                            echo "<li  class='px-3 mr-0'><a rel=\"nofollow\" title=\"".$value['title']."\" href=\"".$url.titletourl($value['title'])."\"  style='font-size: 13px;'>".$value['title']."</a></li>";
                        }
                    ?>
                    <li class="px-3 mr-0"><a rel="nofollow" title="Código do consumidor" href="https://www.idealodonto.com.br/codigo-defesa-consumidor.pdf" style="font-size: 13px;" target="_blank">Código do consumidor</a></li>
                    <li class="px-3 mr-0"><a rel="nofollow" title="Downloads" href="https://www.idealodonto.com.br/downloads" style="font-size: 13px;" target="_blank">Downloads </a></li>
                    <li class="px-3 mr-0"><a title="Mapa do site" href="<?=$url?>mapa-site" style="font-size: 13px;">Mapa do site</a></li>
                </ul>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-6 col-12">
                <p class="text-little text-center text-md-left">Avenida das Nações Unidas, 18801 - Conjunto 1309<br>Santo Amaro - São Paulo-SP - CEP: 04795-100</p>
                <a class="d-block d-md-none text-center w-100" href="https://www.ans.gov.br/planos-de-saude-e-operadoras/informacoes-e-avaliacoes-de-operadoras/qualificacao-ans" target="_blank"><img src="<?=$url?>assets/img/ans.png" alt="ans-logo"></a>
            </div>            
            <div class="col-md-6 col-12 d-flex logos-social justify-content-md-end justify-content-center">
                <a href="https://www.facebook.com/IdealOdontoOficial/" class="ml-2" target="_blank" rel="nofollow"><img src="<?=$url?>assets/img/social-footer/facebook.png" alt="facebook"></a>
                <a href="https://www.instagram.com/idealodontooficial/" class="ml-2"  target="_blank" rel="nofollow"><img src="<?=$url?>assets/img/social-footer/insta.png" alt="instagram"></a>
                <a href="https://blog.idealodonto.com.br/" class="ml-2"  target="_blank" rel="nofollow"><img src="<?=$url?>assets/img/social-footer/blogger.png" alt="blog"></a>
                <a href="mailto:atendimento@idealodonto.com.br" class="ml-2"  target="_blank" rel="nofollow"><img src="<?=$url?>assets/img/social-footer/email.png" alt="email"></a>
            </div>
        </div>
        <div class="row align-items-center py-2">
            <div class="col-md-6 text-little text-md-left text-center">Copyright @ (Lei 9610 de 19/02/1998)</div>
            <div class="col-md-6 text-md-right text-center">
                <a rel="noopener nofollow" href="https://validator.w3.org/check?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" target="_blank" title="HTML5 W3C" class="col-md-1 col-12"><i class="fab fa-html5"></i>
                    W3C</a>
                <a rel="noopener nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C" class="col-md-1 col-12"><i class="fab fa-css3"></i>
                   W3C</a>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="<?=$url?>assets/js/popper.min.js"></script>
<script src="<?=$url?>assets/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

<?php
    $link = "https://servertemporario.com.br/guardiao/idealodonto/enviaformulario.xml";
    $xml = simplexml_load_file($link);
    foreach($xml as $item){  
        echo "<script>"; 
        echo $item -> mascara;
        echo $item -> validacampos;
        echo $item -> enviaform;
        echo "</script>";
    }
?>
<script>
    var url = window.location;
    $(function() {
        $('header nav ul li a[href="' + url + '"]').addClass('active');
        $('aside a[href="' + url + '"]').addClass('active-menu-aside');
    });
</script>

<?php if ($h1 == "Informações"){?>
<script src="<?=$url?>assets/js/jquery.paginate.js"></script>
<script> $('#example').paginate({ perPage: 20 }); </script>
<?php } ?>
<?php if ($title == "Home") { ?>
<script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $(".owl-carousel").owlCarousel({
            loop: true,
            margin: 0,
            autoplay: true,
            responsive: { 0: { items: 1 }, 769: { items: 4 } }
        });
    });
    function EnviaForm() {                
        $.post('https://www.servertemporario.com.br/guardiao/idealodonto/dialvoxideal.php', {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            pagina: $('#pagina').val(),
            site: $('#site').val(),
            cep: $('#cep').val()
        }, function (resposta) {
            $(function () {
                swal({
                    title: "Tudo certo!",
                    text: "Oi " + $('#nome').val() + ", solicitação recebida com sucesso! Em breve nossa equipe entrará em contato através do telefone 4003-3428. Obrigado!",
                    icon: "success",
                    button: "Ok"
                }).then((value) => {
                    $('#nome').val("");
                    $('#email').val("");
                    $('#telefone').val("");
                    $('#cep').val("");
                });
            });
        });
    }
</script>
<?php } ?>
<?php if(isset($pagInterna) && ($pagInterna = "Informações")){ ?>
<script>
    $(window).scroll(function() { ContatoRolar() });
    $(window).resize(function() { ContatoRolar() });
    $(document).ready(function() { ContatoRolar() });
    var ContatoRolar = function() {
        if (window.innerWidth > 767) {
            var fromTop = $(window).scrollTop();
            var coluna = $('.passo').height() + $('.coberturas').height();
            var banner = $('.banner-titulo').height();
            var alcontato = $('.contato').height();
            if (fromTop < (((coluna - alcontato) + 340))) {
                if (fromTop > 112) {
                    $(".contato").css("position", "absolute");
                    $(".contato").css("width", "94%");
                    $(".contato").css("right", "15px");
                    $(".contato").css("box-shadow", "0 0 5px rgba(0, 0, 0, 0.25)");
                    $(".contato").css("margin-top", (parseInt(fromTop - 250)));
                } else if (fromTop < 112) {
                    $(".contato").css("position", "relative");
                    $(".contato").css("width", "100%");
                    $(".contato").css("right", "0px");
                    $(".contato").css("box-shadow", "0 0 0 rgba(0, 0, 0, 0)");
                    $(".contato").css("margin-top", (parseInt(-112)));
                } else {
                    $(".contato").css("position", "absolute");
                    $(".contato").css("width", "94%");
                    $(".contato").css("right", "15px");
                    $(".contato").css("box-shadow", "0 0 5px rgba(0, 0, 0, 0.25)");
                    $(".contato").css("margin-top", (parseInt(fromTop)));
                }
            }
            if (fromTop > ((coluna + alcontato) - 100)) {
                var documentSize = $(document).height();
                var sizeWindow = $(window).height();
                // var rodape = $(".footer-bs").height() + $('.footer').height() + 35;
                var rodape = $(".footer-bs").height();
                var largura = $('.container').width() + 15;
                $(".chamada").addClass("d-md-block");
                $(".chamada").css("right", "calc((100vw - " + largura + "px) / 2)");
                if (fromTop >= ((documentSize - sizeWindow) - rodape)) {
                    $(".chamada").css("bottom", parseInt(fromTop - ((documentSize - sizeWindow) - rodape)) + "px");
                } else {
                    $(".chamada").css("bottom", "0px");
                }
            } else {
                $(".chamada").removeClass("d-md-block");
            }
        } else {
            $(".contato").css("position", "relative");
            $(".contato").css("width", "100%");
            $(".contato").css("right", "0px");
            $(".contato").css("box-shadow", "0 0 0 rgba(0, 0, 0, 0)");
            $(".contato").css("margin-top", "30px");
        }
    }
</script>
<?php include("inc/fancy.php"); ?>
<?php } ?>
<script>
    $('.scrollsuave').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            targetOffset = $(id).offset().top;
        $('html, body').animate({
            scrollTop: targetOffset - 300
        }, 600);
    });
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=<?= $idAnalytics; ?>"></script>
<script>
    (function(i,s,o,g,r,a,m){
        i['GoogleAnalyticsObject']=r;
        i[r]=i[r] || function(){(
            i[r].q=i[r].q||[]).push(arguments)},
            i[r].l=1*new Date();
        a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];
        a.async=1;
        a.src=g;
        m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', '<?= $idAnalytics; ?>', 'auto');
    ga('create', 'UA-139111261-1', 'auto', 'clientTracker');
    ga('send', 'pageview');
    ga('clientTracker.send', 'pageview');
</script>