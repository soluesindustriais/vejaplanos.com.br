   <div class="modal fade" id="form-modal">
       <div class="modal-dialog modal-dialog-centered">
           <div class="modal-content" style="background:transparent;border:0px;">
               <div class="modal-body p-0">


                   <div class="contato-modal">
                       <button type="button" class="close" data-dismiss="modal"
                           style="color: #fff;opacity: 1;margin-right:7px">&times;</button>
                       <?php $link = "https://servertemporario.com.br/guardiao/idealodonto/formulario.xml";

                        $xml = simplexml_load_file($link);
                        foreach ($xml as $item) {  ?>
                       <div class="form-topo">
                           <h4><?= $item->titulo; ?></h4>
                           <h3><?= $item->telefone; ?></h3>
                       </div>

                       <div class="formulario">
                           <?= str_replace("#h1#", $h1, $item->campos); ?>
                       </div>
                       <?php } ?>
                   </div>

               </div>
           </div>
       </div>
   </div>