<div class="banner-titulo odonto-quality">
    <div class="h-100 w-100 d-flex align-items-center">
        <div class="container align-items-center">
            <div class="row">

                <div class="col-md-4 d-none d-md-block">
                    <img src="<?= $url ?>assets/img/logotest.png" alt="logo ideal odonto">
                    <h2 class="mt-3 font-weight-bold">O plano odontológico ideal para você</h2>
                    <p style="font-weight: 500;">Seu sorriso merece o melhor plano odontológico do Brasil. Tenha um
                        plano odontológico, tenha o plano Ideal Odonto.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-7">
            <div class="row passo">

                <?php $link = "https://servertemporario.com.br/guardiao/idealodonto/planos-mpi.xml";
                $xml = simplexml_load_file($link);
                foreach ($xml as $item) { ?>

                <div class="col-6 mt-5">
                    <h2 class="titulo"><?= $item->nome; ?></h2>
                    <div class="wrap">
                        <p class="subtitulo">Mensal</p>
                        <p class="valor">R$ <span><?= $item->preco; ?></span></p>
                        <?php if ($item->anual != "") { ?>
                        <p class="subtitulo">Anual com desconto</p>
                        <p class="valor2">R$ <span><?= $item->anual ?></span></p>
                        <?php } else { ?>
                        <p class="obs"><?= $item->desc; ?></p>
                        <?php } ?>
                    </div>
                    <p class="obs"><?= $item->carencia; ?></p>
                </div>
                <?php } ?>


            </div>
        </div>

        <div class="col-12 col-md-5">
            <div class="contato">
                <?php $link = "https://servertemporario.com.br/guardiao/idealodonto/formulario.xml";

                $xml = simplexml_load_file($link);
                foreach ($xml as $item) {  ?>
                <div class="form-topo">
                    <h4>
                        <?= $item->titulo; ?>
                    </h4>
                    <h3>
                        <?= $item->telefone; ?>
                    </h3>
                </div>

                <div class="formulario">
                    <?= str_replace("#h1#", $h1, $item->campos); ?>

                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="w-100 mt-5 py-5" style="background:#E7E7E7">
    <div class="container">
        <div class="row coberturas">
            <div class="col-md-7 col-12">
                <div class="row">
                    <div class="col-12 d-flex  align-items-center mb-3">
                        <img src="<?= $url ?>assets/img/lp-mpi/icone.png" alt="Coberturas">
                        <h2 class="titulo-plano txt-laranja">Coberturas</h2>
                    </div>
                    <?php $link = "https://servertemporario.com.br/guardiao/idealodonto/cobertura-mpi.xml";
                    $xml = simplexml_load_file($link);
                    foreach ($xml as $item) {
                        echo $item->desc;
                    } ?>
                </div>
            </div>
        </div>

    </div>
</div>

<a href="#formulario"
    class="scrollsuave fixed-bottom d-block d-md-none btn-mpi rounded-0 text-center text-white">RECEBER LIGAÇÃO</a>
<div class="fixed-bottom d-none chamada p-3 col-md-3"
    style="left:auto;background:#E7E7E7;box-shadow: rgba(0, 0, 0, 0.25) 0px 0px 5px">
    <div class="row align-items-center">
        <div class="col-4"><img src="<?= $url ?>assets/img/img-mpi/300x220/<?= $urlPagina ?>-1.jpg" class="w-100"
                alt="<?= $h1; ?>"></div>
        <div class="col-8">
            <h4 class=" text-uppercase">
                <?= $h1; ?>
            </h4>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-12 text-center"><a href="#formulario"
                class="scrollsuave d-block btn-mpi py-1 w-100  text-center text-white">RECEBER LIGAÇÃO</a></div>
    </div>
</div>