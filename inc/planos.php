<?php 
    $link = "http://www.servertemporario.com.br/guardiao/idealodonto/planos.xml";
    $xml = simplexml_load_file($link);
?>
<div class="row <?php if($h1 != 'Planos feitos para Você e para sua família'){echo 'py-5';} ?>">
    <?php
        $i = 0;
        foreach($xml as $item){
            $j = $i;
            switch(strtolower($h1)){
            case "odonto orto":
                $j= $j+2;
                break;

            case "odonto kids":
                $j = $i;
                break;

            case "odonto quality":
                $j = $j-2;
                break;

            case "planos feitos para você e para sua família":
                $j = 0;
                break;
        }
        if(strpos(strtolower($item->nome), strtolower($h1)) !== false){
            $alinhamento = "order-".$j;
        }else{
            $alinhamento = "order-".$i;
        }
    ?>
    <div class="col-md-4 <?= $alinhamento; ?>">
        <div class="precos-plano w-100 <?php if(strpos(strtolower($item->nome), strtolower($h1)) !== false){echo "active-plano";} ?> ">
            <img src="<?= $url; ?><?= $item->urlimg; ?>" alt="<?= $h1 ?>">
            <h2 class="h3 text-black mt-1"><?=$item->nome; ?></h2>
            <ul class="lista-plano">
                <?= $item->desc; ?>
            </ul>
            <p class="carencia"><?= $item->carencia; ?></p>
            <p class="preco-pl mb-4"><?= $item->preco; ?><span class="mensais">mensais</span></p>

            <div class="row m-0 justify-content-center">
            <?php if(strtolower($h1) == 'planos feitos para você e para sua família'){ ?>
                <div class="col-md-6 col-12 p-1"><a class="btn btn-lg button-slider px-0 text-white" style="width:100%" href="<?= $item->link; ?>"> Adquirir Já</a></div>
                <div class="col-md-6 col-12 p-1"><a class="btn btn-lg button-slider px-0 text-white btn-azul" href="<?= $url.strtolower(str_replace(' ', '-', str_replace('<span id="quality"></span>', '', $item->nome)));?>" style="width:100%;">Saiba Mais</a></div>
            <?php }else{ if(strpos(strtolower($item->nome), strtolower($h1)) !== false){ ?>
                <div class="col-md-6 col-12 p-1">
                    <a class="btn btn-lg px-0 text-white font-weight-bold" href="<?= $item->link; ?>" style="width:100%;background:#00933d;">CONTRATAR</a>
                </div>
            <?php }else{ ?>
                <div class="col-md-6 col-12 p-1"><a class="btn btn-lg px-0 text-white font-weight-bold" href="<?= $item->link; ?>" style="width:100%;background:#FF6602;">CONTRATAR</a></div>
            <?php } } ?>
            </div>
            <div class="tp-pag"><span class="tp-pagamento"><?= $item->pagamento; ?></span> </div>
        </div>
    </div>
<?php $i++; } // fim do foreach($xml as $item) ?>
</div>
<?php  include("form-modal.php"); ?>




