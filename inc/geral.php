<?php
$nomeSite = "Ideal Odonto";
$slogan = "O plano odontológico que você precisa está aqui!";
$idAnalytics = "UA-125795183-30";

$latitude = "-23.6531196";
$longitude = "-46.7226373";
$cidade = "São Paulo";
$uf = "SP";

$dir  = pathinfo($_SERVER['SCRIPT_NAME']);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/";  }
else { $url = $http."://".$host.$dir["dirname"]."/";  }

$emailContato = "contato@doutoresdaweb.com.br";
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
$creditos    = 'Doutores da Web - Marketing Digital';
$siteCreditos    = 'www.doutoresdaweb.com.br';
$explode    = explode("/", $_SERVER['PHP_SELF']);
$urlPagina = end($explode);
$urlPagina    = str_replace('.php', '', $urlPagina);
$urlPagina == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);

//Gerar htaccess automático
$urlhtaccess = $url;
$schemaReplace = strpos($urlhtaccess, 'https://www.') === false ? 'https://' : 'https://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess, '/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/titletourl.php');
include('inc/gerador-htaccess.php');
$prepos = array(' a ', ' á ', ' à ', ' ante ', ' até ', ' após ', ' de ', ' desde ', ' em ', ' entre ', ' com ', ' para ', ' por ', ' perante ', ' sem ', ' sob ', ' sobre ', ' na ', ' no ', ' e ', ' do ', ' da ', ' ', '(', ')', '\'', '"', '.', '/', ':', ' | ', ',, ');