<?php
$h1 = "Dúvidas Frequentes";
$title = $h1;
$desc = "Dúvidas sobre os produtos e serviços comercializados pela empresa. Clique aqui para saber mais detalhes. Dúvidas, entre em contato conosco.";
include('inc/head.php');
?>

<body>
    <?php include 'inc/header.php' ?>
    <main>
       
        <section>
            <div class="container my-5">
                <div class="row">
                    <div class="col-12">
                        <h2 class="text-uppercase">ESTÁ COM DÚVIDAS?</h2>
                        <p class="h4">Pesquise pela sua duvida no campo a seguir:</p>
                    
                    <table class="buscaFaq w-100">
                        <tr>
                            <td style="width:80%;">
                                <input type="text" id="inputsearch" placeholder="Buscar..." class="busca" onkeypress="if(event.key === 'Enter'){SearchFunction();}" style="width:100%;padding: 9px;">
                            </td>
                            <td style="width:20%">
                                <input type="button" onclick="SearchFunction();" value="Buscar" class="btnfaq btn button-slider" style="width:100%;border-radius:0px">
                            </td>
                        </tr>
                    </table>
                        <div id="conta_resultado" class="col-12 p-0"></div>
                    </div>
                </div>
                <div class="row mt-4">

                    
                     <div class="col-12">
                    <table class="tg" id="searchtable">
                        <tbody>
                            <tr style="">
                                <td>
                                    <h2>Quais são os meios de pagamento disponíveis?</h2>
                                    <p>
                                        Através de cartão de crédito ou boleto bancário.
                                    </p>
                                    <hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Receberei carteirinha do plano?</h2>
                                    <p>
                                        Após 24h de sua adesão sua carteirinha estará disponível na Área do Cliente
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Antes de contratar, o que devo fazer?</h2>
                                    <p>
                                        Aconselhamos que verifique o plano que mais se adeque as suas necessidades, temos também a disposição o Manual de Contratação, em Downloads. Mas caso ainda tenha alguma dúvida por gentileza entrar em contato com nossa central.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Qual a diferença em optar por pagamento Mensal ou Anual?</h2>
                                    <p>
                                        Não existe nenhuma diferenciação, em ambas o período de carência seria o mesmo.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Pagarei algo a mais para o dentista por utilizar o plano?</h2>
                                    <p>
                                        Não, nossos planos não possuem franquia ou co-participação os procedimentos cobertos são 100% pagos pela operadora ao dentista.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Formas de contratação dos planos da Ideal Odonto?</h2>
                                    <p>
                                        Através do link X você vai ter acessos aos nossos planos comercializados, assim poderá escolher o que mais se adequada a sua família.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Caso tenha marcado consulta com um credenciado e não possa comparecer, como devo proceder?</h2>
                                    <p>
                                        Solicitamos que entre em contato com a clínica e remarque a sua consulta para outra data.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Com quem falo em caso de dúvidas?</h2>
                                    <p>
                                        Pode entrar em contato em nossa central no 0800 730 73 73, nossos colaboradores estarão a sua disposição.

                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Como faço para agendar uma consulta?</h2>
                                    <p>
                                        A nossa Rede Credenciada fica disponível através do link X, lá você terá acesso a todos os dados das nossas clínicas para proceder com o agendamento, caso sim tenha alguma dificuldade por gentileza entrar em contato com a nossa central.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Irei receber um manual com a Rede Credenciada?</h2>
                                    <p>
                                        A nossa rede credenciada está em pleno crescimento, desta forma o manual estaria sempre desatualizado, dessa forma optamos em trabalhar com a Rede On-Line e manter você sempre atualizado de nossas novas parcerias.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Como eu sei quais são os dentistas disponíveis da Rede Credenciada?</h2>
                                    <p>
                                        Todo dentista que constar em nossa rede atende o nosso plano, caso venha ocorrer alguma divergência por gentileza entrar em contato conosco para verificarmos a situação e tomar as medidas cabíveis.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Atende em todo o Brasil?</h2>
                                    <p>
                                        Sim, todos os nossos planos são Nacionais.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Quem pode contratar o Plano?</h2>
                                    <p>
                                        Pessoa física identificada e qualificada conforme dados constantes na proposta contratual, que é peça integrante deste instrumento contratual para todos os fins de direito.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Como faço para ver as Condições Gerais dos Planos antes de contratar?</h2>
                                    <p>
                                        Os contratos de nossos produtos estão disponíveis na aba download no final da nossa página inicial.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Existe reajuste nos Planos?</h2>
                                    <p>
                                        Sim, o reajuste é anual de acordo com a variação do IPCA (índice de preços ao consumidor ampliado)
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Como verifico a cobertura do plano que contratei?</h2>
                                    <p>
                                        Dentro das condições gerais possui a TABELA DE COBERTURA E REEMBOLSO de seu plano com código, nome e regras técnicas e administrativa para a realização de cada procedimento coberto pelo seu plano.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Quais tipos de próteses os planos cobrem?</h2>
                                    <p>
                                        Todos os procedimentos cobertos estão a disposição na TABELA DE COBERTURA E REEMBOLSO de seu plano.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Os planos cobrem implante dentário?</h2>
                                    <p>
                                        No momento não temos nenhum produto com esta cobertura, porém, implantes sempre são os últimos procedimentos a serem realizados dentro de um plano de tratamento, desta forma toda a preparação para a realização do implante pode ser realizada através de nossa rede Credenciada.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Preciso apresentar algum documento para a contratação dos planos?</h2>
                                    <p>
                                        Não, basta preencher todos os dados solicitados em nossos formulários. Nosso sistema está preparado para validar as informações passadas.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Posso contratar um plano para qualquer pessoa (pai, mãe, irmão, primo etc.)?</h2>
                                    <p>
                                        — São BENEFICIÁRIOS dependentes, com relação ao titular:
                                        a) Esposa(o) ou companheira(o), comprovada a relação estável pelos documentos pertinentes.
                                        b) Os filhos, os enteados, os tutelados que ficam equiparados aos filhos, para fins deste contrato.
                                        c) Pai, mãe, irmãos, avós, netos(as), tios(as), sobrinho(as), bisnetos(as), sogro(a), genro, nora, padrasto, madrasta, enteado(a), cunhado(a) e concunhado(a).
                                    </p><hr>
                                </td>
                            </tr>
                            <!-- <h2>SOBRE OS TRATAMENTOS</h2> -->
                            <tr style="">
                                <td>
                                    <h2>Existe limite de consultas ou tratamentos?</h2>
                                    <p>
                                        Não existe limite de utilização, porém, existe a garantia nos procedimentos, assim como regras para autorização de alguns tratamentos.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Os tratamentos realizados pela Rede Credenciada possuem garantia?</h2>
                                    <p>
                                        Sim, cada tratamento possui o período de garantia próprio, onde a clínica que realizar tem responsabilidade sobre o procedimento realizado dentro daquele período.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Como faço para trocar de dentista?</h2>
                                    <p>
                                        O adequado seria iniciar e terminar o tratamento com um único dentista, mas se por algum motivo venha a existir esta alteração basta informar o doutor que deseja interromper o tratamento e iniciar o tratamento em outra clínica.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Com quem posso falar sobre dúvidas de tratamentos?</h2>
                                    <p>
                                        Nossa central está a disposição para tirar qualquer dúvida e todo plano de tratamento antes de autorizado para seu dentista realizar é validado pelos nossos doutores técnicos responsáveis.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>A Ideal Odonto possui um controle de qualidade dos tratamentos feitos pela Rede Credenciada?</h2>
                                    <p>
                                        Sim, os pagamentos aos prestadores só são pagos se os mesmos seguirem diversas regras impostas pela operadora, para assim garantirmos o melhor tratamento para você e sua família.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>No dia da consulta, preciso apresentar algo para ser atendido?</h2>
                                    <p>
                                        É necessário a apresentação de sua carteirinha digital e de um documento com foto.

                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <!-- SOBRE O REEMBOLSO -->
                                    <h2>O serviço tem reembolso?</h2>
                                    <p>
                                        Dentro de seu contrato existe a modalidade de livre escolha, onde só pode ser utilizada após autorização da operadora, onde reembolsamos o valor gasto de acordo com a nossa tabela.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Qual o prazo para o reembolso?</h2>
                                    <p>
                                        O reembolso ocorrerá em até 30 (trinta) dias, contados a partir da data de entrega da
                                        documentação completa.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Se eu escolher ir no meu dentista particular, como devo fazer no dia da consulta?</h2>
                                    <p>
                                        A clínica junto a você, irão preencher o Formulário de Solicitação de Reembolso respeitando as regras técnicas e administrativas contidos na TABELA DE COBERTURA E REEMBOLSO de seu plano. Lembrando que a modalidade Livre escolha só pode ser realizada após autorização da Operadora.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Como calcular o valor de reembolso das coberturas?</h2>
                                    <p>
                                        Em nossa TABELA DE COBERTURA E REEMBOLSO consta o valor que reembolsamos para cada procedimento, assim como as regras técnicas e administrativas para que o reembolso ocorra.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Quando eu usar o dentista particular o reembolso será integral?</h2>
                                    <p>
                                        Não, o valor que será reembolsado, assim como as regras a serem respeitadas estão disponíveis na TABELA DE COBERTURA E REEMBOLSO do plano contrato.
                                        <!-- CARÊNCIA -->
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Qual o tempo de carência?</h2>
                                    <p>
                                        A carência seria de cento e oitenta dias (180) para prótese e ortodontia, para as demais especialidades seria de noventa dias corridos.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Por quanto tempo tenho que permanecer no plano?</h2>
                                    <p>
                                        O período mínimo seria de 12 meses, se cancelar antes deste período irá ocorrer a cobrança de uma multa.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Qual é o início de vigência dos planos?</h2>
                                    <p>
                                        Eles têm como início a data da contratação.

                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Caso cancele o contrato, posso solicitar minha reinclusão no plano?</h2>
                                    <p>
                                        Não, você não voltará para o plano que possuía, será feito uma nova adesão respeitando as regras dos planos comercializados na data da solicitação da reinclusão.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Caso eu cancele meu cartão de crédito, onde o débito da mensalidade está programado, o que acontece com meu plano?</h2>
                                    <p>
                                        Seu plano em nosso sistema irá constar como inadimplente, pois, não conseguiremos mais cobrar as parcelas, desta forma será inativado. O titular deve entrar em contato com a nossa central e solicitar a alteração do método de pagamento para não perder seus benefícios.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Pagarei multa para cancelar os Planos?</h2>
                                    <p>
                                        Sim, antes de 12 meses de vigência inicial, incidirá à CONTRATANTE o pagamento de multa de 20% (vinte por cento) das mensalidades restantes para completar o período, relativo ao número de BENEFICIÁRIOS excluídos, independentemente da utilização.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Sou titular e pedi cancelamento do contrato de meu plano ou retirada de um dependente. Posso reativar o plano para o dependente após cancelar?</h2>
                                    <p>
                                        Ocorrerá uma nova contratação respeitando as regras contratuais do produto comercializado na data da solicitação.
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Como posso solicitar o cancelamento do contrato?</h2>
                                    <p>
                                        O cancelamento deve ser solicitado através de nosso Serviço de Atendimento ao Cliente (SAC) no 0800 730-7373
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Com quem solicito o cancelamento do contrato?</h2>
                                    <p>
                                        O cancelamento deve ser solicitado através de nosso Serviço de Atendimento ao Cliente (SAC) no 0800 730-7373
                                    </p><hr>
                                </td>
                            </tr>
                            <tr style="">
                                <td>
                                    <h2>Dúvidas sobre Termos Odontológicos? </h2>
                                    <br><strong>Clínica Geral:</strong>
                                    <p> O famoso cirurgião-dentista é responsável pela prevenção, diagnóstico e tratamento das doenças da boca, maxilares e todas as suas estruturas anexas. Ele é quem faz a primeira análise do problema e, dependendo da situação, indica para um profissional especializado.
                                        Radiologia: A radiologia odontológica é uma das modalidades do Diagnóstico Por Imagem de maior preferência de atuação pelos profissionais da radiologia.
                                    </p><br><strong>Urgência e Emergência:</strong>
                                    <p> As palavras emergência e urgência são muito parecidas à primeira vista, mas em saúde são extremamente diferentes. Emergência é uma situação crítica ou um perigo iminente onde ocorre risco de MORTE. Entretanto, a Urgência é uma situação que deve ser resolvida imediatamente, que não pode ser adiada, mas que não tem risco de vida iminente.
                                    </p><br><strong>Protese:</strong>
                                    <p> É o profissional que trabalha com a reabilitação bucal em todas as funções, como estética, fonética e mastigação. Ele é responsável por atender e indicar a confecção de próteses dentárias. Não confundir o protesista com o protético, que é o profissional que realizou um curso e aprendeu a fazer as estruturas das próteses.
                                    </p><br><strong>Endodontia:</strong>
                                    <p> Esse especialista é responsável pela nossa polpa dentária, que fica dentro dos dentes, de todo o sistema de canais radiculares e dos tecidos periapicais, responsáveis pela sustentação, nutrição e defesa dos dentes. Esse também é o dentista que cuida do tratamento de canal visando a manutenção do dente na cavidade bucal.
                                    </p><br><strong>Odontopediatria:</strong>
                                    <p> Sua função é acompanhar o desenvolvimento do rosto e arcada dentária preservando os dentes de leite até a chegada dos permanentes para que não tenhamos problemas lá na frente. Além disso, fazer um acompanhamento com este profissional desde cedo é importante para o desenvolvimento da fala e mastigação nos primeiros anos de vida.
                                    </p><br><strong>Dentistica:</strong>
                                    <p> A Dentística é a área da odontologia que trata das alterações dos dentes e tecidos circunvizinhos, ou seja, trata da doença cárie, suas repercussões sobre a estrutura dental e alterações de forma — textura e cor dos dentes, devolvendo ao paciente à saúde, e aos dentes a estética, função e anatomia.
                                    </p><br><strong>Periodontia:</strong>
                                    <p> É o dentista que estuda e trata de todas as doenças relacionadas à gengiva e periodontos, conhecidas como doenças periodontais. Ele cuida do sistema de implantação e suporte dos dentes para que eles permaneçam firmes e fortes na boca.
                                    </p><br><strong>Cirurgia:</strong>
                                    <p> A cirurgia oral é a especialidade da odontologia responsável pelas intervenções cirúrgicas realizadas na cavidade bucal, destinada a prevenir, conservar ou reabilitar a saúde oral dos pacientes.
                                    </p><br><strong>Ortodontia:</strong>
                                    <p> Quem ajuda a organizar os nossos dentes deixando o nosso sorriso mais bonito e alinhado. Ele é responsável pelos aparelhos ortodônticos e ajuda na prevenção e tratamentos dos problemas de crescimento e desenvolvimento do rosto, dos arcos dentários e da mordida.</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                  

                </div>
            </div>
        </section>

    </main>

    <?php include 'inc/footer.php' ?>
    <script>
				function SearchFunction() {
				var input, filter, table, tr, td, i;
				input = document.getElementById("inputsearch");
				filter = input.value.toUpperCase().trim();
				table = document.getElementById("searchtable");
				tr = table.getElementsByTagName("tr");
				cont = 0;
				for (i = 0; i < tr.length; i++) {
				td2 = tr[i].getElementsByTagName("td")[0];
				if (td2) {
				if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
				cont = cont + 1 ;
				}
				else if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
				cont = cont + 1 ;
				}
				else {
				tr[i].style.display = "none";
				}
				}
				}
				
				if (((tr.length - 1) != (cont)) && (cont > 0)) {
				
				document.getElementById("conta_resultado").innerHTML = '<h4>Encontramo(s) '+cont+' resultado(s) por: '+input.value+'</h4><p>';
				}
				
				if ((tr.length - 1) == (cont)){
				swal({title: "Digite alguma coisa para buscar...",
				showCancelButton: false,
				confirmButtonClass: "btn-success",
				confirmButtonText: "Ok",
				closeOnConfirm: true});
				document.getElementById("conta_resultado").innerHTML = '';
				document.getElementById('inputsearch').value='';
				} else if (cont == 0) {
				
				swal({title: "Desculpe, não encontramos nenhum resultado por: "+input.value,
				showCancelButton: false,
				confirmButtonClass: "btn-success",
				confirmButtonText: "Ok",
				closeOnConfirm: true});
				table = document.getElementById("searchtable");
				tr = table.getElementsByTagName("tr");
				for (i = 0; i < tr.length; i++) {
				tr[i].style.display = "";
				}
				document.getElementById('inputsearch').value='';
				document.getElementById("conta_resultado").innerHTML = '';
				}
				//document.getElementById('inputsearch').focus();
				
				}
				function SearchFunction2() {
				
				table = document.getElementById("searchtable");
				tr = table.getElementsByTagName("tr");
				for (i = 0; i < tr.length; i++) {
				tr[i].style.display = "";
				}
				//document.getElementById('inputsearch').focus();
				document.getElementById("conta_resultado").innerHTML = '';
				}
				</script>



</body>

</html>
