<section class="mb-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6"><img src="<?= $url ?>assets/img/dentistas.png" alt="Ilustração de dois dentistas"
                    class="img-fluid"></div>
            <div class="col-md-6 text-left p-5">
                <h2 class="mt-4 mb-3  text-uppercase h1 ">
                    O sonho do sorriso perfeito na palma das suas mãos
                </h2>
                <p>Cuidar do sorriso vai além das escovações diárias, se tornou necessário possuir disciplina para
                    visitar o dentista e dar continuidade em procedimentos.

                </p>
                <p>Muitas pessoas deixam de visitar clínicas odontológicas devido ao valor da consulta e taxas dos
                    procedimentos. Foi pensando nisso, que a Ideal Odonto criou três planos odontológicos, com a missão
                    de tornar acessível a todos os públicos, independente da idade, consultas e tratamentos
                    odontológicos.

                </p>

                <a class="btn btn-lg button-slider mt-2 " href="<?= $url ?>informacoes">Saiba mais</a>
            </div>
        </div>
    </div>
</section>