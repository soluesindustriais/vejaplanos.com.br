<?php
include('inc/vetKey.php');
$h1 = "convênio odontológico";
$title = $h1;
$desc = "Cuide melhor da sua saúde bucal com o auxílio do convênio odontológico Ao nascer, as crianças ainda não possuem as dentinas formadas, mas mesmo assim,";
$key = "convênio,odontológico";
$legendaImagem = "Foto ilustrativa de convênio odontológico";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Cuide melhor da sua saúde bucal com o auxílio do convênio odontológico</h2>
                <p>Ao nascer, as crianças ainda não possuem as dentinas formadas, mas mesmo assim, é essencial começar
                    nos primeiros meses os cuidados com a saúde bucal. A limpeza nessa fase deve ser feita com muita
                    delicadeza, massageando a boquinha das crianças com uma gaze ou uma fraldinha de tecido molhada com
                    água, colocada no dedo indicador. Após, a mamãe precisa passar na parte interna da bochecha, pela
                    gengiva e ainda na língua do bebê, e pronto: está feita a higienização.</p>
                <p>Este é um ritual simples, realizado ainda nos primeiros dias de vida, quando não há nenhuma dentina
                    na boca sequer. Só por este detalhe tão relevante podemos perceber a dimensão que os cuidados com a
                    saúde oral tem na vida das pessoas. Quando a atenção começa na infância e se mantém com o passar do
                    tempo, é muito mais fácil exibir um sorriso bonito e saudável, livre de doenças e esteticamente
                    encantador.</p>
                <h2>Vantagens que o convênio odontológico proporciona para a saúde</h2>
                <p>Como dissemos no início deste artigo, quando as precauções com a dentição começa na primeira
                    infância, o resultado só pode ser uma boca com dentes mais brancos e alinhados. Mas para esbanjar
                    saúde oral por aí não existe milagre. Não adianta a mãe tratar com carinho do sorriso do bebê, e
                    depois, lá na adolescência, o filho relaxar e esquecer dos costumes importantes de limpeza e
                    higiene. Por mais que os dentes tenham sido tratados com zelo antes, se não forem mantidos os
                    cuidados ao longo da vida, os problemas aparecem e podem colocar tudo a perder.</p>
                <p>Para muitas famílias, uma das alternativas que ajudam a manutenção da saúde oral é o convênio
                    odontológico. Isso porque, graças ao plano dental, crianças, jovens e adultos conseguem se organizar
                    e sabem que, em determinada data e hora, tem compromisso com as consultas com o odontologista.</p>
                <p>Com a elevação da busca pela assistência à saúde bucal, o mercado de convênio odontológico deu um
                    boom. Hoje em dia, basta uma pesquisa rápida na internet ou mesmo um bate-papo com amigos e
                    familiares, para saber que existem diversos trabalhos disponíveis. Via de regra, todo plano odonto
                    que se preze necessita manter a qualidade dos trabalhos fornecidos, contar com uma rede de dentistas
                    especializados e capacitados, boas condições de pagamento, preço acessível e coberturas expandidas.
                </p>
                <p>O correto para não adquirir um serviço ruim e que só lhe renda dores de cabeça em vez de atendimento
                    rápido e eficiente, é perder algum tempo fazendo uma pesquisa de mercado. Converse com indivíduos
                    que são clientes de determinadas empresas e procure os pontos positivos e negativos e se vale a pena
                    fazer o investimento.</p>
                <h2>Entenda como funciona o convênio odontológico</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>A regulamentação e fiscalização dos planos de saúde médico-hospitalar e odontológico é realizada pela
                    Agência Nacional de Saúde Suplementar (ANS). Dessa maneira, antes de adquirir um convênio
                    odontológico verifique se a empresa ou cooperativa que fornece o serviço tem registro ativo na ANS.
                    Outra dica fundamental, é ficar de olho na abrangência territorial do plano: algumas operadoras
                    limitam o atendimento ao endereço de origem do paciente, já outras têm profissionais e clínicas
                    credenciadas em todo o território nacional. Não deixe de verificar ainda as coberturas de
                    atendimento, ou seja, tudo que o convênio oferece ao paciente.</p>
                <p>Na prática, os planos de saúde odontológico cobrem as seguintes coberturas: </p>
                <ul>
                    <li>
                        <p>restaurações;</p>
                    </li>
                    <li>
                        <p>diagnósticos;</p>
                    </li>
                    <li>
                        <p>radiologia;</p>
                    </li>
                    <li>
                        <p>cirurgias;</p>
                    </li>
                    <li>
                        <p>limpeza;</p>
                    </li>
                    <li>
                        <p>tratamento de canal;</p>
                    </li>
                    <li>
                        <p>atendimentos emergenciais.</p>
                    </li>
                </ul>
                <h2>Saiba quais coberturas do convênio odontológico estão disponíveis</h2>
                <p>Cada vez mais as pessoas procuram ter uma saúde bucal para ter qualidade de vida. Na realidade, um
                    sorriso perfeito nunca sai de moda, mas a impressão que temos, é que nos dias atuais é quase uma
                    questão de honra sorrir com dentes brancos e alinhados. Porém, apesar da evolução da odontologia,
                    com equipamentos tecnológicos e tratamentos mais rápidos e personalizados, muita gente deixa de
                    frequentar os consultórios regularmente.</p>
                <p>As razões são muitas: uns dão a desculpa do medo do dentista para fugir dos procedimentos. Outros
                    dizem que não tem tempo para buscar os profissionais e agendar as consultas. E há também os que
                    colocam na falta de dinheiro seu desleixo com a saúde oral. Claro que há situações que realmente
                    atrapalham a rotina, mas nenhuma dessas opções é razão para não cuidar do sorriso.</p>
                <p>Graças à chegada do convênio odontológico ao mercado ficou mais complicado utilizar essas três
                    desculpas para fugir do atendimento. Se um profissional particular é caro para determinados
                    tratamentos, primordialmente os trabalhos de urgência e emergência, a opção por um plano dental
                    tende a ficar mais em conta. Os preços são acessíveis e é possível planejar as consultas, sem pesar
                    no orçamento. </p>
                <p>E quando se fala em medo, pior ainda. Nos dias de hoje, os especialistas, seja da rede particular ou
                    habilitados no convênio odontológico, visam o bem-estar do beneficiário. Com atendimento humanizado,
                    os especialistas sabem que é necessário tratar cada indivíduo de um modo especial, e utilizam
                    técnicas para acalmar os mais medrosos. Viu só? Não há desculpas plausíveis para não cuidar da saúde
                    oral exatamente do jeito que ela merece. </p>
                <h2>Veja todos os benefícios que o convênio odontológico tem para você!</h2>
                <p>Antes de falar mais sobre as vantagens do convênio odontológico, vamos entrar em um tema delicado, no
                    entanto, essencial. Ao comentar que está disposto a contratar um plano dental, muitos indivíduos
                    irão lhe dizer que estará jogando dinheiro fora. Isso porque, acreditam que por ser um convênio, com
                    custos normalmente mais baixos, os dentistas não irão atender com qualidade, tampouco farão o
                    tratamento adequadamente. </p>
                <p>Esse é um mito que paira sobre o “mundo” dos planos de saúde odontológicos. Mas não é uma verdade. O
                    recepcionamento do convênio odontológico segue regras e é analisado pela ANS. Além disso, a maioria
                    dos especialistas que se enquadra no perfil de credenciados, é especialista em diversas áreas e está
                    disposto a atender da melhor maneira possível. Os materiais são de alta qualidade e os tratamentos
                    são feitos com extremo cuidado, dentro das coberturas abrangentes. Sendo assim, não há nada de
                    errado em confiar sua saúde bucal a um convênio odontológico.</p>
                <p>Mas o paciente necessita estar atento às coberturas antes de fechar o negócio, afinal, nem todos os
                    tratamentos são contemplados em um plano dental. A maioria deles, por exemplo, não cobre a
                    manutenção ou colocação de aparelhos ortodônticos, próteses, implantes dentários e tratamentos
                    exclusivamente estéticos.</p>
                <p>Os preços também podem variar, exatamente por razão da inclusão ou não de certos procedimentos. O
                    usuário pode escolher pelo pagamento mensal ou anual. Cada contrato tem suas carências bem
                    definidas, mas em média, o usuário pode usar alguns serviços somente depois de 90 dias. Outros
                    procedimentos oferecem carência de até 60 dias - e vale citar do plano odontológico sem carência.
                    Este é certamente um dos mais procurados, porque o beneficiário pode acionar o atendimento em no
                    máximo 24 horas. </p>
                <h2>Prospecção de clientes: convênio odontológico</h2>
                <p>Um dos pontos vantajosos do convênio odontológico sob a ótica dos odontologistas é a possibilidade de
                    captar novos usuários. Os novos dentistas, especialmente logo após a graduação, ainda não criou sua
                    clientela e é complicado começar a carreira sem ter pacientes no consultório. Só a prática dará ao
                    profissional condições de atender com excelência e aos poucos, conquistar um número de pacientes que
                    confiam no seu trabalho.</p>
                <p>Visando isso, quando um odontologista se dispõe a firmar parceria e prestar trabalhos a um convênio
                    odontológico, suas chances de crescimento profissional crescem consideravelmente. Afinal de contas,
                    no dia a dia dos consultórios que atendem planos dentários surgem diversos problemas, perfis de
                    paciente e novos desafios. É, sem dúvida, garantia de agenda cheia para os especialistas
                    desenvolverem ainda mais sua carreira e aptidões.</p>
                <p>Para auxiliar os odontologistas que estão na dúvida se ingressam ou não em um convênio odontológico,
                    os técnicos indicam algumas dicas importantes. São elas:</p>
                <ul>
                    <li>
                        <p>veja se o perfil do convênio se enquadra no estilo de atendimento e na especialidade em que
                            você atua;</p>
                    </li>
                    <li>
                        <p>é a melhor forma para captar novos clientes no início da carreira;</p>
                    </li>
                    <li>
                        <p>confira como anda o mercado odontológico na sua região;</p>
                    </li>
                    <li>
                        <p>pesquise sobre os planos odontológicos;</p>
                    </li>
                    <li>
                        <p>geralmente, a agenda do profissional está sempre lotada;</p>
                    </li>
                    <li>
                        <p>aproveite para fazer um belo Networking e ampliar suas relações de trabalho.</p>
                    </li>
                </ul>
                <h2>Mais qualidade de vida com saúde oral</h2>
                <p>Hábitos básicos de como escovar as dentições três vezes ao dia, utilizar fio dental e manter a
                    regularidade das consultas com seu especialista de confiança, auxiliam a manter a saúde oral em
                    ordem. Mas, milhares de pessoas não levam isso tão a sério. O resultado são doenças dentais que
                    podem ocasionar malefícios para todo o organismo. Uma simples cárie, por exemplo, se não tratada de
                    maneira adequada, leva a infecções e outros distúrbios, sem falar nas dores insuportáveis que o
                    paciente sente por causa do problema. Não vale a pena passar por isso, é muito mais fácil e indolor
                    recorrer à contratação de um convênio odontológico, mantendo as consultas preventivas e corretivas.
                </p>
                <p>Veja os principais benefícios do plano odonto:</p>
                <ul>
                    <li>
                        <p>aproximam os pacientes dos dentistas;</p>
                    </li>
                    <li>
                        <p>fortalecem a rotina de consultas;</p>
                    </li>
                    <li>
                        <p>pagamento mensal ou anual;</p>
                    </li>
                    <li>
                        <p>atendimento personalizado;</p>
                    </li>
                    <li>
                        <p>coberturas que se enquadram no perfil do cliente;</p>
                    </li>
                    <li>
                        <p>preço acessível;</p>
                    </li>
                    <li>
                        <p>abrangência territorial;</p>
                    </li>
                    <li>
                        <p>qualidade nos tratamentos;</p>
                    </li>
                    <li>
                        <p>zero carência (dependendo do plano contratado).</p>
                    </li>
                </ul>
                <h2>Obtenha mais saúde e beleza para seu sorriso</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Tente passar o dia todo sem dar um sorriso sequer. Só de imaginar nesse momento, você já pode ter
                    ficado um pouco triste e desanimado. E é exatamente dessa maneira que os indivíduos que não sorriem
                    passam as manhãs, tardes e noites: angustiadas. O sorriso é capaz de melhorar o bom-humor e a
                    alegria, sentimentos imprescindíveis para manter a qualidade de vida. </p>
                <p>E claro, que sorri mais quem tem uma boca saudável, com dentinas bonitas e branquinhas. Dentro dos
                    consultórios de um convênio odontológico o que mais se vê são beneficiários correndo atrás do
                    prejuízo, corrigindo problemas originados ainda na infância, que se agravam pela falta de cuidado e
                    limpeza oral. No entanto, há sempre tempo para melhorar seu sorriso e transformar sua vida por
                    completo. Quer mesmo saber porque o sorriso vale ouro? Vamos aos destaques:</p>
                <ul>
                    <li>
                        <p>Aumenta a longevidade: sorriso natural rejuvenesce e a expectativa de vida de quem sorri
                            aumenta em até sete anos;</p>
                    </li>
                    <li>
                        <p>Melhora o humor: o sorriso tem um poder incrível. Basta alguém contar uma piada em uma roda
                            de conversa, para até os mais depressivos se animarem;</p>
                    </li>
                    <li>
                        <p>Fortalece o sistema imunológico: menos estresse, mais relaxamento e melhora da imunidade, com
                            menor incidência de doenças;</p>
                    </li>
                    <li>
                        <p>Produtividade: no ambiente de trabalho é comum os problemas afetarem o rendimento, mas os
                            funcionários que encaram as dificuldades com um sorriso no rosto melhoram a produção e
                            refletem a boa energia aos colegas;</p>
                    </li>
                    <li>
                        <p>Cérebro: sabe o famoso exercício físico para o corpo? Para o cérebro, o melhor exercício é o
                            sorriso;</p>
                    </li>
                    <li>
                        <p>Pressão arterial: sorrir diariamente reduz os batimentos cardíacos e ajuda a manter a pressão
                            arterial;</p>
                    </li>
                    <li>
                        <p>Nada de estresse: o sorriso é capaz de amenizar problemas emocionais e aquele estresse do dia
                            a dia;</p>
                    </li>
                    <li>
                        <p>Relações sociais: quem sorri mais, estreita o convívio com amigos e familiares. Mas vale
                            lembrar: sorria com espontaneidade. Um sorriso falso pode ser pior que ficar sem mostrar os
                            dentes.</p>
                    </li>
                </ul>
                <p>E não tem como falar sobre sorriso, sem ressaltar os recepcionamentos de um bom convênio
                    odontológico. A periodicidade das consultas ao especialista reduz as chances de desenvolver
                    problemas orais, além do mais, quanto mais as pessoas realizam os tratamentos corretivos, mais
                    investem na limpeza e na prevenção.</p>
                <p>Ao escovar corretamente a dentina e utilizar fio dental todos os dias, garantimos um sorriso
                    saudável, e as consequências são benéficas para todo o corpo humano. Segundo análise divulgada pela
                    Organização Mundial da Saúde (OMS), o pessimismo eleva o risco de acontecer um Acidente Vascular
                    Cerebral (AVC). E não é exagero nenhum falar que o sorriso é um dos grandes responsáveis pelo estado
                    de saúde completa da população. Quem sorri com confiança, é otimista, ou seja, reduz e muito as
                    chances de doenças graves. </p>
                <h2>Convênio odontológico empresarial: saiba como funciona</h2>
                <p>A chave para o êxito pode estar por trás de um sorriso perfeito. Especialmente nas grandes e médias
                    operadoras, já é possível contar com apoio de um convênio odontológico. Os funcionários sabem da
                    essencialidade de valorizar a saúde física e mental dos colaboradores e, cada vez mais, aderem aos
                    planos de saúde, seja médicos ou dentários. </p>
                <p>Trabalhar ao lado de colaboradores que sorriem mais e se sentem mais seguros é muito bom. O bom
                    rendimento no local de trabalho está completamente associado à qualidade de vida e bem-estar dos
                    indivíduos. Um colaborador que sente dores dentárias e não tem condições financeiras de arcar com o
                    tratamento, terá que se submeter a filas à espera de um atendimento público, ou pagar valores mais
                    altos por um tratamento particular de emergência. </p>
                <p>Para evitar esse ponto, as companhias investem em convênio odontológico, fornecendo essa vantagem aos
                    funcionários. Com este apoio, ao sinal de qualquer problema bucal, o colaborador poderá agendar a
                    consulta sem demora, resolvendo a situação o mais rápido possível. Além do que, com acesso ao
                    tratamento adequado, o funcionário rende mais e mantém a produtividade. Os empresários, por sua vez,
                    terão a garantia de uma equipe comprometida, e o melhor, com um sorriso diferenciado. </p>
                <p>O convênio odontológico empresarial tem evoluído bastante no país, e é uma das modalidades mais
                    presentes no mercado. As formas de pagamento podem ser feitas de três maneiras: o empregador paga o
                    plano dental integral; o funcionário arca com 50% e os outros cinquenta por cento são de
                    responsabilidade do gestor; o colaborador paga o benefício com desconto direto na folha de
                    pagamento. Para o gestor, ainda há outro benefício: os valores investidos com convênio odontológico
                    podem ser descontados no imposto de renda. </p>
                <p>Uma grande vantagem do convênio odontológico empresarial é o atendimento sem carência. As companhias
                    que investem neste método tem a possibilidade de fornecer aos empregados tratamentos em até 24
                    horas. </p>
                <p>Veja outras particularidades do plano empresarial:</p>
                <ul>
                    <li>
                        <p>higiene bucal;</p>
                    </li>
                    <li>
                        <p>aparelho ortodôntico;</p>
                    </li>
                    <li>
                        <p>remoção de tártaro;</p>
                    </li>
                    <li>
                        <p>limpeza completa;</p>
                    </li>
                    <li>
                        <p>extrações;</p>
                    </li>
                    <li>
                        <p>radiografias;</p>
                    </li>
                    <li>
                        <p>aplicação de flúor;</p>
                    </li>
                    <li>
                        <p>inclusão de dependentes no plano (familiares);</p>
                    </li>
                    <li>
                        <p>urgência e emergência 24 horas.</p>
                    </li>
                </ul>
                <h2>Cuidados essenciais sobre a saúde oral</h2>
                <p>Quanto mais escovamos as nossas dentições, mais a dentição fica bonita e impecável. Isso não é bem
                    verdade. É claro que temos que manter a escovação dentária todos os dias, pelo menos três vezes após
                    as refeições e usar também o fio dental como auxílio. Mas exagerar na força ao limpar os dentes
                    podem acabar prejudicando a gengiva.</p>
                <p>Alguns indivíduos com dentição alinhadinha e branquinha, sofrem com retração gengival. Um dilema que
                    inicia devagar, às vezes até despercebido. De repente, uma retração não solucionada evolui para
                    gengivite e situações mais alarmantes. Mas você deve estar se perguntando: como faço para evitar
                    esse problema e continuar com os dentes limpos? É muito simples: faça a escovação correta.</p>
                <p>Segundo os profissionais da saúde, o primeiro passo é eleger uma escova de dentes adequada para sua
                    dentição. No mercado há diversos modelos, desde as mais simples até as arrojadas. Mas não perca
                    muito tempo escolhendo modelos mirabolantes, cheios de frescura. A escova deve ser reta, com cerdas
                    macias. Essa é, sem dúvida, a recomendação da maioria dos dentistas. Na sequência, escolha também um
                    fio dental e, se possível, um enxaguante bucal. Esses itens são fundamentais para complementar a
                    saúde bucal. </p>
                <p>Com os recursos em mãos, é hora de realizar a limpeza adequada. Lembre-se de deixar a força de lado.
                    Uma higienização eficiente requer movimentos suaves e circulares. Inicie escovando os molares, no
                    fundo da boca, depois passe para a parte da frente. Também é essencial escovar o ângulo de
                    mastigação, bem como a dentição interna inferior e superior. Não esqueça de escovar a língua,
                    afinal, ela pode armazenar bactérias que causam doenças. </p>
                <h2>Convênio odontológico: vale a pena mesmo o investimento?</h2>
                <p>Se você chegou até esse ponto do artigo, aposto que já desconfia qual será a resposta para essa
                    pergunta. O fato é que sempre vale a pena investir em saúde oral, e pouco importa o método que você
                    ou sua família escolhe usar. De modo geral, o que vimos até aqui, é o acesso facilitado que o
                    convênio odontológico proporciona aos beneficiários. </p>
                <p>Indivíduos que nunca tiveram contato com um especialista, pelos diversos motivos que mencionamos
                    acima, podem encontrar apoio em um plano de saúde dental que caiba no seu bolso e, claro, atenda
                    suas necessidades. A grande meta de um convênio odontológico não é apenas ser mais barato - até
                    porque, dependendo das coberturas - o custo sobe e pode ser que se assemelhe ao tratamento
                    particular. Mas ainda assim, com convênio dentário, o paciente terá condições de fazer uma bela
                    economia. Porém, este não é ponto principal.</p>
                <p>Um convênio odontológico tem auxiliado a mudar a rotina de milhares de famílias no Brasil, incluindo
                    na rotina as consultas preventivas. Apenas os cuidados com a saúde oral pode garantir um sorriso
                    saudável ao longo dos anos. Claro que imprevistos ocorrem, e até mesmo quem sempre tratou da boca
                    com muito amor e zelo, pode sofrer consequências. De repente, sem mais nem menos, uma dentição
                    quebra, já pensou? No entanto, para os pacientes prevenidos, que contam com suporte de um convênio
                    odontológico, a situação pode ser resolvida rapidamente e sem gastos extras, que atrapalham o
                    planejamento do orçamento familiar. </p>
                <h2>Pense bem em qual será sua decisão</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>No fim das contas, é muito mais barato e saudável focar na qualidade de vida do sorriso. Não vale a
                    pena deixar de realizar investimento em um convênio odontológico ou qualquer que seja a vantagem que
                    mais se enquadre na sua realidade. O custo que se paga pela negligência com os cuidados orais
                    costuma ser muito alto. Pessoas com dentição problemática, sofrem com preconceito, zombaria e até
                    ficam suscetíveis à depressão. Sem uma arcada dentária de boa aparência, milhares de pessoas se
                    escondem e preferem não sair de casa, evitando o convívio social. </p>
                <p>Por isso, a resposta do questionamento anterior é sim: vale muito a pena todo e qualquer investimento
                    quando o tema é saúde. Não se trata unicamente de frequentar o convênio odontológico para manter a
                    estética do sorriso, mas sim, garantir uma vida plena, em todos os quesitos. Quem sorri tem
                    vantagens no ambiente de trabalho, consegue melhorar as amizades, abre portas para o tão sonhado
                    emprego, consegue se expressar com mais facilidade e estreita as relações pessoais.</p>
                <p>Sempre que você pensar no quanto vai gastar para cuidar do seu sorriso, coloque na balança o ganho
                    que terá com a prevenção. Mas, não deixe de analisar qual plano se encaixa melhor no seu orçamento e
                    estilo de vida. Sempre existe um convênio odontológico feito para você!</p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>