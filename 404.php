<?php
$h1 = "Página não encontrada";
$title = $h1;
$desc = "Erro 404: Página não encontrada - Navegue pelo menu do nosso site e encontre o que está procurando, escolha abaixo a página que deseja visualizar.";
// $previousUrl = array();
// $previousUrl[0] = array("title" => "Produtos")
include("inc/head.php");
?>

<body>
    <?php include "inc/header.php" ?>
    <main>
        <section class="container my-5" style="min-height:35vh">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <h2 class="mt-2"><span class="h1">Ops! </span>Alguma coisa deu errado.</h2>
                    <p class="h3">Isso é estranho, parece que você procurou por algo que não existe nesse site</p>
                </div>
                <div class="col-md-12 text-center" style="padding:15px;">
                    <a rel="nofollow" title="Voltar a página inicial" class="btn btn-primary" href="<?= $url ?>">Voltar
                        a página inicial</a>
                    <a rel="nofollow" title="Ver o mapa do site" class="btn btn-outline-primary"
                        href="<?= $url ?>mapa-site">Ver o mapa do site</a>
                </div>
                <div class="col-md-12 text-center mt-5">
                    <img src="<?= $url ?>/assets/img/404.png" alt="404" style="height: 20em;" class="img-fluid">
                </div>
            </div>
        </section>
        <div>


        </div>
    </main>
    <?php include "inc/footer.php" ?>
</body>

</html>