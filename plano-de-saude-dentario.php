<?php
include('inc/vetKey.php');
$h1 = "plano de saúde dentário";
$title = $h1;
$desc = "Produtividade, eficácia e plano de saúde dentário nas empresas Para se destacar no concorrido mercado de trabalho e manter seu trabalho a salvo, os";
$key = "plano,de,saúde,dentário";
$legendaImagem = "Foto ilustrativa de plano de saúde dentário";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Produtividade, eficácia e plano de saúde dentário nas empresas</h2>
                <p>Para se destacar no concorrido mercado de trabalho e manter seu trabalho a salvo, os colaboradores
                    devem focar na produtividade. Hoje em dia, o que conta na hora da contratação não é apenas a
                    apresentação de um ótimo currículo, mas sim, a maneira com que o funcionário se comporta diante das
                    dificuldades e da demanda de trabalho.</p>
                <p>Mas motivar os funcionários para que atinjam seu melhor rendimento é função também das operadoras. Em
                    outra época, a preocupação com a qualidade de vida do funcionário não era algo levado tão a sério
                    pelas empresas. Hoje em dia, esse cenário é totalmente diferente, e ter cuidado com a saúde
                    corporal, oral e mental dos trabalhadores é obrigação.</p>
                <p>De olho na produtividade e qualidade de vida dos pacientes, a numeração de companhias que investe em
                    vantagens como orientação nutricional, academia, análise física e psicológica, bem como planos de
                    saúde médico-hospitalar e plano de saúde dentário aumentou significativamente. No caso específico do
                    convênio odontológico, o investimento em saúde bucal tem melhorado muito o dia a dia dos
                    funcionários. O resultado é uma equipe de trabalho mais motivada, com sorriso saudável e bonito. Em
                    médio e longo prazo, a organização terá maior produtividade e satisfação no ambiente de trabalho.
                </p>
                <h2>Você sabe como funciona um plano de saúde dentário empresarial? Conheça todos seus benefícios aqui!
                </h2>
                <p>O acesso ao plano de saúde dentário dentro das empresas tem melhorado nos últimos anos, tanto que
                    essa categoria é uma que mais aumentam dentro das empresas. Segundo dados do Instituto de Estudos da
                    Saúde Suplementar (IESS) - em janeiro de 2018, a numeração de pacientes de planos dentários subiu em
                    todos os estados do país. Os índices apontam um crescimento de quase sete por cento na contratação
                    do plano odontológico coletivo.</p>
                <p>Apesar do avanços, ainda prevalece no mercado corporativo o convênio médico. Porém, aos poucos, os
                    empresários têm percebido que manter a saúde bucal dos colaboradores em dia é tão fundamental quanto
                    a saúde de todo o organismo. Afinal, qualidade de vida tem a ver com sorriso saudável e bonito. </p>
                <p>Na prática, o plano de saúde dentário empresarial garante ao colaborador atendimento eficaz e
                    eficiente, em clínicas e consultórios credenciados na rede que presta os serviços. O foco é manter a
                    equipe com a saúde bucal em ordem. O custo-benefício às corporações é ótimo. Contratar um convênio
                    odontológico é mais em conta que um plano de saúde médico, além disso, os empresários notaram que
                    muitos colaboradores não se sentiam à vontade no ambiente de trabalho devido a problemas bucais não
                    tratados adequadamente.</p>
                <p>Com acesso ao plano de saúde dentário coletivo, os funcionários podem desfrutar de consultas mais
                    frequentes ao especialista, evitando complicações com a saúde oral. Confira a seguir as doenças
                    bucais que acometem as pessoas:</p>
                <ul>
                    <li>
                        <p>cárie dental;</p>
                    </li>
                    <li>
                        <p>gengivite (doenças periodontais);</p>
                    </li>
                    <li>
                        <p>desgaste do esmalte e dos dentes;</p>
                    </li>
                    <li>
                        <p>infecção de canal (endodontia);</p>
                    </li>
                    <li>
                        <p>retração gengival;</p>
                    </li>
                    <li>
                        <p>bruxismo;</p>
                    </li>
                    <li>
                        <p>má oclusão;</p>
                    </li>
                    <li>
                        <p>excesso de tártaro e placa bacteriana.</p>
                    </li>
                </ul>
                <h2>Vantagens do plano de saúde dentário nas operadoras</h2>
                <p>No ambiente corporativo, a saúde física e mental dos funcionários possui relação direta na
                    produtividade, e é capaz ainda de transformar para melhor o lugar de trabalho.</p>
                <p>Funcionários com dores de dente, por exemplo, produzem bem menos e passam grande parte do dia
                    mal-humorados. Uma dentição doendo tira a concentração, gerando irritabilidade e desconforto. O
                    clima ruim acaba afetando todos os colegas e comprometendo as relações profissionais e pessoais.</p>
                <p>Por outro lado, em companhias que oferecem plano de saúde dentário aos funcionários, o dilema até
                    pode ocorrer, afinal, ninguém está livre de um problema oral, não é mesmo? No entanto, com acesso ao
                    convênio, o funcionário poderá se ausentar para tratar a situação e retornar com gás total, livre de
                    dores e com a saúde em dia. Esta é uma das vantagens do plano de saúde dentário nas organizações:
                    garantir ao colaborador acesso mais rápido ao atendimento.</p>
                <h2>Plano de saúde dentário e as coberturas habilitadas pelo rol da ANS</h2>
                <p>Os planos odontológicos são embasados e fiscalizados pela Agência Nacional de Saúde Suplementar
                    (ANS). O foco é oferecer trabalhos padronizados, com objetivo na saúde e qualidade de vida dos
                    usuários. Apesar dos diferenciais de cada plano de saúde dentário, a maioria garante aos pacientes
                    os atendimentos básicos no âmbito da odontologia. Confira na lista abaixo o rol de procedimentos que
                    devem estar incluídos em qualquer plano de saúde dentário, seja ele empresarial, individual ou
                    familiar: </p>
                <ul>
                    <li>
                        <p>consulta inicial com dentista credenciado à rede;</p>
                    </li>
                    <li>
                        <p>profilaxia (polimento coronário);</p>
                    </li>
                    <li>
                        <p>remoção de tártaro;</p>
                    </li>
                    <li>
                        <p>restaurações;</p>
                    </li>
                    <li>
                        <p>tratamento de cáries;</p>
                    </li>
                    <li>
                        <p>aplicação de flúor;</p>
                    </li>
                    <li>
                        <p>limpeza e tratamento preventivo.</p>
                    </li>
                </ul>
                <p>Estes procedimentos informados acima são de nível simples e todas as modalidades de plano
                    odontológico devem conter tais coberturas. É essencial que o paciente fique atento e analise esses
                    pontos junto à operadora, antes de efetivar a contratação. Além do mais, a maioria dos planos também
                    garante acesso a tratamentos considerados de nível médio, são eles:</p>
                <ul>
                    <li>
                        <p>suturas;</p>
                    </li>
                    <li>
                        <p>curativos;</p>
                    </li>
                    <li>
                        <p>colagem de fragmentos;</p>
                    </li>
                    <li>
                        <p>periodontia (tratamento de gengiva);</p>
                    </li>
                    <li>
                        <p>endodontia (tratamento de canal).</p>
                    </li>
                </ul>
                <h2>Plano de saúde dentário empresarial garante muita economia</h2>
                <p>Com o passar do tempo, os gestores de grandes, médias e até mesmo pequenas empresas, notaram que seus
                    funcionários usavam o plano de saúde médico para grandes intercorrências, causando de certo modo, um
                    alto custo operacional. Colaboradores que precisam se ausentar para realizar grandes cirurgias,
                    exigem mais tempo de recuperação e, consequentemente, demoram mais para retornar à ativa. Já no
                    plano de saúde dentário acontece o contrário: geralmente os funcionários utilizam com mais
                    frequência, porém, os eventos são de baixo custo e a recuperação é rápida.</p>
                <p>Uma vantagem do plano de saúde dentário frente ao plano de saúde médico é a possibilidade de correção
                    de um procedimento mal feito no passado. Isto é, pessoas que não tinham acesso ao odontologista
                    antes da vantagem oferecida pela empresa, e precisam retratar um canal, por exemplo, conseguem
                    melhorar seu sorriso com um tratamento adequado. </p>
                <p>Outro ponto benéfico e que garante economia em médio e longo prazo às empresas é a prevenção. Quando
                    os colaboradores têm acesso ao plano de saúde dentário, podem se organizar e fazer as consultas
                    rotineiras, antes que um problema grave se instale na boca. Até porque, convenhamos: uma cárie
                    intensa e infecções periodontais não ocorrem do dia para a noite. Algumas pessoas passam meses ou
                    até anos sem ir ao odontologista, mesmo sentindo pequenas dores e incômodos. Simplesmente
                    procrastinam o diagnóstico. </p>
                <p>Neste contexto, os funcionários estão cada vez mais cientes que a contratação de um plano de saúde
                    dentário com foco na prevenção reduz o número de licenças, ajudando na produtividade e garantindo
                    economia. Falando em economia, confira outros benefícios do plano de saúde dentário empresarial:</p>
                <ul>
                    <li>
                        <p>uma das modalidades mais acessíveis financeiramente;</p>
                    </li>
                    <li>
                        <p>atendimento emergência e de urgência 24 horas;</p>
                    </li>
                    <li>
                        <p>o plano odontológico oferece flexibilidade, podendo ser estendido à família do colaborador;
                        </p>
                    </li>
                    <li>
                        <p>possibilidade de inclusão de vários serviços;</p>
                    </li>
                    <li>
                        <p>sem carência: os funcionários podem utilizar os serviços do plano assim que for assinado o
                            contrato.</p>
                    </li>
                </ul>
                <p>Os chefes que oferecem a sua equipe esta vantagem, sabem da importância de manter a saúde oral em
                    ordem, evitando patologias graves e um tratamento que, certamente, pesará no bolso do consumidor e
                    deixará o colaborador mais dias de atestado. </p>
                <h2>Colaborador saudável é igual a empresa competitiva!</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>O mercado de trabalho está grandemente competitivo. De um lado, as operadoras estão de olho nos
                    melhores colaboradores. De outro, os funcionários buscam organizações que além de pagar um bom
                    salário, investem no bem-estar dos colaboradores. Sendo assim, como dissemos nos tópicos anteriores
                    deste artigo, incluir o plano de saúde dentário no rol de benefícios dos colaboradores é uma grande
                    tendência e tem atraído bons resultados.</p>
                <p>Não se trata apenas de contratar colaboradores com um sorriso bonito, é muito mais que isso. As
                    operações que garantem a vantagem, reduzem faltas, devido aos dilemas de saúde oral e o melhor,
                    aumentam a satisfação dos trabalhadores. Afinal de contas, trabalhar com uma boca saudável é milhões
                    vezes mais fácil do que ter que encarar horas de trabalho com o molar latejando. A matemática é
                    simples: com qualidade de vida os indivíduos batem metas e trabalham satisfeitas. </p>
                <p>O acesso ao benefício pode ser realizado de muitas maneiras. A mais usada é a que o empregador arca
                    com 100% do pagamento, garantindo ao funcionário este diferencial após o contrato de emprego. Outra
                    maneira bem comum nas empresas de médio e pequeno porte é a divisão dos custos entre gestor e
                    funcionários. Ou seja, o empregador paga 50% do valor do benefício, e os outros 50% fica sob a
                    responsabilidade do colaborador. Geralmente, o desconto é feito direto na folha de pagamento,
                    evitando atrasos na mensalidade. </p>
                <h2>Plano de saúde dentário empresarial: saúde para os colaboradores</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Vale a pena dizer um ponto essencial desta modalidade. Os colaboradores podem estender a vantagem
                    para seus familiares basta incluir os nomes na hora da contratação do plano de saúde dentário. Para
                    as operações, quanto maior for a adesão dos trabalhadores, menor será o investimento final. </p>
                <p>Outro ponto: o empresário pode fazer a dedução no Imposto de Renda. Assim como já aconteceu com
                    outros pacientes como vale-alimentação ou refeição, plano de saúde médico e auxílio-farmácia, os
                    valores investidos em um plano de saúde dentário podem ser deduzidos na declaração. </p>
                <p>Devido à grande procura pelo plano de saúde dentário empresarial, muitos chefes fornecem a
                    possibilidade de escolha ampliada das coberturas, garantindo também a adesão a convênios mais
                    completos. Há pacotes diferenciados, dependendo do estilo da empresa e das necessidades de cada
                    colaborador.</p>
                <p>Hoje em dia, há planos odontológicos que cobrem procedimentos ortodônticos (aparelhos dentários) e
                    até mesmo tratamentos estéticos (como clareamento dental e implantes). Apesar do foco das empresas
                    ser o pacote simples, existem opções no mercado para garantir atendimento personalizado. Ainda
                    assim, o custo-benefício às empresas é notável, e o investimento representa economia no orçamento.
                </p>
                <h2>Tenha mais qualidade de vida com plano de saúde dentário</h2>
                <p>Pensa comigo: quando você se arrumou para ir a sua primeira entrevista de emprego, pensou na roupa
                    que talvez usaria e escolheu o melhor sapato? Com certeza sim, mas também não deixou em segundo
                    plano sua aparência física, arrumando o cabelo, a maquiagem e sim, dando aquela conferida na limpeza
                    oral. Um sorriso é capaz de garantir sua vaga de emprego, pode apostar. E tem mais: uma boca cheia
                    de saúde com dentições bonitas e alinhados é um grande diferencial no mercado de trabalho. </p>
                <p>Dentro das empresas, o plano de saúde dentário tem este principal objetivo: fornecer procedimento
                    correto assim que os colaboradores sentirem necessidade. Além do mais, ter acesso à vantagem faz com
                    que a ida ao dentista se torne algo natural. Quanto mais as consultas se tornarem regulares, menos
                    os trabalhadores irão sofrer por razão de problemas orais provocados por cáries, infecções
                    (abscessos), gengivites, e outros. </p>
                <p>Durante as consultas do plano de saúde dentário outros empecilhos relacionados à saúde oral podem ser
                    diagnosticados. Você já deve ter convivido com algum colega que vivia se queixando de dores de
                    cabeça e enxaquecas. Ele ia ao médico, fazia exames, e nada de descobrir os motivos. Voltava ao
                    trabalho e as reclamações continuavam, inclusive com faltas e desmotivação para cumprir as demandas
                    do dia a dia. Às vezes, basta uma consulta odontológica para seu colega descobrir que as dores de
                    cabeça eram resultado da posição incorreta da arcada dentária ou com empecilhos noturnos como
                    bruxismo o costume de ranger as dentições durante à noite, enquanto dorme. Algumas consultas depois
                    e pronto, o dilema foi resolvido e a paz voltou a reinar no ambiente de trabalho.</p>
                <p>Quanto mais as pessoas se sentem motivadas, mas correspondem às expectativas. Dentro dos ambientes de
                    trabalho, ao oferecer ao colaborador a vantagem de um plano de saúde dentário, e ampliar o uso
                    também a seus familiares, os gestores acabam gerando uma onda de gratidão. Para os colaboradores é
                    uma maneira de ganhar destaque no mercado competitivo, atraindo novos talentos e mão de obra
                    especializada. Já para os funcionários a recompensa pelo trabalho bem feito e pelas horas de
                    dedicação: uma troca que dá certo e tem gerado resultados satisfatórios para ambos os lados. </p>
                <h2>Autoestima lá em cima reflete na empresa e no colaborador</h2>
                <p>Pessoas que trabalham felizes, garantem mais produtividade às operadoras . E o acesso ao plano de
                    saúde dentário empresarial é uma das causas que fazem os funcionários sorrirem. Com a garantia de
                    cobertura odontológica, o trabalhador se sente seguro para fazer melhor as demandas do dia a dia,
                    pois sabe que pode contar com a empresa se um dente quebrar, ou surgir uma doença bucal. </p>
                <p>Uma boa imagem corporativa é verificado pela maneira com que os colaboradores de uma empresa se
                    comportam, afinal, eles vestem a camisa das organizações e são o carro-chefe, tanto para
                    concorrentes como para consumidores. Por isso, ao investir no sorriso dos colaboradores, a empresa
                    está apostando alto no seu principal cartão de visitas, as pessoas. Vale a pena analisar no mercado
                    os tipos de plano de saúde dentário disponíveis e encontrar um que melhor adeque-se à realidade de
                    seus funcionários. Os planos são bastante versáteis e se encaixam no estilo de cada organização.
                </p>
                <h2>Doenças orais x plano de saúde dentário</h2>
                <p>Para manter a saúde bucal os indivíduos precisam adotar hábitos simples de higiene, como escovar os
                    dentes diariamente após as refeições e fazer uso do fio dental. Para complementar os cuidados feitos
                    em casa, os indivíduos devem frequentar o dentista periodicamente, seja na rede de assistência
                    pública, por meio de plano de saúde dentário ou ainda, nos consultórios particulares. Essa rotina de
                    cuidados com a boca irá evitar dilemas mais sérios na boca, como a cárie dental (que interfere
                    diretamente na saúde do dente) e periodontite (doença que compromete os tecidos ao redor dos
                    dentes). </p>
                <p>Quando há ausência de limpeza bucal ou até mesmo escovação inadequada, o acúmulo de bactérias que
                    ficam na superfície da dentição geram as famosas cáries. Aos poucos, a patologia gera a dissolução
                    dos minerais do esmalte dental e, quanto mais os usuários demoram a recorrer ao dentista, mais o
                    problema se agrava causando infecções. </p>
                <p>Dessa forma, os especialistas insistem: ter acesso a um plano de saúde dentário é uma boa coisa para
                    quem quer manter a rotina de consultas, evitando que uma simples cárie se transforme em um problema
                    grave. Assim como a cárie, a patologia periodontal também é causada por bactérias que se concentram
                    na gengiva e no dente. E de novo, a situação pode se agravar se logo no início o paciente não
                    recorrer a um atendimento odontológico. </p>
                <h2>Cuidados orais e prevenção</h2>
                <p>Mas uma coisa é fato: quanto mais as pessoas cuidam das dentições, fazendo a prevenção em casa ou
                    durante as consultas no plano de saúde dentário, mais ficarão livres de patologias orais e exibirão
                    um sorriso lindo e deslumbrante por onde passarem. Para ter este sorriso não é difícil. Veja essa
                    pequena lista de práticas de higiene diárias e simples que podem transformar sua boca hoje mesmo:
                </p>
                <ul>
                    <li>
                        <p>escovar os dentes pelo menos três vezes ao dia, com escova de cerdas macias e arredondadas;
                        </p>
                    </li>
                    <li>
                        <p>tenha cuidado com os procedimentos estéticos, especialmente com o clareamento dental feito
                            fora dos consultórios. Este método pode causar sensibilidade excessiva, comprometendo a
                            saúde da gengiva;</p>
                    </li>
                    <li>
                        <p>escolha creme dental com flúor;</p>
                    </li>
                    <li>
                        <p>use fio dental diariamente;</p>
                    </li>
                    <li>
                        <p>use o enxaguatório bucal para complementar a higiene e ainda ajudar a reduzir o mau hálito;
                        </p>
                    </li>
                    <li>
                        <p>não esqueça de fazer a higiene da língua com escova dental ou limpadores específicos;</p>
                    </li>
                    <li>
                        <p>mantenha uma alimentação saudável, reduzindo o consumo de açúcar, principalmente entre as
                            refeições;</p>
                    </li>
                    <li>
                        <p>preste atenção na saúde da gengiva: ao escovar os dentes ou usar fio dental, se houver
                            sangramento, procure logo o atendimento odontológico, pois pode ser sintoma de gengivite
                            (inflamação na gengiva);</p>
                    </li>
                    <li>
                        <p>lição de casa feita com sucesso, não deixe de visitar o dentista regularmente, pelo menos a
                            cada seis meses.</p>
                    </li>
                </ul>
                <h2>Plano de saúde dentário: sorria mais e tenha mais bem-estar</h2>
                <p>Os cuidados com a saúde bucal deve iniciar ainda na infância. Até mesmo antes da formação da primeira
                    dentição de leite, os pais precisam iniciar a higienização da gengiva, fazendo massagens e limpezas
                    diárias, que preparam a boca para receber a dentição. Depois, assim que as dentinas permanentes
                    nascem, os cuidados devem ser redobrados, com as consultas regulares ao dentista e as práticas de
                    higiene bucal feitas com atenção e da maneira correta. Confira mais benefícios do plano de saúde
                    dentário e não demore para investir no seu bem-estar:</p>
                <ul>
                    <li>
                        <p>foco no tratamento preventivo;</p>
                    </li>
                    <li>
                        <p>melhora da relação entre profissional e paciente;</p>
                    </li>
                    <li>
                        <p>consultas com profissionais especializados;</p>
                    </li>
                    <li>
                        <p>rede de dentistas e clínicas credenciadas;</p>
                    </li>
                    <li>
                        <p>atendimento de emergência e urgência 24 horas;</p>
                    </li>
                    <li>
                        <p>agendamento das consultas de forma rápida;</p>
                    </li>
                    <li>
                        <p>preço acessível;</p>
                    </li>
                    <li>
                        <p>planos personalizados.</p>
                    </li>
                </ul>
                <p>Ter dentições saudáveis pode até ser um privilégio de poucos, mas a verdade é que todos conseguimos
                    recuperar a saúde da cavidade bucal com transformações diárias. Além de se atentar aos cuidados com
                    a saúde oral, contratar um plano de saúde dentário é um método que gera retorno em curto e médio
                    prazo, já que garante acesso de toda a família a atendimentos odontológicos de qualidade. </p>
                <p>Hoje em dia, o número de indivíduos que recorre ao plano de saúde dentário aumentou. De acordo com
                    dados do Instituto Brasileiro de Geografia e Estatística (IBGE), cerca de 23 milhões de
                    beneficiários utilizam os serviços no Brasil, e a tendência é este índice cresça ainda mais em 2020.
                    A transformação significa um novo olhar frente à saúde oral: a sociedade passou a compreender que
                    vale muito mais a pena investir em atendimento especializado do que aguardar a patologia se instalar
                    e ter que sofrer com procedimentos, muitas vezes, dolorosos e demorados. </p>
                <h2>Sorrir, com um sorriso saudável, faz parte da saúde de todo o organismo</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>E faz tempo que deixou de ser apenas um ponto de estética. Nos dias de hoje, ter uma boca bonita é
                    significado de indivíduo que se cuida, que se ama e sabe se valorizar. Por isso, seja dentro do
                    ambiente de trabalho, com acesso aos convênios corporativos, ou nos familiares, com o plano de saúde
                    dentário individual ou familiar, optar pela contratação deste serviço é cada vez mais comum e
                    viável. </p>
                <p>Com preços que cabem no bolso da maioria das pessoas é possível conquistar aquele sorriso de gerar
                    inveja. Antes de fechar o negócio, pesquise sobre as operadoras no mercado, e escolha um plano de
                    saúde dentário que se encaixe o seu perfil e de sua família. Um sorriso bonito abre portas no
                    mercado de trabalho, aproxima as pessoas e melhora as relações. Não deixe sua qualidade de vida para
                    trás: pense no seu sorriso com carinho e invista no que é mais essencial: você e sua família.</p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>