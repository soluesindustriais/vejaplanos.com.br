<?php
include('inc/vetKey.php');
$h1 = "plano dental";
$title = $h1;
$desc = "Com o plano dental é possível ter maior economia no balanço geral Assim como acontece com as consultas médicas privadas, as idas ao especialista";
$key = "plano,dental";
$legendaImagem = "Foto ilustrativa de plano dental";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Com o plano dental é possível ter maior economia no balanço geral</h2>
                <p>Assim como acontece com as consultas médicas privadas, as idas ao especialista pagando por cada
                    tratamento resulta em um gasto enorme ao final do procedimento. Além do dinheiro investido com a
                    intervenção odontológica, por fora ainda são feitos outros exames para dar continuidade com mais
                    segurança, como o raio-x, limpeza, entre outros trabalhos. Por esse motivo, os honorários de
                    dentista privados ultrapassam com facilidade o preço de uma consulta médica.</p>
                <p>Quando inserido na balança ao final do tratamento todos os gastos realizados, é considerável pensar
                    nas vantagens da contratação de um plano dental e assim otimizar a diminuição de gastos e as
                    qualidade da saúde oral.</p>
                <p>Diferente das práticas em consultas particulares, no plano dental é realizado um pagamento todo mês
                    ou apenas todo ano em que todos os procedimentos e intervenções cirúrgicas estão inclusos. Grande
                    parte dos indivíduos que ficam sabendo desse trabalho não o adquirem por ser um pagamento mensal,
                    mas se analisado a fundo, o dinheiro investido em um único procedimento, pode facilmente ultrapassar
                    o pagamento de mais de um ano do plano dental. Entre as vantagens dessa contratação, estão:</p>
                <ul>
                    <li>
                        <p>Clínicas de urgência 24h para pronto atendimento;</p>
                    </li>
                    <li>
                        <p>Teleatendimento 24h por dia;</p>
                    </li>
                    <li>
                        <p>Aprovação online dos tratamentos cobertos;</p>
                    </li>
                    <li>
                        <p>Consultas com médicos capacitados e de confiança;</p>
                    </li>
                    <li>
                        <p>Não há limite de utilização pelo convênio odontológico;</p>
                    </li>
                    <li>
                        <p>Carência de 24h nos pagamentos no cartão de crédito ou boleto anual.</p>
                    </li>
                </ul>
                <p>Em grande parte das vezes, os profissionais são analisados apenas em fases de emergências, quando há
                    inflamações, dores e infelizmente em casos que apenas as restaurações não dão conta de resolver as
                    problemáticas. Por isso, o acompanhamento, a avaliação e o cuidado contínuo que o plano dental
                    fornece, contribui para um sorriso saudável e uma melhor qualidade de vida.</p>
                <p>Há pouco tempo o plano dental se restringia apenas aos especialistas que recebiam essa vantagem
                    incluso no regime contratual. A prática dessa contratação, além de ter melhor custo-benefício para
                    as operadoras, contribui para diminuir as faltas dos colaboradores em função de algum problema de
                    saúde oral, uma vez que notaram que apenas o convênio médico não bastava e necessitavam de um
                    convênio que também atendesse a saúde bucal.</p>
                <h2>As pesquisas falam sobre este trabalho</h2>
                <p>De acordo com o Instituto Brasileiro de Geografia e Estatística (IBGE) e a Agência Nacional de Saúde
                    (ANS) cerca de vinte e dois milhões de pessoas possuem o plano dental no Brasil, sendo que 73,8%
                    estão concentrados em planos contratados por operadoras que tem como benefício para o colaborador
                    esse serviço. Os números parecem altos, mas são inferiores se comparado aos usuários de planos
                    médico-hospitalares: 48 milhões de indivíduos. Já número de pacientes em planos privados,
                    exclusivamente plano dental, é de 12,1% da população, uma crescente de mais de 100% em relação a
                    2009, no qual apenas 5,1% da população tinha acesso a esse benefício.</p>
                <p>No entanto, o trabalho ainda é um bem supérfluo na hora de cortar gastos e pode ser considerado
                    indispensável para alguns funcionários. Ainda assim, o convênio plano dental vem se mostrando cada
                    vez mais essencial: de acordo com um levantamento feito pela ANS, em um período de 8 anos a
                    contratação de planos odontológicos individuais aumentaram 500%. É real a importância por esse
                    serviço, que tem uma crescente considerável ao longo dos anos, por uma questão de diminuição de
                    gastos e também de segurança e qualidade de vida. </p>
                <h2>Você sabe como funciona o plano dental nas empresas?</h2>
                <p>O plano dental pode ser contratado por companhias que oferecem um plano de saúde dentária para os
                    funcionários. E com o mundo corporativo cada vez mais acirrado, um convênio dentário é um grande
                    diferencial da operadora e um ótimo impulsionador de produtividade dos colaboradores, quando se está
                    buscando novos talentos para integrar um time, ou busca incentivar funcionários que já fazem parte
                    da equipe.</p>
                <p>Não só o plano dental, outros benefícios como o convênio médico tradicional, vale-transporte,
                    vale-alimentação, vale-refeição e vale-cultura podem ser alguns dos diferenciais em relação à
                    concorrência no instante em que um candidato escolhe a vaga que ele pretende concorrer. E a
                    companhia é responsável por fazer a busca de convênios, preços e pacotes que sejam atraentes para os
                    funcionários.</p>
                <p>Os benefícios de se contratar um plano dental para empresas é amplo e traz vantagens tanto para o
                    colaborador quanto para o empregador. As transformações que esse serviço pode fazer tem impacto
                    direto no cotidiano do colaborador e também é sentido no bolso e no rendimento do trabalho, pois ele
                    gera:</p>
                <ul>
                    <li>
                        <p>Probabilidade menor de ocorrências de faltas decorrentes de problemas dentários, assim haverá
                            um maior acompanhamento;</p>
                    </li>
                    <li>
                        <p>Mais satisfação por parte do funcionário, que se sentirá mais valorizado e protegido pela
                            empresa;</p>
                    </li>
                    <li>
                        <p>Melhor custo-benefício para a empresa e funcionário, já que é trabalhado a prevenção de
                            doenças;</p>
                    </li>
                    <li>
                        <p>Qualidade de vida, preservando-se de futuros problemas envolvendo a saúde bucal.</p>
                    </li>
                </ul>
                <p>Quem trabalha em empresas que contam com esse benefício sabe a vantagem de poder contar com o plano
                    dental, isso porque muitas vezes os problemas de saúde podem estar relacionados aos casos
                    odontológicos. As dores geradas pela falta de cuidado adequado com os dentes podem ser
                    enlouquecedoras e chegar a afastar um colaborador por dias, dependendo da situação.</p>
                <p>É válido dizer também que as bactérias que geram problemas na boca podem atingir o corpo como um
                    todo. Existem diversos casos de usuários hospitalizados pela falta de acompanhamento, que pela fobia
                    de uma intervenção odontológica acabam por sofrer danos maiores, ou seja, o plano dental passa longe
                    de ser um capricho e se torna um bem essencial completamente ligado à qualidade vida e para evitar
                    empecilhos agravados a saúde e a vida.</p>
                <p>Os benefícios do plano dental vão além da saúde, apesar do principal foco desse trabalho ser atender
                    demandas ligadas a saúde bucal dos colaboradores de uma determinada instituição, os benefícios vão
                    muito além de tudo isso, com o começo de possíveis tratamentos e acompanhamentos mais recorrentes,
                    os colaboradores acabam conquistando uma maior autoestima e confiança, que resultam diretamente no
                    dia a dia dentro e fora da empresa.</p>
                <h2>Razões para adquirir um plano dental</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Só quem já sofreu com dor de dente compreende a necessidade de ter plano dental com clínicas de
                    confiança que possa contar. O cuidado não envolve apenas o tratamento odontológico como uma
                    restauração, obturação, canal, etc. Mas também o recepcionamento do odontologista, que precisa estar
                    atento aos pequenos detalhes dos sintomas apresentados pelo paciente. As orientações deste
                    profissional é que vão guiar os indivíduos que chegam a clínica necessitam de cuidados e diminuir a
                    frustração e fobia que diversas pessoas têm desses tratamentos.</p>
                <p>Outro ponto extremamente essencial é entender que buscar por um odontologista em um momento de
                    desespero, quando se está com muita dor, não significa que o paciente sairá de lá completamente
                    curado e em um passe de mágica tudo irá melhorar, pelo contrário. Há casos que sim, é possível ser
                    tratado de imediato, mas têm situações mais delicadas que exigem procedimentos mais avançados como
                    um canal, retratamento de canal, extração, intervenções cirúrgicas, etc.</p>
                <p>Em alguns desses momentos, as fortes dores se dão por conta de uma inflamação, que se não cuidadas
                    antes de qualquer intervenção com o plano dental, é basicamente impossível ser realizado alguma ação
                    que melhore o quadro. Isso porque a região inflamada não deixa a anestesia agir adequadamente,
                    gerando:</p>
                <ul>
                    <li>
                        <p>Fortes dores;</p>
                    </li>
                    <li>
                        <p>Correr risco de qualquer agravamento de saúde, pois é uma forma irresponsável de se tratar;
                        </p>
                    </li>
                    <li>
                        <p>Incômodo na hora do tratamento;</p>
                    </li>
                    <li>
                        <p>Desconforto.</p>
                    </li>
                </ul>
                <p>O raio-x é um grande parceiro dos especialistas e que colabora para identificar qualquer lesão gerada
                    na dentição e qual a região específica do procedimento, além de guiar o especialista a saber qual
                    tratamento o usuário precisa. Esse serviço geralmente tem o custo baixo e é realizado poucas vezes,
                    geralmente antes de qualquer procedimento quando o beneficiário chega, após a realização de qualquer
                    procedimento e após no acompanhamento para saber se está tudo bem.</p>
                <h2>Veja algumas coberturas</h2>
                <p>O plano dental cobre esses trabalhos e o principal motivo que faz com que as pessoas e empresas
                    firmam este contrato é a manutenção da saúde bucal e a qualidade de vida. Entre os procedimentos
                    cobertos pelo convênio odontológico, estão:</p>
                <ul>
                    <li>
                        <p>Urgências/ Emergências;</p>
                    </li>
                    <li>
                        <p>Raio-X;</p>
                    </li>
                    <li>
                        <p>Consultas;</p>
                    </li>
                    <li>
                        <p>Restauração;</p>
                    </li>
                    <li>
                        <p>Cirurgias e extrações (incluindo siso);</p>
                    </li>
                    <li>
                        <p>Tratamento de gengiva;</p>
                    </li>
                    <li>
                        <p>Limpeza, prevenção e aplicação de flúor;</p>
                    </li>
                    <li>
                        <p>Tratamento de canal;</p>
                    </li>
                    <li>
                        <p>Próteses;</p>
                    </li>
                    <li>
                        <p>Ortodontia.</p>
                    </li>
                </ul>
                <p>O acompanhamento com periodicidade com o plano dental e a higienização adequada pode evitar a dor de
                    dente e a temida cárie, que leva a intervenções odontológicas como restauração e obturação. Nesse
                    sentido o plano dental colabora para a manutenção da saúde das dentições e evita que as cáries façam
                    parte da composição dentária e que isso se torne um problema.</p>
                <h2>Restauração
                    <!--EndFragment--> dental </h2>
                <p>Para quem não sabe, a restauração se faz essencial quando a dentição foi atingido pela cárie e gera o
                    risco de sofrer outras complicações. Os sintomas de cárie na dentição pode não ser apenas a dor, o
                    incômodo aponta sinais com o tempo, e entre eles estão:</p>
                <ul>
                    <li>
                        <p>Manchas amareladas;</p>
                    </li>
                    <li>
                        <p>Danificar por completo o dente e atingir a polpa;</p>
                    </li>
                    <li>
                        <p>Abscessos;</p>
                    </li>
                    <li>
                        <p>Pontos pretos;</p>
                    </li>
                    <li>
                        <p>Pus;</p>
                    </li>
                    <li>
                        <p>Inchaço e vermelhidão na gengiva, que se não tratado pode avançar para uma infecção.</p>
                    </li>
                </ul>
                <p>A prevenção para não ter que realizar a restauração de cárie na dentição pode ser bem básica,
                    iniciando pela escovação diária, lembrando que esse costume deve incluir a ação após todas as
                    refeições feitas, a essencialidade do fio dental é fundamental para não acumular restos de comida e
                    desenvolver um tártaro ou cárie, higienização da língua, gengiva e céu da boca, boa alimentação e
                    acompanhamento clínico.</p>
                <h2>A manutenção é essencial e pode ser realizada de forma muito prática com um plano dental</h2>
                <p>O sorriso é visto como o cartão postal de qualquer pessoa, então cuidar desse bem tão especial
                    acarreta vantagens para a saúde, mas especialmente a autoestima. Isso porque muitos indivíduos
                    deixam de sorrir por não se sentirem à vontade com seus dentes. Então no quesito da correção,
                    tratamento e manutenção, não ficam dúvidas de que o plano dental é uma ótima opção. O plano dental
                    além de possibilitar sorrisos largos e saudáveis, mantém a regularidade dos cuidados e não permite
                    que o paciente chegue em caso extremo de dor.</p>
                <p>Em um mundo movido a registros fotográficos com amigos e selfies, as dentições visivelmente não
                    saudáveis conseguem ser um verdadeiro incômodo para quem o tem. O sorriso de uma pessoa diz muito
                    sobre ela e qualquer empecilho que ocorre nesse local afeta diretamente a autoestima e a forma como
                    ela se apresenta, mas esse problema tem solução com o plano dental e claro, fazendo bom uso dele.
                </p>
                <p>Manchas no esmalte dentário é um problema que pode ter solução e que é muito buscado por pacientes
                    que sentem envergonhados com esse diagnóstico. Vale lembrar que é comum o desgaste natural da camada
                    de esmalte que envolve a coroa da dentição ao decorrer da vida, pois é uma propensão natural do
                    corpo. Mas, existem métodos preventivos que podem retardar esse acontecimento. A higienização oral
                    com frequência é um desses procedimentos a ser seguido em casa.</p>
                <h2>Responsáveis pela saúde bucal</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Um dos grandes responsáveis de problemas ligados à saúde bucal estão relacionados a higienização
                    inadequada na dentição e também se dá por costumes alimentares como: bebidas, comidas, medicação e
                    algumas misturas podem reagir e ocasionar a coloração amarelada ao longo do tempo e a danificação do
                    esmalte do dente, sendo assim, todo cuidado é pouco e o acompanhamento usado o plano dental é
                    primordial.</p>
                <p>É essencial compreender que a carga genética tem influência direta na evolução e alterações que a
                    dentina sofre ao decorrer da vida, mas que alguns costumes frequentes e que às vezes os indivíduos
                    nem se dão conta, contribuem que a dentina fique danificada, como o consumo de café, alguns
                    refrigerantes com excesso de corante, antibióticos, que tem uma quantidade exagerada de flúor e
                    também chás. Os fumantes também devem ficar alerta, esse é o público que mais costuma buscar auxílio
                    com o plano dental.</p>
                <p>As causas podem ser as mais diversificadas, mas em todos os casos geram incômodo e certo desespero.
                    Hoje em dia é fácil encontrar receitas caseiras que prometem milagre em minutos. É importante ter
                    consciência de que determinadas misturas realizadas sem prescrição médica adequada pode acabar
                    piorando a saúde e o estado da dentina. O mais adequado é procurar um plano dental e entender qual o
                    método e acompanhamento a seguir. Existem cuidados básicos que podem evitar problemas dentários e
                    entre eles estão:</p>
                <ul>
                    <li>
                        <p>A orientação é que a higiene ocorra após todas as refeições realizadas;</p>
                    </li>
                    <li>
                        <p>Escovar os dentes pelo menos três vezes ao dia;</p>
                    </li>
                    <li>
                        <p>Diminuir consumo de café e do tabaco, pois esses são os principais alvos de alguns
                            diagnósticos;</p>
                    </li>
                    <li>
                        <p>Procurar tratamento orientado pelo dentista com o plano dental.</p>
                    </li>
                </ul>
                <p>Manter o acompanhamento correto é essencial, levando em consideração o avanço tecnológico e a
                    segurança que o plano dental pode oferecer para sanar problemas relacionados à saúde oral.</p>
                <p>Nesse contexto, e para finalidade do cuidado contínuo, o plano dental geralmente não possui limite de
                    utilização aumentando a frequência que o usuário vai à clínica e a importância com a saúde oral, que
                    é essencial para uma melhor qualidade de vida.</p>
                <h2>Com um plano dental correto é possível evitar problemas no futuro</h2>
                <p>A visita ao consultório odontológico por meio do plano dental deve ser um costume praticado desde a
                    infância, assim como os exames de rotina que são importantes. Um check-up para analisar a saúde oral
                    pode contribuir muito para evitar procedimentos invasivos que podem levar até as intervenções
                    cirúrgicas ou extração de um dente.</p>
                <p>Quando o paciente aciona o plano dental com a necessidade de fazer um procedimento, os riscos podem
                    ser ainda maior do que ele pensa, essa busca pode vir acompanhada de outros sintomas, como:</p>
                <ul>
                    <li>
                        <p>Em casos mais leves, de início da cárie, é possível o tratamento apenas com a restauração;
                        </p>
                    </li>
                    <li>
                        <p>Dores no dente, nesses casos a intervenção médica não é possível, o tratamento deve começar
                            via medicação oral e após amenizar a dor, é possível realizar o procedimento indicado;</p>
                    </li>
                    <li>
                        <p>Em casos mais delicados e que envolve outros tratamentos, certamente há o comprometimento da
                            estrutura do dente;</p>
                    </li>
                    <li>
                        <p>A inflamação da polpa do dente, também é um grande problema, sendo necessária a intervenção
                            de um canal, ou em casos extremos, a extração.</p>
                    </li>
                </ul>
                <h2>Saiba como manter a saúde bucal</h2>
                <p>Esse levantamento mostra o quão complicado podem ser os tratamentos e as dores sofridas pela falta de
                    um acompanhamento feito com o plano dental. O custo é mensal e as visitas aos consultórios que fazem
                    parte desse contrato são ilimitadas.</p>
                <p>Não existe segredo para manter a saúde bucal sempre em dia: higienização todos os dias de forma
                    correta e acompanhamento com dentista de confiança. Tem muitos casos que o plano dental possui
                    clínicas integradas espalhadas pelo Brasil inteiro para realizar o melhor atendimento e proporcionar
                    sorrisos verdadeiros:</p>
                <ul>
                    <li>
                        <p>Plano dental adequado ao perfil de cada paciente, levando em consideração faixa etária e
                            condições financeiras;</p>
                    </li>
                    <li>
                        <p>O atendimento emergencial 24h;</p>
                    </li>
                    <li>
                        <p>Gratuidade em exames, consultas e cirurgias cobertas pelo seu plano;</p>
                    </li>
                    <li>
                        <p>Atendimento personalizado e segurança.</p>
                    </li>
                </ul>
                <p>Contar com um plano dental pode oferece muitos benefícios que não é encontrado em clínicas
                    particulares, como economizar com os gastos com as consultas, além de exames e procedimentos
                    cobrados separadamente. Além disso, o atendimento médico oferecido nesses serviços costuma ser mais
                    rápido, eficiente e com mais qualidade.</p>
                <p>Mesmo quando é inserido na balança o custo desembolsado pelo preço do serviço, o plano dental ainda
                    se mostra uma alternativa perfeita para quem se preocupa com a saúde bucal. Isso porque há uma
                    variedade de planos que se adequam a cada perfil, garantindo tranquilidade para quando necessitar de
                    recepcionamento médico, por um custo dentro da realidade financeira.</p>
                <h2>Ter um recepcionamento adequado só é possível com plano dental!</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Diversas vantagens incluem o plano dental, a começar pela economia financeira em fazer exames de
                    rotina, diminuir gastos com emergências, além de que o convênio pode prevenir uma série de
                    acontecimentos. Com a aquisição do plano dental, o essencial é o início do acompanhamento ideal, já
                    que o recomendado é sempre estar atento ao corpo, existem doenças que surgem silenciosamente, por
                    isso é recomendável que o costume de realizar um check-up se torne rotina a cada determinado
                    período, solicitando exames para conferir a saúde bucal e viver uma vida saudável, sem dor e em
                    segurança.</p>
                <p>Entre os serviços que o plano dental cobre, estão: clínicas de qualidade com pronto atendimento;
                    cobertura de exames; consultas com médicos especialistas, com atendimento de qualidade e em algumas
                    situações, que não dispõe de tempo de carência para utilizar os trabalhos, etc. São muitos os planos
                    disponíveis no mercado, inclusive clínicas que atendem em todas as localidades. Sendo assim, o
                    corretor será o responsável por mostrar as melhores opções que se enquadre ao perfil do contratante.
                </p>
                <p>Geralmente, o plano dental realizado de forma individual é contratado se a empresa não disponibiliza
                    desse trabalho como vantagem e também em casos que o filho dispunha do plano como adicional dos pais
                    e excede a idade limite que a empresa determina, dessa maneira é importante pensar em alternativas,
                    e o plano de saúde pessoa física é uma delas.</p>
                <p>A alternativa de contratar um plano dental é ter a segurança do recepcionamento e de que o problema
                    será resolvido ou cuidado por especialistas capacitados. As emergências que ocorrem faz com que
                    gastos inesperados apareçam e a opção por clínicas particulares pode levar a dívidas imensas:</p>
                <ul>
                    <li>
                        <p>Medicação;</p>
                    </li>
                    <li>
                        <p>Exames;</p>
                    </li>
                    <li>
                        <p>Tempo de observação;</p>
                    </li>
                    <li>
                        <p>Consultas;</p>
                    </li>
                    <li>
                        <p>Retorno;</p>
                    </li>
                    <li>
                        <p>Procedimento cirúrgico.</p>
                    </li>
                </ul>
                <p>Em casos odontológicos, a emergência pode se manifestar no período da noite, então, poder contar com
                    plano dental que disponha de serviço 24h e que não apareça com surpresas, como preços adicionais ao
                    final do mês, sem contar com a segurança e atendimento de qualidade, pode ser muito viável. Essa
                    opção de serviço geralmente é contratada fora dos planos empresariais.</p>
                <p>Outro ponto que muitas pessoas enfrentam problema, é de ter um plano dental que inclua atendimento em
                    outros estados, em caso de viagem a passeio, ou a trabalho que ocorra qualquer tipo de emergência
                    que necessite de um atendimento com comprometimento, seguindo a mesma linha de procedimento que o
                    usuário já está habituado em sua região.</p>
                <p>O alto padrão em todas as clínicas que possuem o plano dental deve ser uma exigência feita ao
                    corretor que for fechar o contrato, afinal o intuito será sempre estar rodeado de profissionais
                    capacitados com as melhores alternativas de tratamentos e que sarão continuidade no plano de
                    melhorias que está sendo realizado pela clínica que está sendo atendido em sua localidade.</p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>