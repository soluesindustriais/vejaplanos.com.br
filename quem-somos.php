<?php
$h1 = "Quem Somos";
$title = $h1;
$desc = "A Ideal Odonto nasceu por meio da experiência de um grupo consolidado há 20 anos no mercado, composto por 12 empresas de diferentes segmentos";
include('inc/head.php');
?>

<body>

    <?php include 'inc/header.php' ?>
    <main>
        <div class="container my-5">
            <div class="row">
                <div class="col-md-12 mb-5 text-center">
                    <div class="row align-items-center">

                        <div class="col-md-6">
                            <p>
                                A Ideal Odonto é seguradora de três opções de planos odontológicos que possuem o
                                objetivo de atender todas as pessoas, independente da idade.


                            </p>
                            <p>
                                A empresa tem o objetivo de ser a maior aquisitora de parcerias do Brasil. Além disso,
                                tem a missão de trazer a independência aos seus clientes, ou seja, possibilitar que eles
                                escolham a clínica e quais profissionais desejam ser atendidos.





                            </p>
                            <p>
                                A Ideal Odonto quer presentear seus clientes, com seus melhores sorrisos.


                            </p>
                        </div>
                        <div class="col-md-6">
                            <img src="assets/img/aboutus1.png" alt="Ilustração de dois dentistas" class="img-fluid">
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <img src="assets/img/aboutus2.png" alt="Ilustração de dois dentistas" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                            <p class="text-center">A missão da Ideal Odonto é ser a melhor seguradora de planos
                                odontológicos, além disso, quer oferecer os melhores tratamentos odontológicos aos seus
                                clientes e proporcionar os melhores sorrisos, por meio da odontologia moderna.


                            </p>

                            <p class="text-center">
                                A maior missão da empresa é facilitar o acesso aos planos odontológicos e ajudar
                                milhares de brasileiros a cuidar de seus sorrisos sem complicações.




                            </p>


                        </div>
                    </div>

                </div>


            </div>

        </div>
    </main>
    <?php include 'inc/footer.php' ?>
</body>

</html>