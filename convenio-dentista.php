<?php
include('inc/vetKey.php');
$h1 = "convenio dentista";
$title = $h1;
$desc = "Com o convenio dentista você terá muitas vantagens a curto, médio e longo prazo Possuir dentições perfeitas e alinhados é o foco de muitos indivíduos";
$key = "convenio,dentista";
$legendaImagem = "Foto ilustrativa de convenio dentista";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Com o convenio dentista você terá muitas vantagens a curto, médio e longo prazo</h2>
                <p>Possuir dentições perfeitas e alinhados é o foco de muitos indivíduos que priorizam o cuidado com a
                    aparência. Para isso, o ideal é procurar ajudar no convenio dentista, já que o dentista é o
                    especialista mais recomendado para manter a saúde oral de qualidade ao mesmo tempo em que fornece um
                    sorriso esteticamente agradável. </p>
                <p>Ao longo de toda a vida, os indivíduos podem desenvolver problemas ou enfermidades orais que
                    necessitam de ajuda profissional com um especialista verdadeiramente adequado e qualificado, que
                    passe confiança aos pacientes. Se não forem tratadas da forma ideal, essas doenças podem se
                    desenvolver, causando o enfraquecimento ou a perda parcial ou total dos dentes. Com isso, mostra-se
                    necessário dar uma atenção maior para a região bucal, não somente por motivos estéticos como também
                    por questão de saúde.</p>
                <p>Visitar consultórios odontológicos é um momento que faz parte da necessidade básica do ser humano,
                    até porque uma saúde oral de qualidade é o que permite aos indivíduos a fazer de atividades simples
                    e complexas, desde mastigar até trabalhar. Sendo assim, em busca de maior comodidade e facilidade,
                    os indivíduos têm buscado cada vez mais a adesão a algum convenio dentista.</p>
                <h2>Categorias do convenio dentista</h2>
                <p>De acordo com uma pesquisa, acredita-se que mais de 25 milhões de indivíduos no Brasil já contam com
                    algum tipo de convenio dentista. Segundo a Agência Nacional de Saúde (ANS), esse número pode saltar
                    para cinquenta milhões em apenas cinco anos. Sendo assim, conforme o passar dos anos, os indivíduos,
                    cada vez mais, vão atrás de tratamento em clínicas que atendam o convenio dentista que contrataram.
                </p>
                <p>Existem várias vantagens para os indivíduos que procuram contratar um bom convenio dentista, como os
                    custos mais baixos que os consultórios privados e a ampla cobertura de tratamentos, tanto eletivos
                    como na urgência. Por isso, é essencial analisar muito a respeito e escolher o convênio que forneça
                    o melhor custo-benefício.</p>
                <p>Nesse quesito, existem diversas modalidades do convenio dentista, voltados para diferentes
                    necessidades, bolsos e gostos. Sendo assim, podem ser escolhidas as seguintes modalidades:</p>
                <ul>
                    <li>
                        <p>Convenio dentista familiar: nesse método, o beneficiado pode incluir filhos, marido, esposa,
                            pai e mãe, lembrando que o plano dentário preço final varia de acordo com a idade, doenças
                            pré-existentes e estilo de vida de cada um dos dependentes. Por isso, recomenda-se colocar
                            na ponta do lápis e analisar se essa modalidade apresenta um custo acessível
                            financeiramente;</p>
                    </li>
                    <li>
                        <p>Convenio dentista empresarial: já nesse caso, o beneficiado não possui qualquer tipo de
                            relação com boletos ou burocracias do plano dentário preço, porque a própria empresa ou
                            alguma empresa terceirizada que se envolve com a responsabilidade relacionada a ele. A
                            contratação é de responsabilidade da empresa pública ou privada, tornando-se bastante
                            econômico e podendo ser descontado diretamente na folha de pagamento do empregado. Também é
                            possível de ser contratado por empresas de pequeno, médio e grande porte;</p>
                    </li>
                    <li>
                        <p>Convenio dentista coletivo por adesão: nesse tipo, os clientes geralmente são sindicatos e
                            associações de determinadas profissões. Logo, a modalidade do plano analisa as
                            características do grupo como um todo e não das pessoas;</p>
                    </li>
                    <li>
                        <p>Convenio dentista individual: esse convênio permite ao indivíduo realizar diferentes tipos de
                            procedimentos que estão cobertos no convênio. O plano odontológico individual é sugerido
                            para quem não deseja incluir outras pessoas e não tem um convênio da empresa. Por outro
                            lado, ele costuma ser um dos tipos de convênios mais caros.</p>
                    </li>
                </ul>
                <h2>Procedimentos disponíveis pelo convenio dentista</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Como todos já sabem, a cobertura dos tratamentos odontológicos varia de acordo com a modalidade
                    selecionada pelo paciente. No entanto, existe uma lista de tratamentos que todos os convênios devem
                    seguir. Pensando assim, estão os tratamentos de baixa complexidade, que incluem a consulta inicial
                    para análise, aplicação de flúor, remoção de tártaro, profilaxia (limpeza), restauração e tratamento
                    de cáries.</p>
                <p>Nos tratamentos de complexidade média, intermediário, estão os curativos e suturas, periodontia
                    (procedimento e cirurgia da gengiva), colagem de fragmentos. Já em relação aos tratamentos de nível
                    alto, que tem mais complexidade, o paciente pode realizar a exodontia (extração dos dentes),
                    endodontia (tratamento de canal), biópsia e cirurgias de pequeno porte. Por fim, a Agência Nacional
                    de Saúde Suplementar (ANS) também estipula que qualquer convenio dentista faça a cobertura de
                    diferentes tipos de radiografia, como a radiografia oclusal, a radiografia periapical e a
                    radiografia bite-wing.</p>
                <p>De maneira geral, os empecilhos e patologias orais podem atingir outros âmbitos do corpo e gerar
                    dores de cabeça, dente e mandíbula, além de dificuldades em funções relacionadas à mastigação, fala
                    e respiração. Nesse sentido, vale a pena contratar o convenio dentista e, assim, passar a cuidar
                    melhor da saúde da boca.</p>
                <p>Através desse tratamento, o usuário pode fazer a higienização, também chamada de profilaxia. Esse
                    tratamento elimina placas bacterianas e placas bacterianas calcificadas, conhecidas também como
                    tártaro. Com isso, a higienização ajuda na manutenção da saúde oral e evita que o beneficiário
                    desenvolva patologias bucais. Em busca de maior eficiência, recomenda-se que a limpeza seja feita,
                    no mínimo, duas vezes por ano.</p>
                <p>O paciente ainda pode fazer a endodontia, conhecida habitualmente como procedimento de canal. Quando
                    a cárie não é solucionada logo no estágio primordial e chega na dentina, ocorre a necessidade de
                    fazer a endodontia. Esse tratamento tem como foco abrir um buraco na dentição, inserir limas, que
                    são pequenas agulhas, e tirar a polpa contaminada. Por isso, o profissional é capaz de realizar a
                    descontaminação e limpeza da região.</p>
                <h2>Periodontia</h2>
                <p>Além do mais, existe também o procedimento de gengiva. A periodontia, também chamada de procedimento
                    de gengiva, serve para desinflamar a região gengival. Com isso, o dentista costuma fazer a
                    higienização nas dentições, eliminando tártaros e placas bacterianas. Em casos no qual o
                    beneficiário apresenta a gengiva muito inflamada, o profissional pode também receitar antibióticos e
                    anti-inflamatório.</p>
                <p>Outro tratamento disponível no convenio dentista é o procedimento de mau hálito. Para eliminar o mau
                    hálito, o procedimento básico no consultório odontológico é realizado um diagnóstico do usuário,
                    baseando-se nos costumes diários, indícios e histórico de patologias. Também podem ser rogados
                    exames clínicos para descobrir se existem doenças na cavidade oral ou pouco fluxo de saliva. Depois
                    de concluir o diagnóstico, o dentista decide qual será o melhor tratamento para o caso, já que não
                    existe um padrão para efetuação do tratamento de mau hálito. </p>
                <h2>Remoção do siso pelo convenio dentista</h2>
                <p>Conhecido também como terceiro molar e/ou dente do juízo, a dentição do siso costuma nascer entre a
                    fase da adolescência e a vida adulta, por volta dos 17 aos 25 anos. Quando ocorre a erupção, a
                    dentição pode passar a inflamar com frequência, fazendo surgir a pericoronarite e, dessa forma,
                    muitos pacientes buscam os consultórios odontológicos para fazer a extração.</p>
                <p>No total, os indivíduos têm dois sisos inferiores e dois superiores, mas nem sempre todos eles
                    nascem. Nesse sentido, são chamados de sisos inclusos, que estão presentes na arcada dentária, mas
                    dentro da gengiva e não ocorre a erupção. Além do mais, eles podem gerar dor e, em algumas
                    situações, torna-se necessário removê-los se eles estiverem fazendo pressão sobre os dentes
                    adjacentes.</p>
                <p>De maneira geral, existem diversas razões para extrair o siso pelo convenio dentista, mas apenas um
                    cirurgião qualificado e de confiança é capaz de analisar e indicar a cirurgia. Nessa linha, os
                    principais fatores que levam o usuário à mesa de cirurgia para retirar o dente do juízo são a
                    pressão, pois o empurramento nos dentes adjacentes ao siso é capaz de provocar o desalinhamento da
                    arcada, criando a necessidade de começar um tratamento odontológico para corrigi-la.</p>
                <h2>Outras curiosidades sobre a remoção do siso</h2>
                <p>Além do mais, existe um motivo da conexão. Os sisos podem encontrar-se conectados com a raiz dos
                    segundos molares, gerando dor e, em alguns casos, a necessidade de fazer a endodontia (procedimento
                    de canal). Outra razão para remover o siso é a pericoronarite. A frequente inflamação do dente do
                    siso é conhecida como pericoronarite. Nesse sentido, o siso não é higienizado da maneira que deveria
                    ser, já que a sua posição dificulta a escovação adequada, então ele passa a inflamar frequentemente
                    por acumular restos de comida e, consequentemente, promover o acúmulo e proliferação de bactérias.
                </p>
                <p>Além do mais, há também a alteração. Afinal, o siso é capaz de prejudicar funções fundamentais do
                    organismo, como a fala, a respiração e a mastigação. Em outros casos, o siso também pode gerar dor.
                    Em alguns indivíduos, a dentição do siso pode causar dores na face, chegando a atingir áreas como
                    mandíbula e ouvido.</p>
                <h2>Saiba todas as vantagens que o convenio dentista pode te proporcionar!</h2>
                <p>Nos dias de hoje, ainda que exista um grande número de especialistas no país, não são todos os
                    indivíduos que acreditam ser importante buscá-los no tempo indicado, que é de, pelo menos, duas
                    vezes por ano. Isso ocorre porque o preço das consultas e procedimentos em clínicas particulares
                    pode ser bem elevado, então o convenio dentista aparece como uma alternativa com atraente
                    custo-benefício para o paciente. </p>
                <p>O cuidado ideal com a saúde bucal deve se tornar um costume na vida de todos os indivíduos, já que
                    afeta o funcionamento de todo o corpo. Como se sabe, uma simples dor de dente pode se agravar em
                    pouco tempo e impedir que o usuário consiga levar a vida geralmente com as atividades diárias. Por
                    isso, com o convenio dentista, torna-se possível prevenir, diagnosticar e tratar empecilhos ou
                    patologias bucais de diferentes gravidades. Nesse contexto, existem diversos benefícios ao
                    contratá-lo, como: </p>
                <ul>
                    <li>
                        <p>Equipe profissional especializada no cuidado e tratamento preventivo dentário infantil; </p>
                    </li>
                    <li>
                        <p>Cobertura em todo o Brasil; </p>
                    </li>
                    <li>
                        <p>Tratamentos preventivos e corretivos;</p>
                    </li>
                    <li>
                        <p>Tempo de carência menor em relação aos planos de saúde em geral;</p>
                    </li>
                    <li>
                        <p>Custo-benefício mais atraente em relação às consultas em consultórios particulares. </p>
                    </li>
                </ul>
                <h2>Saiba quais são os tratamentos estéticos autorizados no convenio dentista</h2>
                <p>Além dos procedimentos cujo foco é cuidar das patologias e problemas orais, como cáries, patologias
                    periodontais, entre outros, existem alguns tipos de convenio dentista que possibilitam fazer
                    tratamentos estéticos. Nesse quesito, existe a ortodontia, que é o sub ramo da odontologia
                    responsável por fazer a movimentação dos dentes e ossos maxilares do paciente, com o objetivo de
                    torná-los mais harmônicos e adequadamente alinhados.</p>
                <p>Essa especialidade é voltada para indivíduos que apresentam dentições tortas ou não-alinhados,
                    gerando prejuízos tanto estéticos quanto funcionais. Nesse sentido, para corrigir os defeitos
                    ortodônticos, podem ser usados diversos tipos de aparelho.</p>
                <p>Além do mais, possuir um sorriso branco é uma meta que pode fornecer confiança e aumento da
                    autoestima para o paciente, além de, inegavelmente, possibilitar dentes mais bonitos. Com esse plano
                    em mente, os especialistas fazem o clareamento dental para oferecerem esses benefícios aos usuários
                    que querem recuperar o aspecto branco dos dentes.</p>
                <p>Por fim, em alguns momentos, existem determinados tipos de conveio dentista que fornece o implante
                    dentário. A perda das dentinas é algo que prejudica não apenas a estética do rosto como também a
                    habilidade de falar, mastigação e respiração, que são fundamentais para o organismo humano. Nesse
                    quesito, os usuários que sofreram uma grande quantidade de perda têm o interesse em saber de algum
                    convenio dentista que cobre implante, já que o tratamento pode ser realizado nos consultórios
                    odontológicos e possibilita a recuperação de um sorriso completo, mas costuma ter um preço elevado.
                </p>
                <h2>Ortodontia pelo convenio dentista</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Além de deformar a estética do sorriso, as dentições tortas ou as arcadas dentárias desalinhadas
                    podem gerar o mau funcionamento de atividades fundamentais para o organismo, como a mastigação, a
                    respiração e a fala. De forma geral, um das principais razões para começar um procedimento
                    ortodôntico é o apinhamento, no qual a pessoa apresenta uma curta arcada dentária com muitas
                    dentições.</p>
                <p>Outro problema é a mordida aberta (as dentinas superiores não encostam nas dentições inferiores na
                    oclusão) e mordida cruzada (as dentições superiores não se alinham corretamente com os inferiores na
                    oclusão). Dessa maneira, torna-se necessário utilizar algum aparelho. Nesse sentido, mostra-se a
                    necessidade de aderir a um aparelho ortodôntico por meio de um convenio dentista, que pode ser
                    algumas das seguintes alternativas:</p>
                <ul>
                    <li>
                        <p>Aparelho ortodôntico fixo estético de porcelana: promovendo uma resistência elevada e cor
                            próxima ao dente natural, o aparelho de porcelana se destaca entre as opções de aparelho
                            estético;</p>
                    </li>
                    <li>
                        <p>Aparelho ortodôntico fixo metálico: é a modalidade mais comum e barata entre todos os tipos
                            de aparelho, possibilitando que o paciente escolha diferentes cores para as borrachinhas dos
                            bráquetes. Além disso, esse aparelho promove as movimentações necessárias dos dentes e da
                            arcada dentária, cuja eficiência existe tanto para casos ortodônticos simples quanto graves;
                        </p>
                    </li>
                    <li>
                        <p>Aparelho ortodôntico invisível: também conhecido como aparelho transparente, o aparelho
                            invisível é removível e fabricado com acetato transparente, sendo considerado uma das opções
                            mais sutis do mercado odontológico, porém é indicado somente para os casos ortodônticos mais
                            simples e de curta duração;</p>
                    </li>
                    <li>
                        <p>Aparelho ortodôntico fixo estético de safira: possibilitando um resultado estético superior
                            em relação às outras opções de aparelho estético, o aparelho de safira é o mais discreto,
                            porém tem um preço maior que os demais;</p>
                    </li>
                    <li>
                        <p>Aparelho ortodôntico fixo lingual: conhecido também como aparelho interno, o aparelho lingual
                            apresenta a estrutura do aparelho fixo metálico, mas é colocado atrás dos dentes, em contato
                            com a língua e não com os lábios, por isso se torna praticamente imperceptível. Dessa forma,
                            o paciente também pode escolher diferentes cores para os bráquetes, como o aparelho de dente
                            preto. Por outro lado, essa opção não é muito acessível financeiramente, já que o preço pode
                            custar até quatro vezes mais que o aparelho fixo comum.</p>
                    </li>
                </ul>
                <h2>É possível realizar um implante dentário pelo convenio dentista</h2>
                <p>É essencial apontar que os implantes fornecem muitas vantagens ao usuário, como a não-rejeição do
                    corpo ao implante, um tempo curto de cirurgia (aproximadamente 30 minutos para cada implante) e a
                    chance de duração indeterminada do implante caso o beneficiário siga o cuidado adequado com a saúde
                    bucal. Nesse contexto, as opções de implante dentário disponíveis são:</p>
                <ul>
                    <li>
                        <p>Implante curto: por ter medidas pequenas e perfuração mínima na estrutura óssea, esse tipo de
                            implante é indicado para as pessoas que não têm a quantidade óssea suficiente na boca e não
                            querem fazer o enxerto ósseo antes do implante, já que ele é um procedimento caro;</p>
                    </li>
                    <li>
                        <p>Zircônia: sendo apresentado como uma capacidade substituta ao titânio, a zircônia fornece
                            vantagens, mas ainda não é muito utilizada nos consultórios odontológicos. Aliás, os estudos
                            sobre material para esse tipo de material ainda não são conclusivos. De qualquer forma, não
                            tem como negar que a cor branca da zircônia é muito semelhante ao dente e apresenta uma
                            ótima resistência;</p>
                    </li>
                    <li>
                        <p>Implante protocolo: a forma do implante protocolo é recomendado para quem perdeu muitos
                            dentes e faz uso da dentadura diariamente. Esse tipo de implante possibilita a inserção de
                            uma prótese fixa, permitindo que o paciente tenha maior conforto e mobilidade na região
                            bucal, sem precisar do uso da dentadura novamente;</p>
                    </li>
                    <li>
                        <p>Titânio: quanto ao material, o cilindro de titânio ainda é o mais amplamente utilizado.
                            Afinal, ele tem grandes vantagens como a altamente resistência, leveza e compatibilidade com
                            o organismo humano. </p>
                    </li>
                </ul>
                <p>Segundo o Instituto Brasileiro de Geografia e Estatística (IBGE), cerca de cinquenta por cento dos
                    adultos brasileiros têm 20 ou menos dentes funcionais. Já entre as pessoas da terceira idade, esse
                    número é ainda mais agravante, pois 70% deles não têm a quantidade correta de dentes funcionais.
                    Para resolver esse problema, colocar a prótese dentária após o implante se revela como uma
                    interessante alternativa na área da odontologia.</p>
                <p>Por isso, é essencial procurar as alternativas de prótese no convenio dentista. Nesse sentido, existe
                    a prótese dental removível. Habitualmente conhecida como dentadura, essa é a prótese mais indicada
                    para quem perdeu grande quantidade de dentições ou todos eles. Existe também a prótese dental
                    parcialmente fixa. Famosa também como ponte ou coroa, essa prótese é mais indicada para quem ficou
                    sem um ou mais dentinas, enquanto os outros ao redor continuam intactos. Dessa forma, vale a pena
                    adquirir um convenio dentista.</p>
                <h2>É possível fazer clareamento dentário pelo convenio dentista</h2>
                <p>O clareamento dental aponta uma alta taxa de sucesso nos consultórios odontológicos no geral, já que
                    proporciona um resultado muito satisfatório para os usuários. Sendo assim, nem todos os indivíduos
                    podem fazer esse tratamento: grávidas, lactantes, pacientes com irritação na gengiva, sensibilidade
                    nos dentes e restaurações severas são alguns dos grupos de risco. </p>
                <p>Antes de fazer o procedimento, é comum que os indivíduos busquem na internet artigos sobre esse
                    assunto, pesquisem sobre o custo do clareamento dental e leiam os relatos de beneficiários que já
                    fazem o clareamento dental. No entanto, fazer isso pode trazer prejuízo, já que nem tudo que está
                    escrito na internet é verdade.</p>
                <p>Sendo assim, torna-se fundamental conversar abertamente com o especialista conveniado pelo convenio
                    dentista para entender o procedimento e as opções relacionadas a ele nos mínimos detalhes. Para
                    sanar as dúvidas de quem está interessado no assunto, existem algumas questões importantes. Por
                    exemplo, o clareamento não deixa as dentições mais sensíveis. Sendo assim, o clareamento a laser
                    possibilita um resultado mais eficaz e rápido que o caseiro, já que, para alguns usuários, uma única
                    sessão basta. Lembrando, é claro, que comidas ou bebidas com forte pigmentação, como café e
                    refrigerantes de cola, precisam ser evitadas depois que o procedimento tiver sido concretizado.</p>
                <h2>Siga as recomendações</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Além disso, o efeito do clareamento nas dentições pode durar por longos anos caso o usuário siga
                    adequadamente os cuidados recomendados pelo dentista. Nessa linha, existem diferentes tipos de
                    clareamento. O clareamento a laser, por exemplo, é realizado no consultório odontológico com luz
                    pulsada, cujo resultado desejado pelo paciente pode ocorrer em apenas três sessões.</p>
                <p>Assim como o clareamento a laser, o clareamento caseiro necessita de um dentista para fazer o
                    acompanhamento do usuário. A diferença, nesse caso, é que ele usará uma moldeira feita pelo dentista
                    com uma substância clareadora. Nesse sentido, o gel clareador vai depender da cor dos dentes do
                    paciente. Após fabricar a moldeira, ela deve ser utilizada em domicílio sob as recomendações de
                    tempo e frequência do dentista. Em geral, o tempo de uso é de, aproximadamente, quinze dias. </p>
                <p>Já no caso do clareamento de farmácia, o usuário encontra kits de clareamento em farmácia. Os kits
                    desse tratamento são autoaplicáveis, nos quais contêm geralmente gel clareador, moldeira
                    pré-fabricada ou tiras branqueadoras. O usuário pode conseguir o resultado desejado com esse
                    clareamento, mas indica-se sempre que o usuário não faça o tratamento sem antes consultar um
                    dentista.</p>
                <h2>Conseguiu entender a importância de um convenio dentista?</h2>
                <p>Com isso, ficou cada vez mais possível conseguir um sorriso branco sem gastar muito dinheiro com o
                    preço do clareamento dental. Nesse caso, vale lembrar também que é essencial fazer o tratamento com
                    um profissional de confiança, para que assim seja possível ter o resultado ideal. Mas com um
                    convenio dentista, não é necessário se preocupar com o custo do tratamento.</p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>