<?php
include('inc/vetKey.php');
$h1 = "plano ortodôntico";
$title = $h1;
$desc = "Plano ortodôntico: arrume a arcada dentária com aparelho ortodôntico  É muito comum ver crianças e adolescentes utilizando aparelhos dentários.";
$key = "plano,ortodôntico";
$legendaImagem = "Foto ilustrativa de plano ortodôntico";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Plano ortodôntico: arrume a arcada dentária com aparelho ortodôntico </h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>É muito comum ver crianças e adolescentes utilizando aparelhos dentários. Essa área da odontologia,
                    realiza sob a supervisão e acompanhamento de um profissional, o ortodontista, é usada para corrigir
                    ossos maxilares e a disposição dos dentes na arcada dentária. Geralmente, o quadro aponta para
                    deformações ou posições inadequadas na boca, atrapalhando a estética, a fala e até mesmo a
                    mastigação.</p>
                <p>A grande meta dos aparelhos dentais é disponibilizar um sorriso alinhado e bonito ao paciente,
                    mantendo a saúde bucal em primeiro plano. Afinal, muitos indivíduos que sofrem com os dentes tortos,
                    apresentam outros problemas de saúde relacionados, por exemplo, dificuldades respiratórias e de
                    deglutição. Por isso, a cobertura de plano ortodôntico está presente em diversos convênios dentários
                    no país.</p>
                <h2>Saiba como funciona o plano ortodôntico</h2>
                <p>Com apoio de um plano ortodôntico, os pacientes podem descobrir o mais cedo possíveis problemas de má
                    oclusão. Normalmente, após uma rápida análise, radiografias e fotografias, o dentista consegue
                    identificar a situação e fazer o diagnóstico. Para saber exatamente qual o grau de deformação da
                    arcada dentária, o paciente também faz o mapeamento da cavidade bucal por meio de um molde de gesso.
                    Somente após essa etapa, o ortodontista chega ao caso clínico e decidirá então, qual o procedimento
                    mais indicado.</p>
                <p>O plano ortodôntico é algo muito exclusivo, justamente por esta razão não se enquadra em grande parte
                    dos planos odontológicos disponíveis. Vale a pena sempre verificar essa cobertura antes de contratar
                    o serviço das operadoras. Confira a seguir como saber se você ou alguém da sua família é “candidato”
                    a usar aparelho dental:</p>
                <ul>
                    <li>
                        <p>mordida aberta: fique atento aos espaços entre as superfícies laterais ou frontais;</p>
                    </li>
                    <li>
                        <p>mordida cruzada anterior: acontece quando a arcada superior se localiza muito atrás da
                            inferior;</p>
                    </li>
                    <li>
                        <p>diastema: o paciente apresenta falhas e espaços, que ocorreram pela falta do desenvolvimento
                            adequado dos dentes;</p>
                    </li>
                    <li>
                        <p>mordida cruzada: normalmente, ao morder, a arcada inferior se posiciona mais à frente da
                            superior;</p>
                    </li>
                    <li>
                        <p>desvio de linha mediana: neste caso, não há alinhamento entre a arcada superior e a inferior;
                        </p>
                    </li>
                    <li>
                        <p>apinhamento: pessoas com arcada dentária pequena, com espaço insuficiente para acomodar a
                            dentição.</p>
                    </li>
                </ul>
                <h2>Plano ortodôntico: aparelho fixo ou móvel?</h2>
                <p>Pode solicitar para qualquer criança que você conhece e até mesmo a algum adolescente, ou jovem. A
                    maioria, com certeza, já sonhou em colocar aparelho ortodôntico. Há uns que querem tanto que até já
                    pediram aos pais para ganhar um aparelho de presente. Coisa de criança, que viu o amiguinho usar e
                    quer ser igual, ainda mais quando o colega da escola aparece todo dia com borrachinhas de cores
                    diferentes. </p>
                <p>Apesar da popularidade dos aparelhos entre a garotada, seu uso deve ser orientado e prescrito por um
                    profissional. O modismo fez com que muitos jovens colocassem aparelho sem precisar, e isso gera
                    problemas graves de saúde. Contudo, quando bem assessorado, o paciente terá apenas ganhos à saúde
                    oral com o tratamento oferecido pelo plano ortodôntico. </p>
                <p>Além do mais, é o ortodontista quem indica o uso de aparelho fixo ou móvel. A primeira fase do
                    tratamento é a correção das dentições, ou seja, corrigir a posição na arcada dentária chamada de
                    alinhamento e nivelamento. A próxima fase é a do encaixe da mordida e da oclusão. Neste sentido, o
                    odontologista pode orientar para o uso de acessórios desde molas a ajuste ou elásticos. Passada essa
                    fase, é hora de realizar a correção estética, que varia conforme o tipo de rosto e formato dentário.
                    E pensar que depois de todo esse trabalho, ainda há a parte mais importante do processo: a
                    manutenção do aparelho. </p>
                <h2>Plano ortodôntico: fase de adaptação</h2>
                <p>Para muita gente, especialmente para quem já passou da infância e da adolescência, usar aparelho
                    dental é muito desagradável. Sem dúvida, a adaptação é a parte mais complicada, porque apesar dos
                    progressos e alta tecnologia presente nos procedimentos, os ajustes feitos no consultório
                    regularmente, geram dor e desconforto. Lógico que, com o passar do tempo, ainda mais com apoio de um
                    plano ortodôntico, os beneficiários se acostumam e o tratamento fica mais tranquilo.</p>
                <p>Além das consultas para manter a funcionalidade do aparelho, o usuário precisa redobrar os cuidados
                    com a saúde bucal. Quando a higiene oral é deixada em segundo plano, os alimentos se acumulam nos
                    bráquetes (peças coladas aos dentes) e o risco das cáries aumenta. Sem falar nos outros problemas,
                    como periodontite (doenças na gengiva) e desenvolvimento das tão temidas manchas nos dentes.</p>
                <h2>Conheça as variedades de aparelho</h2>
                <p>Como dissemos no tópico anterior, há dois tipos de aparelho: o fixo e o móvel. Contudo, há diversas
                    variáveis. Veja um pouco sobre os aparelhos e saiba quais deles estão incluídos no pacote do plano
                    ortodôntico contratado. O aparelho fixo pode ser comum. Este modelo é gerado por bandas fixadas ao
                    redor dos dentes. O foco é fazer uma ligação ao aparelho. Além disso, a ligação é feita também pelos
                    bráquetes, na parte externa do dente. Nas crianças, esses materiais podem ser coloridas, fazendo a
                    diversão dos pequenos. Já os adultos preferem os bráquetes transparentes. </p>
                <p>Outro tipo bastante utilizado é o fixo estético. Levando em consideração que quem passa a usar
                    aparelho não quer que o mesmo seja notado, este modelo uniu a tecnologia à necessidade dos pacientes
                    mais exigentes e preocupados com o visual. Muito discreto, o aparelho fixo estético é pequeno e
                    praticamente imperceptível. Isso porque, os bráquetes são feitos de porcelana, quase na tonalidade
                    dos dentes. No entanto, há uma grande desvantagem neste modelo, por isso é pouco utilizado no plano
                    ortodôntico: o custo é bastante alto se comparado aos demais. No mais, também é preciso reforçar a
                    higiene oral, para evitar o acúmulo de resíduos.</p>
                <h2>Aparelhos móveis ajudam no desenvolvimento ósseo</h2>
                <p>A fase de dentição de leite é fundamental para o desenvolvimento ósseo. Este momento é crucial para
                    que as dentinas permaneçam na posição adequada, sem ficar desalinhados ou tortos. Após análise
                    clínica, que pode ser realizada pelo especialista em consulta do plano ortodôntico, este tipo de
                    aparelho é indicado para prevenir o problema antes que ele se instale na boca. </p>
                <p>Parece um aparelho sem muita essencialidade, mas é um dos mais importantes nesse processo de
                    reabilitação. Se não houver uso contínuo do beneficiário e visitas regulares ao dentista, o problema
                    se agrava e, bem provavelmente, o tratamento será ainda mais longo com uso de aparelho fixo. </p>
                <p>Há ainda o aparelho autoligado. A principal característica deste modelo é não fazer uso dos
                    elásticos, as famosas borrachinhas. Os bráquetes possuem um dispositivo próprio fixado do fio ao
                    dente. Uma das grandes vantagens deste modelo é a redução de atrito entre fio e bráquetes Resultado:
                    tempo menor de tratamento. A parte negativa é o preço mais elevado, e também, por isso, pouca
                    abrangência no plano ortodôntico.</p>
                <p>Outro aparelho disponível aos pacientes é o invisível, chamado de alinhador. Seu principal foco é
                    fazer a correção das dentições usando placas removíveis de acetato. Normalmente, este aparelho
                    dental é indicado para casos mais simples, na correção de alinhamento. Diferente dos demais, o
                    alinhador não tem bráquetes, tampouco fios. Com tanto conforto é o sonho da maioria dos pacientes. O
                    problema é o custo elevado, que afasta essa opção de vários convênios dentários com cobertura para
                    plano ortodôntico. </p>
                <p>Existe também o aparelho lingual. O próprio nome já indica, este modelo fica em contato com a língua.
                    Os bráquetes são fixados na parte interna das dentições, ficando praticamente invisíveis. A estética
                    neste tipo de aparelho é completamente preservada, por outro lado é importante focar na limpeza da
                    boca.</p>
                <h2>Ortodontia progressiva e especializada</h2>
                <p>Outro ponto que merece destaque é o progresso na área da ortodontia. Hoje em dia, os aparelhos
                    dentários estão cada vez mais discretos e leves, garantindo, além da eficácia do tratamento,
                    qualidade de vida ao paciente. Confira a seguir as principais categorias dos aparelhos ortodônticos
                    e suas indicações:</p>
                <ul>
                    <li>
                        <p>corretivos: neste caso, o problema já está instalado. Os aparelhos mais completos são usados
                            para tratar a má oclusão e encaixe imperfeito da dentição;</p>
                    </li>
                    <li>
                        <p>preventivos: estes aparelhos são usados para amenizar a má oclusão, bem como problemas com o
                            encaixe entre os dentes;</p>
                    </li>
                    <li>
                        <p>pré-protéticos: pessoas que perderam dentes naturais e precisam movimentar os demais, fazem
                            uso deste aparelho. Normalmente, os pacientes já possuem próteses e implantes dentários;</p>
                    </li>
                    <li>
                        <p>interceptivos: o aparelho é usado para impedir que os problemas de má oclusão avancem;</p>
                    </li>
                    <li>
                        <p>orto-cirúrgicos: essa modalidade é bastante complexa, pois ocasionam problemas graves de má
                            oclusão que afetam os ossos da face. </p>
                    </li>
                </ul>
                <h2>Veja quais são as coberturas de plano ortodôntico</h2>
                <p>Se ter acesso a um plano de saúde odontológico é essencial, imagine as facilidades de contar com a
                    inclusão desta cobertura, ou seja, o plano ortodôntico. No entanto, na hora de contratar os
                    trabalhos, confira as modalidades do procedimento. Algumas empresas oferecem a cobertura completa,
                    ou seja, o paciente pode contar com a aplicação do aparelho e manutenção do tratamento. Já, em
                    determinados convênios é coberto apenas a manutenção que ocorre mensalmente, sem pagar o aparelho.
                    Outro tipo de cobertura faz o contrário: faz apenas a aplicação, mas não oferece a manutenção
                    regular. </p>
                <p>Essas regras são diferenciadas, porque cada operadora decide se será viável ou não realizar a
                    cobertura. Como os demais casos, a Agência Nacional de Saúde Suplementar (ANS) regula e fiscaliza as
                    normas dos convênios dentários contratados país afora. Devido a muitas vantagens e diferenciais, a
                    concorrência entre as operadoras é alta, e vale a pena o consumidor pesquisar sobre os tipos de
                    cobertura oferecidos e a variação de preços. </p>
                <h2>Higiene bucal: atenção redobrada com aparelho</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>A manutenção da limpeza bucal é fundamental para que as dentições cresçam saudáveis e bonitos e se
                    mantenham com boa aparência e fortes ao longo da vida. Quem possui plano odontológico consegue
                    manter uma rotina de visitas ao dentista, auxiliando muito na saúde oral. Quando há recomendação de
                    aparelho, ter um plano ortodôntico também é um diferencial para ajudar no tratamento, reduzindo
                    despesas durante o período.</p>
                <p>Porém, de maneira geral, a limpeza dentária é imprescindível em qualquer idade, com ou sem aparelhos
                    dentais. O fato é, que usuários que exibem os afamados sorrisos metálicos, precisam redobrar os
                    cuidados com a higiene. Confira as dicas para manter a limpeza em dia e evitar problemas ainda
                    maiores:</p>
                <ul>
                    <li>
                        <p>bochechos com flúor: nem todas as pessoas fazem uso do enxaguante bucal, mas no caso dos
                            pacientes com aparelho dental, é algo obrigatório. Afinal, o bochecho vai ajudar a eliminar
                            os restos de alimentos que permanecem entre os bráquetes, ajudando na redução das manchas
                            nos dentes;</p>
                    </li>
                    <li>
                        <p>fio dental é obrigatório para todos: recurso importantíssimo em qualquer situação, para quem
                            utiliza aparelhos, é indicado também os rosqueadores de fio e escovas de dente interdental;
                        </p>
                    </li>
                    <li>
                        <p>Escovação todo santo dia e no capricho: quem usa aparelho nos dentes tem que reforçar a
                            escovação. Além disso, o ideal é usar cremes dentais com flúor e escovas com cerdas bem
                            macias;</p>
                    </li>
                    <li>
                        <p>protetores bucais: pessoas que utilizam aparelho dental e praticam esportes devem recorrer
                            aos protetores para evitar ferimentos na boca e na gengiva.</p>
                    </li>
                </ul>
                <h2>Prevenção e plano ortodôntico: cuide da sua saúde bucal</h2>
                <p>Mesmo com tanto acesso a informações, muitas pessoas não frequentam o dentista periodicamente. Um
                    número grande de brasileiros apenas marca a consulta quando a dentição começa a doer ou surge um
                    incômodo na boca. Essa falta de manutenção e cuidados preventivos ocorre por inúmeros motivos, entre
                    eles, desleixo, falta de dinheiro para arcar com os gastos ou medo do dentista. </p>
                <p>Os convênios odontológicos foram desenvolvidos para melhorar a assistência à saúde oral e incentivar
                    as pessoas a frequentarem o dentista regularmente. A ortodontia preventiva também tem este papel,
                    com foco no diagnóstico rápido e eficaz da má oclusão. O plano ortodôntico faz parte deste pacote,
                    oferecendo ao beneficiário atendimento que poderá corrigir a tempo situações mais graves. Alguns
                    pacientes que usufruem do plano ortodôntico não chegam, por exemplo, a utilizar aparelhos fixos.
                </p>
                <p>O plano ortodôntico preventivo pode ser usado por toda a família. Normalmente, na dentição decídua e
                    mista, desenvolvida dos três aos 12 anos, é quando surgem os primeiros problemas de oclusão,
                    determinantes para a colocação do aparelho. Mas indivíduos de todas as idades podem melhorar a saúde
                    bucal com a aplicação do tratamento adequado. </p>
                <p>Além disso, ao visitar o dentista periodicamente, já se faz odontologia de prevenção. Muitos
                    indivíduos possuem problemas de má formação dentária e sonham em colocar aparelhos para corrigir as
                    imperfeições. Porém, nem sempre essa é melhor indicação. A ortodontia preventiva consegue, por meio
                    de procedimentos simples, evitar até mesmo o uso de aparelhos apenas com pequenas intervenções. </p>
                <h2>Transição de hábitos alimentares </h2>
                <p>Quem precisa colocar aparelho dental tem que encarar o começo de um tratamento normalmente chato e
                    desconfortável. Não tem outra alternativa. Mesmo que o aparelho seja o mais discreto e anatômico
                    possível, ter algo puxando e pressionando a dentição requer adaptação. Mas não é só isso. Ao usar o
                    aparelho os indivíduos necessitam deixar de lado algumas rotinas, principalmente alimentares.
                    Orientar os pacientes frente às mudanças que virão, deve ser uma das preocupações do dentista, seja
                    ele credenciado a um plano ortodôntico ou da rede particular. </p>
                <p>Isso porque, alguns cuidados na escolha de comidas irá ajudar na preservação do aparelho e,
                    consequentemente, na manutenção do tratamento. Quanto menos bráquetes quebrados ou deslocados,
                    melhor. Para evitar esse tipo de situação incômoda, quem utiliza aparelho deve deixar de lado
                    comidas crocantes. Esqueça balas, nozes, pé-de-moleque, pipoca, torresmo e até o milho cozido,
                    aquele da espiga, vendido no litoral e queridinho dos turistas. Essa lista de comidas é inimiga dos
                    aparelhos, porque podem estragar o bráquete. </p>
                <p>Outro tipo de alimento que deve ficar fora da sua cozinha são os grudentos. Por mais que o paciente
                    capriche na escovação, será difícil tirar todos os restos de chiclete, caramelo, jujuba e pirulito.
                    Além do mais, esses doces não são indicados para ninguém, nem para os que não usam aparelhos. O
                    melhor é evitá-los pois, geram placa bacteriana e estragam o sorriso.</p>
                <p>Uma dica perfeita, muito comentada no consultório e por odontologistas especializados: quem usa
                    aparelho deve consumir frutas em fatias menores; não se recomenda morder diretamente no formato
                    natural. Às vezes, uma deliciosa maçã pode fazer um grande estrago, danificando o aparelho e
                    soltando o fio ortodôntico. </p>
                <h2>Por que investir em plano ortodôntico?</h2>
                <p>Quanto mais cedo um problema de má oclusão for detectado, mais eficaz e rápido será o solucionado.
                    Neste quesito, é papel do usuário buscar também um serviço de odontologia especializado, com
                    atendimento capaz de oferecer os melhores recursos e resultados. Atualmente, o acesso ao plano
                    ortodôntico é uma das maneiras de conciliar qualidade nos procedimentos corretivos e preço
                    acessível.</p>
                <p>Os odontologistas credenciados, aptos a atender plano ortodôntico, são especialistas na área e podem
                    oferecer ao paciente alguns serviços diferenciados. Na outra ponta, o usuário necessita manter a
                    atenção com a saúde bucal, evitando o consumo de certos alimentos (citados acima neste artigo) e
                    investindo na higiene adequada. </p>
                <p>Quem usa aparelho dental sabe que os tratamentos são longos, normalmente levam de um a três anos.
                    Nesta fase, o paciente de um plano ortodôntico terá que fazer várias visitas ao dentista, para
                    consultas de manutenção. Os gastos com o convênio, quando existe a cobertura, possibilitam que os
                    pacientes façam planejamento e consiga economizar. Os usuários que não optaram pelo plano
                    ortodôntico, terão que arcar com os custos de um dentista particular. </p>
                <h2>Plano ortodôntico: mitos e verdades</h2>
                <p>A primeira coisa que vêm à cabeça das pessoas quando recebem o diagnóstico para a utilização do
                    aparelho ortodôntico é: será mesmo que vai doer e até quando terei que permanecer com o tratamento?
                    Não é possível responder essa pergunta de maneira exata, até porque, o tempo e a intensidade da dor
                    variam de paciente para paciente. No entanto, há situações bastante comuns à maioria das pessoas, e
                    uma delas é: quanto mais cedo colocar o aparelho, mais rápida será a melhora.</p>
                <p>Sendo assim, sempre batemos na tecla de que é muito essencial levar as crianças ao dentista ainda nos
                    primeiros anos de vida. Algumas famílias, mais preocupadas, investem na contratação de convênios
                    dentários com cobertura de plano ortodôntico. Este é um ponto positivo para manter a saúde bucal
                    sobre todos os aspectos, inclusive no tratamento preventivo, quando se percebe na infância,
                    problemas como mordida cruzada e aberta.</p>
                <p>Apesar de tudo isso, é mito achar que o aparelho ortodôntico funciona melhor em uma dentição ou
                    outra. Seja na fase das dentições decíduos (de leite) ou permanentes, o plano ortodôntico irá
                    atender as necessidades do beneficiário, com resultados tanto preventivos como corretivos. O que
                    realmente faz diferença no sucesso do tratamento é uma indicação correta do dentista, aliada às
                    consultas mensais e cuidados com a higienização todos os dias. </p>
                <p>Na sequência, confira outros mitos e verdades sobre o tratamento:</p>
                <ul>
                    <li>
                        <p>é verdade que pessoas que começam a usar aparelho sentem dificuldade na fala, contudo, é algo
                            passageiro;</p>
                    </li>
                    <li>
                        <p>É verdade que um tempo após a retirada do aparelho, os dentes podem voltar a ficar tortos.
                            Isso ocorre porque a oclusão está sempre em movimento e, neste momento, que os pacientes
                            percebem a importância do aparelho de contenção. É preciso usá-lo após a retirada do
                            aparelho ortodôntico, mas infelizmente, muitos pacientes não seguem as orientações e pagam o
                            preço;</p>
                    </li>
                    <li>
                        <p>uma grande verdade: a manutenção do aparelho é feita somente no consultório com um
                            ortodontista;</p>
                    </li>
                    <li>
                        <p>um grande mito é acreditar que quanto mais apertado estiver o aparelho, mais fará o efeito
                            desejado. Isso não tem nada a ver com o sucesso do tratamento, aliás, pode até prejudicar a
                            arcada dentária;</p>
                    </li>
                    <li>
                        <p>é mentira que todos os aparelhos são iguais e servem para a mesma coisa e que a cola que
                            prende os bráquetes danifica os dentes. O que estraga os dentes são a falta de escovação.
                        </p>
                    </li>
                </ul>
                <h2>Sorriso saudável é possível com plano ortodôntico</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Em muitos convênios odontológicos, a cobertura de plano ortodôntico já é uma realidade. Inclusive,
                    para diversos clientes, ter acesso a este diferencial nos planos de assistência à saúde é
                    imprescindível. Na década de 70, o sorriso metálico era visto apenas em crianças e adolescentes,
                    parecia uma regra.</p>
                <p>Nos dias de hoje isso mudou, e facilmente encontramos beneficiários menores de cinco e até senhores
                    com mais de 80 com sorriso metálico. As facilidades de pagamento e o acesso ao procedimento, por
                    meio do plano ortodôntico, fortaleceu a área da ortodontia e fez com que o número de pessoas
                    interessadas nos procedimentos aumentasse. Hoje, o sorriso metálico com indicação correta é
                    fundamental para conquistar a autoestima e a qualidade de vida. </p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>