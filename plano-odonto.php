<?php
include('inc/vetKey.php');
$h1 = "plano odonto";
$title = $h1;
$desc = "Plano odonto: saúde bucal completa a curto, médio e longo prazo Uma das primeiras lições que aprendemos na vida é que cuidar da dentição é essencial.";
$key = "plano,odonto";
$legendaImagem = "Foto ilustrativa de plano odonto";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Plano odonto: saúde bucal completa a curto, médio e longo prazo</h2>
                <p>Uma das primeiras lições que aprendemos na vida é que cuidar da dentição é essencial. Nossos pais
                    perderam um tempo precioso explicando como segurar a escova de dente e, depois, quais eram os
                    movimentos corretos a se realizar para ter uma limpeza adequada. Realmente, a saúde oral inicia
                    dessa forma e, se formos analisar a complexidade de sua relevância na vida das pessoas, é algo
                    bastante simples de realizar diariamente.</p>
                <p>Segundo a Organização Mundial de Saúde (OMS), ter saúde oral significa que a pessoa está livre de
                    dores, alterações e desconfortos na boca ou face. Além do mais, abrange também as condições de
                    câncer oral ou garganta, ulcerações bucais, doenças e infecções. Um indivíduo com saúde oral não
                    sofre com distúrbios que atrapalham a qualidade de vida, ou seja, não há problemas para a
                    alimentação, à fala e ao sorriso. Podemos resumir a saúde bucal também de uma forma mais amigável,
                    dizendo que pessoas saudáveis estão de bem com a vida, sorrindo à toa.</p>
                <p>Para conquistar essa tão sonhada saúde oral, o início é lá atrás, ainda na infância, com o conselho e
                    exemplo dos pais, lembra? Mas em qualquer fase da vida é possível recuperar o tempo que se foi e
                    iniciar a cuidar da dentição com carinho e responsabilidade. Nos dias de hoje, o acesso ao plano
                    odonto, auxilia as pessoas a melhorarem a rotina nos dentistas e iniciarem os tratamentos
                    preventivos e corretivos. </p>
                <h2>Saúde oral e as patologias mais recorrentes </h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>A cavidade oral não é um órgão desligado do restante do corpo, muito pelo contrário. Tudo que ocorre
                    nesse local está conectado diretamente ao organismo. Dessa forma, tudo que fazemos de bom para os
                    nossos dentes refletem em mais saúde. Em contrapartida, quando deixamos de lado os cuidados,
                    problemas podem entrar pela boca e afetar os outros órgãos do corpo humano.</p>
                <p>Você já parou para pensar, que uma dentição infeccionada e não tratada leva a disfunções
                    imunológicas, digestivas e até cardíacas? Pois é. Se você ficou preocupado, contrate logo seu plano
                    odonto ideal e comece hoje já os procedimentos. De acordo com o Instituto do Coração, cerca de 45%
                    das patologias cardíacas em São Paulo surgem por razão de problemas orais. Pode ser uma cárie sem
                    tratamento, gengivas inflamadas, dentes fraturados e abcessos: todos estes sintomas podem ajudar a
                    proliferação de bactérias que chegam ao coração e causam problemas sérios de saúde. </p>
                <h2>Por que fazer um plano odonto?</h2>
                <p>Primordialmente, vamos apontar o que é um plano odonto e para que serve. A atividade que gere os
                    planos ou seguros de saúde em geral é a Saúde Suplementar. Sua operação é baseada pelo poder
                    público, representado pela Agência Nacional de Saúde Suplementar (ANS). Na prática, para que o
                    trabalho chegue até o cliente, no caso, o paciente, existem cinco formas de atuação no mercado, são
                    elas:</p>
                <ul>
                    <li>
                        <p>instituições filantrópicas;</p>
                    </li>
                    <li>
                        <p>cooperativas;</p>
                    </li>
                    <li>
                        <p>autogestões;</p>
                    </li>
                    <li>
                        <p>medicinas de grupo;</p>
                    </li>
                    <li>
                        <p>seguradoras especializadas em saúde.</p>
                    </li>
                </ul>
                <p>Devido à elevação econômica do Brasil, ainda na década de 60, surgiu então a chamada Saúde
                    Suplementar. Com o progresso da economia, as operadoras começaram a contratar mais mão de obra e,
                    aos poucos, algumas instituições passaram a oferecer planos de saúde médico aos funcionários.
                    Resultado: mais famílias passaram a planejar estes recepcionamentos.</p>
                <p>Mas de fato, a atividade de planos assistenciais de saúde entraram em vigor no Brasil por meio da lei
                    9.656 de 1998, que dispõe sobre os planos de saúde. Na sequência foi desenvolvido o rol da ANS,
                    responsável pela regulamentação e fiscalização. Antes disso ocorrer, todas as seguradoras de saúde
                    seguiam as normas e regras da Superintendência de Seguros Privados (Susep).</p>
                <p>A estimativa é que, em 2019, cerca de 50 milhões de indivíduos utilizem de serviços de planos de
                    saúde médicos; outros 20 milhões já são pacientes de plano odonto. É fundamental lembrar que além
                    dos convênios privados, a população tem acesso garantido aos tratamentos dentários também pelo
                    Sistema Único de Saúde (SUS). </p>
                <h2>Um plano odonto e suas particularidades que você precisa conhecer </h2>
                <p>Quando homens e mulheres escolhem por adquirir um plano odonto necessitam sentar e discutir a
                    importância desta modalidade na rotina familiar. Geralmente, apesar de conhecermos de trás para
                    frente as maneiras de cuidar da saúde bucal, a maioria de das pessoas não segue à risca as regras de
                    higiene. E mais: se muitos não fazem a manutenção mínima necessária de higienização em casa, imagina
                    se ir ao dentista faz parte da rotina? Lógico que não. </p>
                <p>E este é o primeiro ponto que devemos estudar na contratação de um plano odonto. Quando passamos a
                    realizar parte deste momento, as consultas se tornam rotina e isso trará consequências positivas aos
                    pacientes. O plano odonto tem alguns benefícios interessantes, como:</p>
                <ul>
                    <li>
                        <p>preço acessível;</p>
                    </li>
                    <li>
                        <p>coberturas diferenciadas;</p>
                    </li>
                    <li>
                        <p>sem carência (dependendo do plano odonto);</p>
                    </li>
                    <li>
                        <p>consultas frequentes ao dentista;</p>
                    </li>
                    <li>
                        <p>atendimento emergencial;</p>
                    </li>
                    <li>
                        <p>inclusão de dependentes.</p>
                    </li>
                </ul>
                <h2>Procedimentos e coberturas do plano odonto</h2>
                <p>No Brasil, mais de vinte e dois milhões de brasileiros usam os planos de saúde odontológicos. Com
                    tanta oferta no mercado, é normal que apareçam dúvidas sobre qual é o melhor plano odonto a
                    contratar. Na realidade, a grande parte dos convênios dentários oferece serviços semelhantes,
                    contudo, é essencial estar atento antes de fechar o contrato.</p>
                <p>O início do procedimento odontológico em um plano odonto é exatamente igual a um atendimento
                    particular. Primeiro, o beneficiário vai passar por uma avaliação do clínico geral. Muitos dentistas
                    aproveitam também este primeiro contato para estreitar os laços de confiança. É hora de falar e
                    bater um papo sério. O especialista irá sentir se a pessoa sente medo ou receio ao tratamento e como
                    encara sua rotina de cuidados com a boca.</p>
                <p>Sendo assim, é hora dos tratamentos simples de qualquer plano odonto. Após o exame clínico,
                    normalmente é feito uma higienização mais profunda, chamada de profilaxia. Muitos profissionais
                    fazem também a remoção do tártaro e aplicação de flúor. Depois, se for o caso, o paciente será
                    encaminhado para outros especialistas, conforme as necessidades.</p>
                <h2>Coberturas de urgência e emergência no plano odonto</h2>
                <p>Segundo a Agência Nacional de Saúde Suplementar (ANS), atendimentos de urgência e emergência são:</p>
                <ul>
                    <li>
                        <p>recolocação de dentes quebrados por acidente;</p>
                    </li>
                    <li>
                        <p>curativos quando há hemorragia constante;</p>
                    </li>
                    <li>
                        <p>curativos e drenagem em dentes com dor intensa, acompanhado de inflamação e infecção;</p>
                    </li>
                    <li>
                        <p>problemas gengivais graves com sangramento e dores intensas.</p>
                    </li>
                </ul>
                <p>Este método é tão relevante que integra todos os planos de saúde odontológicos no país e estão
                    regulamentados pela lei 9.656/98. Outras questões que necessitam rapidamente de procedimento são a
                    recimentação de próteses, drenagem de abcessos, colagem de fragmentos, tratamentos das alveolites
                    (infecção do alvéolo, parte interior do osso onde se encaixa o dente) e, por fim, a imobilização
                    dentária. O atendimento de urgências no plano odonto é 24 horas, justamente para garantir aos
                    pacientes a tranquilidade quando um problema sério ocorrer. </p>
                <h2>Coberturas simples de planos odontológicos</h2>
                <p>Como dissemos acima, a primeira consulta feita em qualquer plano odonto será o ponto de partida para
                    que o odontologista faça uma avaliação médica, identificando ou não possíveis problemas orais. O
                    próximo passo é continuar os procedimentos preventivos e corretivos. De modo geral, o plano odonto
                    oferece aos usuários coberturas simples:</p>
                <ul>
                    <li>
                        <p>limpeza;</p>
                    </li>
                    <li>
                        <p>tratamento de cáries;</p>
                    </li>
                    <li>
                        <p>radiografias;</p>
                    </li>
                    <li>
                        <p>endodontia (tratamento de canal);</p>
                    </li>
                    <li>
                        <p>retração gengival;</p>
                    </li>
                    <li>
                        <p>cirurgias;</p>
                    </li>
                    <li>
                        <p>gengivite;</p>
                    </li>
                    <li>
                        <p>extração de dentes.</p>
                    </li>
                </ul>
                <p>Contudo, cada plano odonto se encaixa no perfil de seus pacientes e, alguns, são mais completos,
                    fornecendo ao usuário colocação de próteses e implantes e clareamento dental (procedimento
                    estético). Outro período que causa dúvida na hora de contratar o serviço é carência. Nos dias de
                    hoje, as formas mais comuns de atuação no mercado seguem essas regras:</p>
                <ul>
                    <li>
                        <p>90 dias: periodontia (tratamento de gengiva) e endodontia (tratamento de canal);</p>
                    </li>
                    <li>
                        <p>24 horas: urgências e diagnósticos;</p>
                    </li>
                    <li>
                        <p>60 dias: prevenção, radiologia e cirurgia;</p>
                    </li>
                    <li>
                        <p>180 dias: colocação de próteses e demais procedimentos.</p>
                    </li>
                </ul>
                <p>Muitas operadoras anunciam ainda o chamado plano odonto sem carência. Normalmente, o pagamento destes
                    convênios é anual à vista, dificultando o acesso a quem não tem dinheiro na hora. No entanto, quem
                    pode arcar com o custo terá os atendimentos à disposição assim que o contrato entrar em vigência.
                </p>
                <p>Uma dúvida muito frequente para quem quer investir em um plano dental é quais serão os especialistas
                    responsáveis pelos atendimentos. Ao contratar o plano odonto, os usuários terão acesso a uma lista
                    de dentistas que está apto a realizar os procedimentos. Dessa forma, antes de contratar o convênio,
                    analise o nome dos profissionais e como é a realidade da sua região.</p>
                <h2>Essencialidade da saúde bucal na infância</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Se perguntarmos por aí, qual fase de sua vida os indivíduos sentem mais saudade, com certeza será da
                    infância. Além disso, tem pessoas que costumam utilizar a expressão: "queria ter sido criança a vida
                    inteira". De fato, os primeiros anos são os mais essenciais para a formação da personalidade que
                    iremos carregar ao longo da trajetória humana. E mais: é ainda quando criança que adquirimos hábitos
                    de higiene, que melhoram nossa saúde.</p>
                <p>Quando a questão é saúde bucal, logo vem à cabeça os ensinamentos sobre a escovação. E isso é
                    realmente essencial, porque tudo se inicia exatamente com a higiene diária. Os pais devem orientar
                    as crianças sobre a relevância da escovação e também sobre a importância de visitar o dentista a
                    cada seis meses. </p>
                <p>Ao ter acesso a um plano odonto, grande parte das famílias inclui na rotina estes cuidados. Com
                    escovação em dia e consultas, dificilmente a criança terá problemas orais mais sérios. O resultado
                    de tanto cuidado será dentinas mais bonitas e saudáveis nas demais fases da vida. Veja os processos
                    de higienização bucal em crianças:</p>
                <ul>
                    <li>
                        <p>ao aparecer o primeiro dente é necessário começar a escovação, pelo menos duas vezes ao dia;
                        </p>
                    </li>
                    <li>
                        <p>no primeiro aniversário dê aos pequenos um presente: sua primeira visita ao dentista;</p>
                    </li>
                    <li>
                        <p>limpeza regular das gengivas do bebê recém-nascido com pano úmido após as alimentações
                            (mamadas);</p>
                    </li>
                    <li>
                        <p>depois, mantenha as consultas de seis em seis meses.</p>
                    </li>
                </ul>
                <h2>Escovação e fio dental: as coisas mais importantes no dia a dia</h2>
                <p>Contar com o auxílio de um plano odonto é essencial para auxiliar a manter a saúde oral. No entanto,
                    o melhor remédio são duas coisas muito simples e em conta de realizar: a escovação e o uso de fio
                    dental. Não é perda de tempo ficar em frente ao espelho, massageando a escova sobre as dentições
                    inferiores e superiores todos os dias, ao menos três vezes após as refeições.</p>
                <p>Este costume de limpeza auxilia a eliminar resíduos de comidas, que ao se acumularem entre os dentes,
                    causam doenças, como cáries e a formação do temido tártaro e placa bacteriana. Mas não esqueça de
                    outro detalhe bem importante. Antes mesmo de iniciar a escovação faça uso do fio dental. Por mais
                    caprichada que sua limpeza seja, alguma sujeira sempre ficará escondida. É exatamente essa a função
                    do fio dental: retirar o acúmulo de comida que fica naquele lugar onde a escova não alcança. </p>
                <p>O uso de enxaguante oral não é obrigatório, ainda mais se você faz corretamente os passos citados
                    acima. Mas os bochechos são ótimos auxiliadores para completar a limpeza da boca, sem falar que
                    ajuda a manter o hálito agradável. Confira as recomendações a seguir e obtenha ótimos resultados:
                </p>
                <ul>
                    <li>
                        <p>faça movimentos circulares suaves, de frente para trás, na parte frontal e também nos dentes
                            posteriores e superiores. Jamais esfregue com força, essa prática não limpa os dentes, e
                            ainda pode irritar a gengiva;</p>
                    </li>
                    <li>
                        <p>escove a língua para ajudar a refrescar o hálito e eliminar as bactérias que causam acúmulo
                            de placa;</p>
                    </li>
                    <li>
                        <p>mantenha a escova de dentes em um ângulo de 45 graus sobre a linha das gengivas;</p>
                    </li>
                    <li>
                        <p>uso o fio dental com frequência atrás dos dentes frontais inferiores;</p>
                    </li>
                    <li>
                        <p>pessoas com problemas na gengiva podem adicionar o enxágue bucal com antisséptico.</p>
                    </li>
                </ul>
                <h2>Plano odonto empresarial</h2>
                <p>Assim como o plano individual e familiar, o plano empresarial tem elevado consideravelmente no
                    Brasil. Hoje em dia, no ambiente corporativo, ter um sorriso saudável faz toda a diferença. Por
                    isso, cada vez mais gestores buscam ao plano odonto específico para as corporações, com o foco de
                    melhorar a qualidade do trabalho e consequentemente, a vida dos colaboradores.</p>
                <p>O plano odonto empresarial tem muitas vantagens. Para os colaboradores, os custos são muito
                    acessíveis e há boas condições de pagamento, que podem ser realizados com participação ou não dos
                    colaboradores. Além do mais, o plano odonto voltado às empresas conta com coberturas diferenciadas e
                    normalmente tem carência zero. Ou seja, os funcionários têm acesso a serviços de saúde bucal pagando
                    valores baixos e ainda contam com atendimento rápido e eficaz. Outra vantagem para o empresário é a
                    dedução desse valor investido nos funcionários no imposto de renda. </p>
                <p>Por outro lado, o maior benefício de investir neste plano de saúde odontológico é a produtividade. Um
                    estudo realizado pela Organização Mundial de saúde (OMS) revela que pessoas mais felizes rendem mais
                    no ambiente de trabalho. E as operadoras que oferecem o benefício do plano de saúde
                    médico-hospitalar e o plano odonto estão de olho neste cenário. Ter colaboradores mais motivados é a
                    chave do sucesso para muitos empreendedores. </p>
                <p>Já para os funcionários, a matemática é muito simplista. Com a garantia de acesso à procedimentos
                    orais, ele não irá necessitar gastar tempo e dinheiro correndo atrás de atendimento particular ou
                    mesmo ficar horas na fila do serviço público. Quando o colaborador tem este benefício extra
                    oferecido pela empresa, consegue organizar suas idas ao dentista e evita situações de emergência. O
                    resultado é a melhora na produtividade e na qualidade de vida do indivíduo. </p>
                <h2>Saúde oral e plano odonto para idosos</h2>
                <p>Quando os cuidados com a saúde iniciam ainda na infância, os indivíduos possuem grandes chances de
                    chegar à terceira idade com dentes saudáveis. No entanto, a atenção à dentição nessa fase da vida é
                    essencial. Os idosos podem desenvolver mais facilmente problemas como gengivite, por isso não é hora
                    de descuidos. O plano odonto para essa idade é personalizado e muitos fornecem a colocação de
                    próteses e implantes dentários. No entanto, na velhice, os costumes de limpeza são tão essenciais
                    como as consultas frequentes ao dentista. Pessoas mais velhas também sofrem com a chamada "boca
                    seca".</p>
                <p>O problema pode ter causa na utilização excessiva de medicamentos e outros distúrbios de saúde.
                    Parece algo simples, mas se estes casos não forem tratados, os dentes podem ficar ainda mais
                    prejudicados. A indicação dos dentistas é manter a boca úmida, inclusive com tratamentos
                    específicos. Na terceira idade, é necessário reforçar também o cuidado com as dentaduras. Muitos
                    idosos fazem uso deste recurso, porque, infelizmente, não tomaram os cuidados necessários nas fases
                    anteriores. Além do mais, antigamente era normal a prática de extração dentária.</p>
                <p>Ao sinal de qualquer problema mais grave, os dentistas costumavam extrair o dente e usar as famosas
                    coroas no lugar dos naturais. Com o avanço da odontologia essa realidade mudou. Hoje, toda tentativa
                    é válida para manter os dentes. Aliás, essa é uma das premissas dos bons profissionais: lugar de
                    dente é na boca. Mas quando não tem mais jeito e já foram realizadas as remoções, lembre-se de
                    cuidar com carinho da dentina substituta, acoplados à boca por dentaduras, próteses e implantes.
                    Deve-se seguir as instruções do dentista e frequentar as consultas pelo menos uma vez ao ano, no
                    caso de pacientes com dentaduras definitivas.</p>
                <h2>Com o plano odonto é possível ter uma qualidade de vida no presente e no futuro</h2>
                <p>A comunicação é fundamental na vida dos indivíduos. É como dizia o ditado que "ninguém é feliz
                    sozinho". E é a mais pura verdade. Mas experimente ter uma dentição feia, amarelada e com mau hálito
                    para ver se essa socialização será tão prazerosa assim. Infelizmente, milhões de indivíduos sofrem
                    de depressão, ansiedade e estresse, e se isolam no seu próprio mundo. Parece exagero, mas muitos
                    desses indivíduos sofrem também de problemas bucais.</p>
                <p>Com dentições feias, os indivíduos preferem se isolar. Passam a não participar de reuniões de
                    trabalho - algumas até mesmo estão fora do mercado, porque não conseguem uma vaga, fogem das festas
                    e param até de conversar e trocar ideias com os amigos. O constrangimento pelo sorriso deficitário
                    atrapalha a vida social dos indivíduos. E não é apenas isso: uma boca doente atrapalha costumes
                    básicos, como mastigar alimentos. </p>
                <p>O contrário também é verdade, o grande foco deste artigo é exatamente lembrar que é possível ganhar
                    qualidade de vida e voltar a sorrir sem medo. O acesso a um plano odonto pode ser um primeiro passo
                    rumo ao sorriso dos seus sonhos. Vale a pena realizar um investimento. Quanto antes decidimos cuidar
                    da nossa saúde bucal, mais precocemente colheremos os frutos dessa atitude. </p>
                <h2>Cuide-se com pouco e tenha uma vida melhor</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Costumes básicos de limpeza garantem uma cavidade bucal livre de patologias, além de manter dentes
                    brancos e brilhantes. O plano odonto tem um papel fundamental na mudança de mentalidade da maioria
                    dos indivíduos, que geram a um tempo atrás, que bastava visitar o odontologista a cada morte de papa
                    para tudo ficar bem. Hoje em dia, sabemos que a saúde bucal está diretamente ligada à frequência aos
                    consultórios. </p>
                <p>Com plano odonto todas as famílias programam os pagamentos mensais e incluem o bem-estar na rotina. É
                    essencial prestar atenção aos tipos de trabalhos oferecidos pelas empresas, assim, você evita
                    momentos desagradáveis. Cada plano odonto tem seus benefícios e desvantagens e cabe ao consumidor se
                    informar para escolher o convênio que mais se adequa a sua realidade.</p>
                <p>Porém, tenha em mente: ter acesso a um plano odonto trará tranquilidade e segurança. Além do mais,
                    cada vez mais os atendimentos são humanizados. Não é fácil deitar na cadeira do dentista, ainda mais
                    morrendo de medo e sentindo dores insuportáveis. Dessa maneira, especialistas qualificados sabem que
                    é preciso focar também na humanização do atendimento e tratamento dental. Ao transmitir esse cuidado
                    ao beneficiário, tudo ocorrerá de forma mais rápida e menos dolorosa. </p>
                <h2>Saiba escolher um bom plano odonto que te satisfaça</h2>
                <p>O plano odonto precisa focar esse diferencial. Não se trata apenas de fornecer o melhor preço do
                    mercado, nem tampouco muitas coberturas. Lógico que os trabalhos são relevantes, afinal é que os
                    usuários buscam. Mas o tratamento dental precisa ir além e proporcionar ao paciente àquela vontade
                    de retornar ao consultório. Desde sempre os indivíduos têm medo do dentista. Algumas sentem pavor só
                    de ouvir o barulho do motorzinho ainda na recepção. Este quadro mudou bastante devido aos métodos
                    utilizados e procedimentos muito mais contemporâneos.</p>
                <p>Mas o grande responsável por essa adaptação é o odontologista. Com atendimento humano, o usuário se
                    sente seguro e relaxado. Uma boa conversa antes de começar o procedimento é importante e cria essa
                    relação de proximidade e confiança. Quanto mais as pessoas frequentarem os dentistas, menos terão
                    problemas na boca. Pense nisso e invista em um plano odonto. </p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>