<?php

$h1 = "Planos feitos para Você e para sua família";
$title = $h1;
$desc = "Conheça nossos planos. A Ideal Odonto nasceu por meio da experiência de um grupo consolidado há 20 anos no mercado, composto por 12 empresas...";
include('inc/head.php');
?>

<body>

    <?php include 'inc/header.php' ?>
    <main>
        <section class="container my-5">
            <div class="row">
                <div class="col-md-8 col-12 mb-4 mx-auto">
                    <h2 class="text-center text-uppercase">Tenha prazer em Sorrir</h2>
                    <p class="h4 text-center">Com os planos da Ideal Odonto, você tem acesso aos melhores profissionais do Brasil com carência de até 24 horas para casos emergenciais.</p>
                </div>
            </div>

            <?php include('inc/planos.php'); ?>
            

           </section>
    </main>
    
    <?php include 'inc/footer.php' ?>
    
    
    
    
</body>

</html>
