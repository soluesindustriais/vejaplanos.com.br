<?php
include('inc/vetKey.php');
$h1 = "plano de odontologia";
$title = $h1;
$desc = "Você conhece a cobertura do plano de odontologia? Aqui você compreenderá um pouco melhor sobre o assunto! Para prevenir e cuidar patologias ou";
$key = "plano,de,odontologia";
$legendaImagem = "Foto ilustrativa de plano de odontologia";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Você conhece a cobertura do plano de odontologia? Aqui você compreenderá um pouco melhor sobre o
                    assunto!</h2>
                <p>Para prevenir e cuidar patologias ou problemas orais, a contratação de um plano de odontologia é a
                    alternativa mais recomendada. Com ele, o usuário tem a possibilidade de cuidar adequadamente da
                    saúde oral por meio de inúmeros tratamentos odontológicos fornecidos. Nesse quesito, devem ser
                    avaliadas adequadamente cada uma das opções disponíveis para escolher a que ofereça o melhor
                    custo-benefício.</p>
                <p>Vale dizer que o cuidado com a saúde oral é algo que não pode ser ignorado, já que afeta o
                    funcionamento de todo o corpo. Afinal de contas, uma aparentemente inofensiva e simples dor de dente
                    pode se agravar e impedir o paciente de continuar com atividades cotidianas, como comer e trabalhar.
                    Por isso, indica-se a contratação do plano de odontologia.</p>
                <p>Nos dias de hoje, devido aos custos mais acessíveis, o plano de odontologia está se popularizando
                    entre os brasileiros. Dados mostram que mais de vinte e cinco milhões de indivíduos no Brasil já
                    possuem algum tipo de plano de odontologia. Segundo a Agência Nacional de Saúde (ANS), essa
                    porcentagem pode aumentar para 50 milhões nos próximos cinco anos. Com isso, as pessoas, cada vez
                    mais, irão procurar tratamento em clínicas conveniadas pelo plano de odontologia que contrataram.
                </p>
                <h2>Benefícios ao contratar um plano de odontologia</h2>
                <p>É essencial dizer que existem muitas vantagens para os indivíduos que desejam contratar um plano de
                    odontologia, como os preços bastante acessíveis e a ampla quantidade de cobertura dos tratamentos.
                    Dessa maneira, o cliente inicia a construir o hábito de frequentar o consultório odontológico mais
                    vezes, então torna-se mais eficiente a prevenção de problemas ou doenças bucais. </p>
                <p>Nos dias de hoje, já existem muitos tipos de plano de odontologia voltados para pessoas de diferentes
                    idades, necessidades e rendas mensais. De qualquer maneira, vale dizer que quem adere a um plano de
                    odontologia de qualidade tem acesso às seguintes vantagens:</p>
                <ul>
                    <li>
                        <p>Tratamentos preventivos e corretivos; </p>
                    </li>
                    <li>
                        <p>Cobertura em todo o território nacional;</p>
                    </li>
                    <li>
                        <p>Tempo de carência reduzido em relação aos planos de saúde em geral;</p>
                    </li>
                    <li>
                        <p>Custo-benefício mais interessante em relação às consultas em consultórios particulares; </p>
                    </li>
                    <li>
                        <p>Equipes altamente capacitadas e especializadas.</p>
                    </li>
                </ul>
                <h2>Entenda como eleger o melhor plano de odontologia</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Se em outro período grande parte das pessoas apenas ia ao especialista quando estivesse com um
                    problema muito grave, nos dias de hoje existe uma maior conscientização em relação à necessidade de
                    cuidar da saúde oral. Dessa maneira, a contratação de um plano de odontologia se tornou essencial
                    para quem se preocupa com a saúde oral e não deixa de lado o cuidado ideal com os dentes.</p>
                <p>Existem muitos tipos de plano de odontologia disponíveis no mercado, o que pode dificultar a decisão
                    do interessado em relação ao melhor convênio entre eles. Por outro lado, mostra-se essencial
                    verificar alguns critérios importantes na hora de escolher uma das assistências disponíveis, como o
                    valor do custo mensal ou anual do plano de odontologia, a quantidade de cobertura dos procedimentos
                    odontológicos, a reputação da empresa, o feedback dos clientes do plano de odontologia, a existência
                    de urgência e a emergência 24h.</p>
                <h2>Saiba qual a cobertura mínima estipulada para o plano de odontologia</h2>
                <p>Por mais que exista uma grande numeração de técnicos da odontologia no país, não são todos os
                    indivíduos que consideram ser importante buscá-los no tempo recomendado, ou seja, pelo menos duas
                    vezes por ano. Isso ocorre porque o preço das consultas e procedimentos em clínicas particulares
                    pode ser bastante elevado, então o plano de odontologia surge como uma opção com excelente
                    custo-benefício para o cliente.</p>
                <p>Nesse sentido, a cobertura de cada plano pode variar de acordo com a modalidade escolhida pelo
                    usuário. No entanto, a Agência Nacional de Saúde Suplementar (ANS) exige uma lista mínima de
                    tratamentos com os quais todos os convênios necessitam disponibilizar aos usuários, em que estão
                    incluídas as radiografias oclusais, que servem para acompanhar o progresso e desenvolvimento do
                    dente, radiografia periapical (exame de coroas, raízes e ossos dos dentes) e radiografia bite-wing,
                    cujo objetivo é ver o alinhamento entre as arcadas dentárias.</p>
                <p>Além do mais, existem também os tratamentos de nível baixo, que incluem a consulta inicial para
                    análise, aplicação de flúor, extração de tártaro, profilaxia (limpeza), restauração e procedimentos
                    de cáries; os tratamentos de nível médio, que são os curativos, suturas, periodontia (tratamento e
                    cirurgia da gengiva), colagem de fragmentos; e, por fim, todo plano de odontologia deve
                    disponibilizar os procedimentos de nível alto, incluindo a exodontia (extração dos dentes),
                    endodontia (tratamento de canal), biópsia e cirurgias de pequeno porte.</p>
                <h2>Tratamentos mais requisitados no plano de odontologia</h2>
                <p>Existem diversos tipos de tratamentos em consultório odontológico, tanto estéticos quanto preventivos
                    e corretivos. Com a ajuda da tecnologia, os tratamentos odontológicos ainda se tornaram mais
                    eficazes, rápidos e seguros. Nesse sentido, grande parte dessas operações podem ser cobertas pelo
                    plano de odontologia.</p>
                <p>Nesse sentido, os especialistas costumam disponibilizar muitos procedimentos odontológicos para que
                    os pacientes tenham dentes bonitos e saudáveis. Para isso, os tratamentos mais comuns realizados
                    diariamente pelo plano de odontologia são: </p>
                <ul>
                    <li>
                        <p>Tratamento de canal: a endodontia, também conhecida como tratamento de canal, é um
                            procedimento realizado para descontaminar o dente cuja cárie atingiu a raiz. Nesse
                            tratamento, o profissional insere agulhas finas, chamadas de limas, retira a polpa
                            contaminada, realizando a descontaminação e limpeza do canal;</p>
                    </li>
                    <li>
                        <p>Tratamento de mau hálito: antes de iniciar o tratamento, o dentista faz um diagnóstico do
                            problema bucal a partir de um questionário e conversa com o paciente, baseando-se nos seus
                            hábitos e histórico de doenças. O profissional também pode pedir exames clínicos para ver se
                            existem doenças na cavidade oral ou pouco fluxo salivar. Assim que o diagnóstico é
                            realizado, o profissional escolhe o tratamento mais indicado para o caso;</p>
                    </li>
                    <li>
                        <p>Tratamento de cárie: a cárie é uma lesão que atinge o esmalte e, possivelmente, a dentina do
                            paciente. Com o objetivo de tratar esse problema, caso a cárie tenha atingido o dente de
                            forma superficial, o dentista realiza a obturação. Nesse procedimento, o profissional remove
                            o tecido atingido e restaura o dente; </p>
                    </li>
                    <li>
                        <p>Tratamento de gengiva: a periodontia, também chamada de tratamento de gengiva, é um
                            procedimento cujo objetivo é desinflamar a região da gengiva que possivelmente contraiu
                            gengivite ou periodontite. Nesse caso, o profissional faz uma profilaxia (limpeza) geral dos
                            dentes e gengivas, removendo tártaros e placas bacterianas invisíveis a olho nu. Se a
                            gengiva estiver muito inflamada, é possível que o dentista indique o uso de antibióticos ou
                            anti-inflamatórios.</p>
                    </li>
                </ul>
                <h2>Como resolver o abscesso pelo plano de odontologia</h2>
                <p>Na hora em que uma infecção bacteriana surge e se afeta em uma área entre a gengiva e os ossos da
                    face ou do pescoço, origina-se o abscesso do dente. Dessa maneira, constata-se que uma dentição que
                    não é adequadamente solucionado pode desenvolver esse problema. Além do mais, mostra-se essencial
                    dizer que, entre as principais causas do abscesso dentário, estão a alimentação rica em açúcar e
                    higienização bucal incorreta.</p>
                <p>O abscesso dentário é analisado como um acúmulo de pus que pode surgir em diferentes partes da
                    cavidade interna da cavidade bucal. No caso do abscesso periodontal, ele aparece nas gengivas ao
                    redor da raiz de uma dentina. Já o abscesso periapical aparece na ponta da raiz do dente. Dessa
                    forma, ao notar qualquer tipo de anormalidade na região bucal, recomenda-se procurar um dentista
                    qualificado e de confiança pelo plano de odontologia para solucionar o problema.</p>
                <p>Nesse sentido, o abscesso dentário pode afetar automaticamente a qualidade de vida de um indivíduo,
                    porque os sintomas interferem nas atividades do dia a dia. Além do mais, caso não seja solucionado,
                    esse dilema ainda é capaz de desenvolver a osteomielite, que é uma infecção no osso. Por isso, é
                    importante estar atento aos sintomas mais comuns que indicam a presença do abscesso dentário, tais
                    como:</p>
                <ul>
                    <li>
                        <p>Dor de dente;</p>
                    </li>
                    <li>
                        <p>Vermelhidão na área afetada;</p>
                    </li>
                    <li>
                        <p>Sensibilidade dentária;</p>
                    </li>
                    <li>
                        <p>Drenagem de pus;</p>
                    </li>
                    <li>
                        <p>Dificuldade para morder e mastigar alimentos;</p>
                    </li>
                    <li>
                        <p>Gânglios do pescoço inchados;</p>
                    </li>
                    <li>
                        <p>Dificuldade ao abrir a boca;</p>
                    </li>
                    <li>
                        <p>Dente escurecido. </p>
                    </li>
                </ul>
                <p>O aparecimento do abscesso dentário acontece por razão de uma cárie ou devido à inflamação na
                    gengiva. Sendo assim, antes de iniciar o procedimento, o odontologista deve diagnosticar o caso do
                    usuário ao descobrir qual foi a razão específico para o aparecimento desse dilema. No caso de uma
                    cárie, o especialista deve fazer a endodontia (tratamento de canal), em que realiza a remoção do
                    tecido doente central, conhecido como polpa, e drenar o abscesso. Ele pode fazer esse tratamento ao
                    abrir um buraco no dente, com o uso de uma broca, remover a polpa, fechar o canal com cimento
                    odontológico e fechar o dente com uma coroa.</p>
                <p>Nos casos mais básicos, o odontologista apenas faz a incisão de drenagem do abscesso, abrindo um
                    corte pequeno no abscesso para extrair o pus e depois limpando a área com uma solução salina. Já nos
                    momentos mais progressivos, o especialista faz a exodontia (extração do dente), se não for possível
                    salvar a dentição, e depois fazer a drenagem na área.</p>
                <p>Quando a infecção se espalha e atinge as dentições próximas, mandíbula e outras regiões, é essencial
                    também utilizar antibióticos, para que assim seja possível conter a infecção. Nesse sentido, o
                    especialista costuma prescrever alguns antibióticos como cefalexina, flanax, amoxicilina, clordox ou
                    clindamicina. Nesse quesito, vale a pena mostrar que o abscesso dentário não se cura sozinho. Dessa
                    maneira, ao notar os principais sintomas, é essencial buscar um tratamento no consultório
                    odontológico. </p>
                <h2>Como resolver a patologia periodontal pelo plano de odontologia</h2>
                <p>A periodontia é o sub ramo da odontologia que cuida da região gengival. Até porque, a qualidade da
                    saúde oral depende dos seus componentes, como as dentições, a língua e as gengivas. Nesse último
                    caso, mostra-se essencial dar uma atenção especial à área gengival, que pode contrair doenças
                    periodontais e necessitar da realização de uma cirurgia periodontal.</p>
                <p>A patologia periodontal é analisada por infecções bacterianas severas que podem atingir a estrutura
                    das dentições e o tecido da gengiva. Capaz de atingir um ou mais dentes, essa doença ainda pode
                    causar vermelhidão e inchaço. Nesse contexto, vale destacar que estima-se que oitenta por cento dos
                    adultos apresentem algum tipo de problema na região gengival. E, em muitos desses casos, é
                    necessário fazer a cirurgia periodontal.</p>
                <p>Após identificar a presença de uma doença periodontal, o odontologista deve fazer alguma das
                    modalidades da cirurgia periodontal. Nesse quesito, existe o retalho gengival. Caso o paciente tenha
                    bolsas periodontais com mais de cinco milímetros de extensão, mostra-se importante reduzi-las. Nesse
                    quesito, muitos indivíduos com periodontite passam por esse procedimento, em que o profissional
                    corta o tecido gengival para separá-lo do dente, promovendo uma limpeza geral na região. Depois
                    disso, com a utilização de um aparelho ultrassônico e manualmente, o profissional retira o tártaro,
                    a placa e película que fica abaixo das bolsas periodontais.</p>
                <p>Existe também a gengivectomia. O foco dessa cirurgia periodontal é remover o excesso de tecido da
                    gengiva e promover uma melhoria na higienização dos dentes. Para que essa operação possa ser feita,
                    é importante aplicar uma anestesia local na região gengival e é possível realizá-la pelo plano de
                    odontologia.</p>
                <p>Outra alternativa de cirurgia periodontal é a gengivoplastia. Com o foco de remodelar o tecido
                    gengival saudável próximo as dentições, cuja meta é aprimorar sua aparência, é realizada a
                    gengivoplastia. Caso o paciente tenha retração gengival (afastamento da gengiva em relação ao
                    dente), pode ser realizada a operação. Aliás, pode ser feito também um enxerto, operação na qual
                    ocorre a retirada do tecido do palato, que posteriormente é colocado no local onde houve a retração
                    gengival.</p>
                <p>Nesse contexto, é fundamental mencionar que a doença periodontal aparece a partir do acúmulo de
                    bactérias ao redor do dente, que se multiplicam e originam a placa bacteriana. Se não for retirada,
                    o tecido gengival próximo à ela pode inflamar e causar a gengivite. De modo geral, a placa
                    bacteriana e os resíduos de comidas podem ser eliminados com costumes cotidianos de cuidado com a
                    higiene bucal, já que assim é possível fazer a limpeza da superfície dos dentes e eliminação da
                    placa bacteriana na linha da gengiva. Por outro lado, se esses componentes não forem corretamente
                    removidos, a gengivite pode se agravar e originar a periodontite. Com isso, o tecido gengival começa
                    a inflamar mais ainda, provocando sangramento e formação de bolsas entre a gengiva e o dente,
                    fazendo surgir a doença periodontal e a possibilidade de perda ou enfraquecimento dentário.</p>
                <p>Essa doença é capaz de atingir negativamente a raiz da dentina, estimulando o seu amolecimento. Nesse
                    contexto, o usuário deve se consultar rapidamente com um periodontista pelo plano de odontologia
                    para solucionar o caso, eliminando assim as bolsas periodontais, tártaros e placas bacterianas.</p>
                <h2>Como solucionar da retração gengival pelo plano de odontologia</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>A retração gengival caracteriza-se como um deslocamento da gengiva, gerando a exposição da raiz da
                    dentição. Para resolver esse problema oral, existem diferentes procedimentos e um deles é por meio
                    de cirurgia. Nesse sentido, o especialista pode realizar a cirurgia de enxerto gengival pediculado,
                    o livre ou o de tecido conjuntivo, mas a escolha depende bastante do caso do paciente.</p>
                <p>O deslocamento da posição correta da gengiva ainda pode desenvolver outros problemas orais como
                    hipersensibilidade, cáries, danificação e perda dentária. Conforme o tempo vai passando, torna-se
                    normal que a retração gengival fique ainda mais evidente e piore a situação, por isso deve ser
                    tratada quanto antes no consultório odontológico pelo plano de odontologia.</p>
                <p>Antes de realizar a cirurgia, é essencial analisar e obter um diagnóstico se o usuário realmente tem
                    a retração gengival. Caso seja constatada a presença da retração gengival, geralmente é possível
                    identificar os seguintes sintomas no paciente: </p>
                <ul>
                    <li>
                        <p>Presença de tártaro;</p>
                    </li>
                    <li>
                        <p>Hipersensibilidade;</p>
                    </li>
                    <li>
                        <p>Presença de mau hálito;</p>
                    </li>
                    <li>
                        <p>Dor de dente;</p>
                    </li>
                    <li>
                        <p>Sangramento na hora de escovar os dentes ou passar fio dental;</p>
                    </li>
                    <li>
                        <p>Exposição da raiz do dente. </p>
                    </li>
                </ul>
                <p>Dessa forma, para que o paciente fique com a gengiva na posição adequada, ele tem a alternativa de
                    fazer a cirurgia da gengiva retraída. Nesse quesito, o foco do profissional é cobrir a raiz exposta
                    do dente por meio do reposicionamento gengival ou do enxerto de gengiva.</p>
                <p>Com o enxerto gengival, o odontologista extrai um pedaço de gengiva de um local e reposiciona-o na
                    dentição onde ocorre a exposição da raiz. Essa modalidade de cirurgia da gengiva é mais usada em
                    casos de retração extensa. Já no reposicionamento gengival, o dentista descola, puxa e reposiciona a
                    gengiva que estava retraída. Por ser mais simples, esse tipo de cirurgia é recomendado para os casos
                    de retração menos extensa.</p>
                <p>Por ser um dos tipos de cirurgia da gengiva mais utilizados, mostra-se essencial mencionar a cirurgia
                    de enxerto gengival pediculado. Esse tipo de procedimento é o mais indicado para os usuários que
                    precisam cobrir uma pequena parte da gengiva, já que o tecido que tampa a raiz está localizado no
                    dente ao lado.</p>
                <p>Na cirurgia de enxerto gengival pediculado, o especialista realiza algumas incisões com o foco de
                    puxar a gengiva por meio de movimentos rotatórios. Com isso, o profissional consegue recobrir a área
                    que anteriormente estava com a raiz exposta e reposiciona a gengiva do modo certo. Dessa maneira, é
                    finalizado o procedimento e o paciente já sai do consultório com a gengiva localizada na região
                    ideal.</p>
                <h2>Como lidar com a inflamação no canal pelo plano de odontologia e como solucioná-la</h2>
                <p>Como já foi dito, a endodontia, famosa também como procedimento de canal, é um tratamento
                    odontológico amplamente realizado nos consultórios odontológicos, no qual o dentista consegue fazer
                    a limpeza do dente contaminado por uma cárie que atingiu a raiz para saber como tirar inflamação da
                    dentição. Nesse sentido, as dentições que apresentam lesões superficiais ou cavidades mais
                    profundas, chamadas também como "furos", normalmente mostram que uma cárie foi instalada. </p>
                <p>A cárie é originada a partir da proliferação de bactérias na boca, gerando assim as placas
                    bacterianas. Se essas placas não forem solucionadas, elas ainda podem se desenvolver e se
                    calcificar, gerando o aparecimento dos tártaros. No futuro, esses tártaros ainda podem desenvolver
                    outros dilemas, como a gengivite e a periodontite. Sendo assim, é muito fundamental cuidar
                    adequadamente da saúde bucal para evitar que sejam desenvolvidas patologias nesta região.</p>
                <p>Com o canal inflamado, a pessoa inicia a ter dificuldade em fazer atividades simples como a
                    mastigação, além de outros dilemas. Para constatar que o canal está inflamado, o especialista começa
                    uma análise completa da boca, perguntando também quais são os sintomas que o usuário vem sentindo, o
                    histórico de doenças e os hábitos diários. Nesse contexto, antes de saber como tirar inflamação no
                    dente, revela-se importante destacar os principais sintomas desse problema, como a dificuldade na
                    hora de abrir a boca ou mastigar, a sensibilidade na região afetada, a vermelhidão na área atingida,
                    o inchaço nos gânglios do pescoço, o dente escurecido, a drenagem de pus e a dor de dente.</p>
                <p>Com o foco de saber como cuidar da inflamação no canal do dente, retirando a cárie que chegou até a
                    raiz e contaminou o canal do dente, o especialista primeiro necessita abrir um buraco no interior da
                    dentição, com o uso de uma broca, para alcançar a dentina. Logo em seguida, ele insere limas
                    (pequenas agulhas) para retirar a polpa infeccionada. Após essa etapa, o odontologista realiza a
                    descontaminação e a limpeza do canal inflamado.</p>
                <p>Já na parte final do procedimento, o especialista insere um cimento odontológico, comumente fabricado
                    com hidróxido de cálcio, no canal aberto. Para fechar o buraco na dentição, ele costuma usar coroas
                    fechadas com resina composta ou porcelana para as dentinas frontais e ligas de prata ou ouro para os
                    dentes pré-molares e molares. Assim, o tratamento é finalizado.</p>
                <h2>Como solucionar adequadamente da saúde bucal</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Para cuidar de maneira adequada da dentição e conquistar uma saúde bucal ideal, essa região deve
                    estar livre de cáries, tártaros, gengivite e outros problemas comuns que aparecem na boca. Dessa
                    maneira, para conquistar dentições bonitas e saudáveis, primeiramente é necessário aliar escovação,
                    fio dental e antisséptico diariamente. </p>
                <p>Além do mais, mostra-se essencial levar em conta a alimentação. Uma saúde oral correta não está
                    alinhada à ingestão em excesso de alimentos açucarados ou cheios de carboidratos, que facilitam a
                    proliferação de bactérias. Por isso, recomenda-se uma alimentação saudável e equilibrada. </p>
                <p>Por fim, a visita à odontologia semestralmente por meio do plano de odontologia é outro passo
                    fundamental. Afinal de contas, apenas um odontologista capacitado é capaz de realizar o check-up
                    bucal e verificar se o paciente possui algum problema a ser tratado. Nesse caso, muitas pessoas
                    preferem contratar algum plano de odontologia para obter uma rotina de cuidado com a saúde bucal.
                </p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>