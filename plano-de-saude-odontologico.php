<?php
include('inc/vetKey.php');
$h1 = "plano de saúde odontológico";
$title = $h1;
$desc = "Plano de saúde odontológico e sinônimo de qualidade de vida! A assistência à saúde é um direito de todo indivíduo. Dessa forma, milhares de indivíduos";
$key = "plano,de,saúde,odontológico";
$legendaImagem = "Foto ilustrativa de plano de saúde odontológico";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$urlPagInterna = "informacoes";
include("inc/head.php"); ?>

<body>
    <?php include("inc/header.php"); ?>
    <?php include("inc/lp-mpi.php"); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <?php if (isset($pagInterna) && ($pagInterna != "")) {
                    $previousUrl[0] = array("title" => $pagInterna);
                } ?>
                <?php include 'inc/breadcrumb.php' ?>
            </div>
            <div class="col-12 mt-3">
                <h1 class="text-uppercase">
                    <?= $h1; ?>
                </h1>
            </div>
            <article class="col-md-9 col-12 text-black">
                <?php $quantia = 3;
                $i2 = 1;
                include('inc/gallery.php'); ?>
                <!--StartFragment-->
                <h2>Plano de saúde odontológico e sinônimo de qualidade de vida!</h2>
                <p>A assistência à saúde é um direito de todo indivíduo. Dessa forma, milhares de indivíduos sofrem no
                    Brasil pela demora no atendimento, principalmente pela baixa qualidade dos serviços fornecidos pela
                    rede pública. Nos últimos anos, desde que os trabalhos de assistência privada à saúde
                    médico-hospitalar foram desenvolvidos, os procedimentos tiveram uma expressiva melhora, garantindo
                    aos usuários acesso rápido aos atendimentos, internações, cirurgias e exames. Logo após, também
                    surgiu no mercado o plano de saúde odontológico. </p>
                <p>A preocupação com a qualidade de vida do sorriso e a saúde bucal como um todo fez com que muitos
                    pacientes passassem a procurar atendimento odontológico de qualidade. Neste contexto, o mercado de
                    planos dentais têm evoluído consideravelmente paralelo ao de planos de saúde. </p>
                <p>Entre as causas que apontam para o desenvolvimento do plano de saúde odontológico no país estão a
                    transformação no perfil do especialista e da própria realidade da profissão. Após a formatura,
                    muitos odontologistas necessitam captar pacientes e fornecer atendimento por meio de convênio
                    dentário é uma excelente alternativa no início da carreira. Outro ponto relevante para o
                    fortalecimento desta modalidade é o acesso precário da população aos serviços de saúde bucal.</p>
                <p>Antes da chegada do plano de saúde odontológico, a maioria das famílias contavam apenas com o
                    atendimento da rede pública e dos consultórios particulares. Levando em consideração o valor de uma
                    consulta particular, tornava-se praticamente inevitável não deixar os cuidados com a saúde oral em
                    segundo plano. Sem a frequência correta ao especialista, as chances de desenvolver patologias e
                    empecilhos bucais aumentam drasticamente. Resultado: um sorriso imperfeito e nada saudável.</p>
                <h2>Qual a vantagem do plano de saúde odontológico</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Ao decidir pela adesão a um plano de saúde odontológico, o primeiro passo é investigar sobre o
                    questionamento. Antes de fechar o contrato com qualquer empresa, afinal, há diversas de opções no
                    mercado, os consumidores precisam entender como funciona o serviço e quais suas principais
                    vantagens. </p>
                <p>A primeira delas, e talvez, a mais óbvia, seja a possibilidade de criar um vínculo maior com o
                    recepcionamento. Ou seja, quem não tem plano de saúde odontológico, mas tampouco não mantém as
                    consultas com um odontologista particular em dia, dificilmente terá qualidade no seu sorriso. Isso
                    porque, apesar de todos os cuidados com a limpeza em casa, é imprescindível visitar o profissional
                    pelo menos de seis em seis meses, para realizar a tão essencial manutenção da saúde bucal. </p>
                <p>Além disso, este é o ponto crucial. Na prática, o plano de saúde odontológico colocará a vida do
                    beneficiário nos trilhos. Com o abatimento das parcelas mensais ou mesmo da parcela única (anual), o
                    paciente inclui a visita ao dentista na rotina. É como se fosse uma “obrigação”. No fundo, não deixa
                    de ser, afinal, quem gostaria de pagar por um produto e não utilizá-lo? Com o plano dental ocorre
                    exatamente isso. O usuário paga pelo convênio e tem a sua disposição especialistas para fazer o
                    atendimento, que pode ser de urgência e emergência, ou apenas consultas preventivas e corretivas.
                </p>
                <p>A rotina das consultas garante que o paciente mantenha a saúde bucal, ou, em caso de problemas mais
                    sérios, a descoberta será quanto antes. Automaticamente, o procedimento também. Isso evita que
                    dentes fiquem doendo na boca por anos e anos, e a pessoa fique sem atendimento adequado, provocando
                    malefícios que certamente atingirão todo o organismo. </p>
                <h2>Plano dental e coberturas: quais os diferenciais deste trabalho</h2>
                <p>O plano de saúde odontológico cobre os principais atendimentos da área, ou seja, o básico que é
                    necessário para que a saúde oral esteja em dia é realizado nos convênios. De forma geral, as
                    empresas que prestam o trabalho tem que, obrigatoriamente, seguir o rol de procedimentos e eventos
                    em saúde da Agência Nacional de Saúde Suplementar (ANS). Neste conjunto estão inseridos exames,
                    cirurgias, consultas e demais tratamentos. Todo paciente tem direito a aproveitar após realizar os
                    pagamentos. Essa determinação é regulamentada e fiscalizada pela Lei de Planos de Saúde - Lei 9.656
                    de 1998.</p>
                <p>Contudo, apesar de obrigatório, diversos contratos de plano de saúde odontológico não cobrem todos os
                    itens. Vale mencionar que cada convênio tem suas particularidades e é o consumidor que necessita
                    estar atento no momento da contratação. Com o objetivo de melhorar ainda mais os trabalhos, a ANS,
                    por meio de uma resolução normativa, divulgada em novembro de 2017, fez uma atualização no rol de
                    tratamentos.</p>
                <p>Trata-se de uma nova cobertura, tanto para planos de assistência médica como odontológico. Hoje em
                    dia, quase 23 milhões de indivíduos usam um plano de saúde odontológico. Esta ampliação, feita a
                    cada dois anos, entrou em vigor em janeiro de 2018 - foram incluídos 18 novos tratamentos. Confira
                    na lista abaixo quais são os procedimentos simples e de nível médio oferecidos pelos convênios
                    dentários:</p>
                <ul>
                    <li>
                        <p>restaurações;</p>
                    </li>
                    <li>
                        <p>remoção de tártaro;</p>
                    </li>
                    <li>
                        <p>tratamento de cáries;</p>
                    </li>
                    <li>
                        <p>aplicação de flúor;</p>
                    </li>
                    <li>
                        <p>profilaxia (polimento coronário);</p>
                    </li>
                    <li>
                        <p>consulta inicial com dentista credenciado à rede;</p>
                    </li>
                    <li>
                        <p>limpeza e tratamento preventivo.</p>
                    </li>
                </ul>
                <p>O plano de saúde odontológico também oferece ao paciente os atendimentos de nível médio, que visam o
                    efeito corretivo. Normalmente são realizados em casos mais graves. São eles:</p>
                <ul>
                    <li>
                        <p>suturas;</p>
                    </li>
                    <li>
                        <p>curativos;</p>
                    </li>
                    <li>
                        <p>colagem de fragmentos;</p>
                    </li>
                    <li>
                        <p>periodontia (tratamento de gengiva);</p>
                    </li>
                    <li>
                        <p>endodontia (tratamento de canal).</p>
                    </li>
                </ul>
                <h2>Qualidade do sorriso e mais saúde bucal</h2>
                <p>Quem cuida das dentinas desde a infância, dificilmente vai sofrer com problemas graves. Isso porque,
                    cuidar da arcada dentária em casa, com as práticas de limpeza, não custam caro e são fáceis de
                    fazer. A indicação de todos os odontologistas é a mesma: escovar as dentições pelo menos três vezes
                    ao dia, após as refeições. E o conselho não é por acaso: essa é a rotina fundamental para que o
                    sorriso esteja limpo e refrescante. </p>
                <p>Além da escovação, as pessoas devem utilizar também o fio dental. Este recurso ajudará a eliminar os
                    resíduos de alimentos que ficam entre as dentições, especialmente os molares, que ficam no fundo da
                    boca. Passar fio dental é tão importante quanto usar a escova de dentes, mas muitas pessoas
                    esqueceram ou simplesmente acredita que não seja essencial. Mas é, e muito. Outro ponto relevante é
                    o enxaguante bucal. Este item não é obrigatório, mas auxilia e na limpeza total da boca e a manter o
                    hálito ainda mais fresco.</p>
                <p>Com estes costumes simples de limpeza, a saúde oral estará a um passo de ser conquistada. Isso
                    porque, ter uma boca livre de patologias é algo difícil, mas possível. Lembra da indicação do
                    profissional? Agora, para manter o sorriso bonito e saudável é preciso frequentar assiduamente o
                    consultório odontológico. Não existe fórmula mágica, o que existe são ações que somadas conseguem
                    garantir qualidade de vida e saúde oral aos indivíduos. </p>
                <p>Costumes essenciais para ter dentes saudáveis:</p>
                <ul>
                    <li>
                        <p>escovação diária (principalmente após as refeições);</p>
                    </li>
                    <li>
                        <p>consumir alimentos saudáveis, como frutas, legumes e verduras;</p>
                    </li>
                    <li>
                        <p>usar fio dental diariamente;</p>
                    </li>
                    <li>
                        <p>usar escova com cerdas macias;</p>
                    </li>
                    <li>
                        <p>consultar o dentista de seis em seis meses;</p>
                    </li>
                    <li>
                        <p>trocar a escova de dentes a cada três meses;</p>
                    </li>
                    <li>
                        <p>fazer bochechos;</p>
                    </li>
                    <li>
                        <p>ao primeiro sinal de problema bucal, procurar atendimento imediatamente.</p>
                    </li>
                </ul>
                <h2>Plano de saúde odontológico é fundamental</h2>
                <p>E se você pensa que não adianta investir em um plano de saúde odontológico em qualquer faixa etária,
                    está completamente enganado. A dentina é essencial em qualquer período da vida. Na infância, começam
                    os primeiros cuidados, que se bem realizados, podem determinar um sorriso saudável pelo resto da
                    vida. Contudo, nunca é tarde para tratar dos dentes. </p>
                <p>Neste sentido, investir em um convênio dental pode ser um divisor de águas quando o assunto é a
                    manutenção da saúde bucal das famílias. Os serviços odontológicos, principalmente os particulares,
                    têm fama de serem caros. E infelizmente, não se trata apenas de fama: os atendimentos não são
                    baratos mesmo, e muitas vezes, dependendo do estado em que o usuário chega ao consultório, os
                    procedimentos encarecem ainda mais o tratamento. </p>
                <p>Desta maneira, poder planejar sua ida ao dentista, sem sustos e imprevistos, faz parte da vida de
                    quem usufrui de plano de saúde odontológico. Com acesso ao convênio, o paciente passa a agendar
                    consultas com mais frequência, o que auxilia diretamente na saúde do sorriso. E como mencionamos no
                    começo deste tópico, qualquer pessoa, em qualquer idade, pode melhorar sua qualidade de vida se
                    investir na contratação de um plano dental.</p>
                <h2>Plano de saúde odontológico para crianças de todas as faixas etárias</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>A criançada gosta aprender brincando, não é mesmo? Aproveite então essa característica lúdica dos
                    pequenos para modificar a rotina e cuidados com os dentes numa grande diversão. Uma maneira muito
                    eficaz de desmitificar a figura “ruim” do dentista - criada há décadas - é frequentando o
                    consultório desde os primeiros anos de vida. Na verdade, antes mesmo do nascimento dos dentes, os
                    pais devem iniciar a higienização da gengiva dos bebês. Com massageadores específicos é possível
                    fazer uma boa limpeza após cada mamada e ainda ajudar no desenvolvimento dentário. Depois, os
                    especialistas recomendam que o ideal é que a criança marque a consulta de estreia quando fizer um
                    ano de vida.</p>
                <p>Nesse momento da vida, é ótima contar com o apoio do plano de saúde odontológico. As famílias podem
                    investir em um contrato que atenda as necessidades de todos, inclusive dos menores de idade. O
                    profissional responsável pelo atendimento na infância é o odontopediatra. </p>
                <p>Conheça algumas instruções para melhorar a rotina de saúde bucal da criançada:</p>
                <ul>
                    <li>
                        <p>não compare a dor do dente com as visitas ao dentista, são coisas diferentes. Meninos e
                            meninas precisam entender que ir ao consultório é importante para o tratamento preventivo. A
                            dor é consequência da falta de cuidados com a limpeza do sorriso e não é culpa do dentista;
                        </p>
                    </li>
                    <li>
                        <p>faça o momento da escovação ser descontraído e não transforme-o em um sacrifício para os
                            pequenos;</p>
                    </li>
                    <li>
                        <p>sempre que possível, verifique a escovação das crianças;</p>
                    </li>
                    <li>
                        <p>os pais devem orientar como segurar a escova de dentes e como devem ser feitos os movimentos;
                        </p>
                    </li>
                    <li>
                        <p>marque consultas frequentes no odontopediatra, esse cuidado irá evitar problemas bucais mais
                            sérios;</p>
                    </li>
                    <li>
                        <p>seja exemplo: os adultos têm papel relevante no desenvolvimento da saúde bucal das crianças.
                            Torne-se uma referência positiva.</p>
                    </li>
                </ul>
                <h2>O plano de saúde odontológico é ótimo para quem está na terceira idade</h2>
                <p>Em outros períodos, a remoção dentária era algo comum nos consultórios. Sem acesso a recursos
                    tecnológicos, grande parte dos profissionais preferia retirar os dentes doentes e danificados,
                    substituindo-os pelas conhecidas coroas ou dentaduras. Por isso, um grande número de idosos não tem
                    os dentes naturais, porque os mesmos não foram preservados. </p>
                <p>Felizmente, a prioridade hoje em dia é (seja para dentistas particulares como da rede pública ou
                    credenciados pelo plano de saúde odontológico) preservar a maior quantidade possível de dentinas.
                    Nem que apenas um possa ser salvo, ainda vale a pena. Nesse sentido, mesmo com uso de dentaduras,
                    próteses e os implantes dentários é possível melhorar a saúde bucal, graças a tratamentos
                    específicos e consultas especializadas.</p>
                <p>Ter um plano de saúde odontológico na terceira idade irá ajudar a diminuir as patologias comuns da
                    velhice, como periodontite (problemas da gengiva), perda dentária, a proliferação das cáries devido
                    à salivação reduzida. A falta de cuidado com a saúde oral na terceira idade pode levar ao surgimento
                    ou agravamento de diversas doenças, como a diabetes, insuficiência cardíaca ou problemas renais.
                    Além disso, para os idosos, o atendimento precisa ser diferenciado. Nesta fase da vida, a maioria
                    apresenta algum tipo de problema de saúde e dificuldades motoras. Toda atenção é necessária para que
                    a consulta seja prazerosa e o paciente responda bem ao tratamento. </p>
                <p>E ainda tem mais: senhores e senhoras que protegem seu sorriso fortalecem sua saúde mental. Uma
                    cavidade bucal saudável e bonita na terceira idade transforma a vida das pessoas mais velhas. Seja
                    dentições naturais ou dentaduras, o cuidado com a saúde bucal garante qualidade de vida, reduzindo
                    os índices de depressão nesta faixa etária. O convívio social é primordial para a saúde dos idosos,
                    mas muitos, evitam sair com os amigos ou até mesmo conviver com a família, por causa da aparência
                    dos dentes. Um plano de saúde odontológico pode transformar esse quadro, fazendo com que os
                    velhinhos sintam orgulho dos anos de experiência. Sem falar na melhoria das atividades simples do
                    dia a dia, como mastigar e sentir o sabor dos alimentos. </p>
                <p>Confira os fatores de risco que comprometem a saúde bucal na terceira idade:</p>
                <ul>
                    <li>
                        <p>osteoporose: a doença periodontal é um dos indicadores da presença de osteoporose,
                            principalmente em mulheres;</p>
                    </li>
                    <li>
                        <p>xerostomia: a boca seca é um problema sério na terceira idade, afinal, a falta de salivação
                            adequada aumenta o risco de cáries e gengivite;</p>
                    </li>
                    <li>
                        <p>envelhecimento geral: o primeiro sinal que a idade está chegando, mesmo para àqueles que tem
                            uma boa saúde bucal, é a retração das gengivas;</p>
                    </li>
                    <li>
                        <p>doenças como diabetes, hipertensão e obesidade podem comprometer a saúde bucal.</p>
                    </li>
                </ul>
                <h2>Plano de saúde odontológico para operadoras e seus funcionários</h2>
                <p>Bom para gestores, melhor ainda para colaboradores. Essa é a principal característica do plano de
                    saúde odontológico empresarial. Hoje em dia, devido à correria do mercado de trabalho, grande parte
                    dos funcionários não tem tempo para cuidar da saúde. Passam horas e horas dentro das grandes
                    empresas, indústrias e fábricas, e não esquecem de dar atenção à saúde física, mental e oral. Para
                    melhorar este cenário, muitas empresas já apostam nos planos de saúde médico-hospitalar, e outras
                    aderiram também aos convênios dentários. </p>
                <p>Normalmente, o plano de saúde odontológico para empresas possui coberturas mais amplas e tem carência
                    zero. Ou seja, o atendimento acontece em no máximo 24 horas, após o início da vigência do contrato.
                    Outro ponto positivo é a possibilidade dos trabalhadores estenderem as vantagens a seus familiares,
                    com preços acessíveis e condições facilitadas de pagamento. </p>
                <p>Para os gestores, uma equipe de funcionários que usufruem de plano de saúde odontológico é sinônimo
                    de produtividade. Quanto mais os colaboradores têm acesso a consultas preventivas, menos tempo
                    perdem cuidando de doenças e problemas sérios. Além do ganho em qualidade de vida, as consultas são
                    mais eficazes, sem a necessidade de atestados e faltas que comprometem a rotina de trabalho. </p>
                <p>O plano de saúde odontológico empresarial tem custo ainda mais acessível que as demais modalidades.
                    Isso porque, quanto maior a adesão de colaboradores, melhor será a negociação com a operadora.
                    Também é possível contratar o pagamento de várias formas. Uma das mais recorrentes é quando a
                    empresa arca com os custos integrais do tratamento dentário. Somente ficará fora do benefício, os
                    custos que o colaborador quiser fazer por conta própria. </p>
                <p>Outra opção de negociação é o pagamento 50% do empregador e 50% do funcionário - o desconto pode ser
                    feito diretamente na folha de pagamento, evitando atrasos e garantindo o atendimento sempre que
                    necessário.</p>
                <p>Vantagens do plano odontológico empresarial:</p>
                <ul>
                    <li>
                        <p>coberturas amplas;</p>
                    </li>
                    <li>
                        <p>consultas;</p>
                    </li>
                    <li>
                        <p>maioria dos planos não tem limite máximo de clientes e permite a inclusão de conjunges e
                            filhos até 24 anos;</p>
                    </li>
                    <li>
                        <p>emergência 24 horas;</p>
                    </li>
                    <li>
                        <p>restaurações;</p>
                    </li>
                    <li>
                        <p>raio-x;</p>
                    </li>
                    <li>
                        <p>extrações;</p>
                    </li>
                    <li>
                        <p>tratamentos de canais e gengivas;</p>
                    </li>
                    <li>
                        <p>preço acessível.</p>
                    </li>
                </ul>
                <p>Além de todos esses benefícios, o chefe que investe neste benefício a seus colaboradores ganha um
                    retorno que dinheiro não compra: a boa reputação no mercado. Operadoras que oferecem plano de saúde
                    odontológico e médico aos funcionários, demonstram preocupação com o bem-estar de quem veste a
                    camisa da equipe. Outro ponto positivo para as empresas é a possibilidade de dedução no imposto de
                    renda. </p>
                <h2>Investimento em saúde acima de tudo: muitos benefícios para você!</h2>
                <p><a href="<?=$url?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i++;?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"><img src="<?= $url; ?>assets/img/img-mpi/<?=$urlPagina?>-<?=$i-1;?>.jpg" class="galeria centro" alt="<?= $h1; ?>"></a>Altos índices de depressão e ansiedade estão automaticamente conectados à falta de cuidados com a
                    saúde oral. Para quem acredita que cuidar dos dentes é apenas uma questão de estética, está muito
                    enganado. Um sorriso bonito não é apenas um belo cartão de visitas, mas é responsável pela qualidade
                    de vida das pessoas. </p>
                <p>Sem o funcionamento pleno da arcada dentária, a mastigação e a fala, práticas naturais no cotidiano
                    do ser humano, ficam comprometidas. Imagine uma pessoa com dentições feias, amarelos e tortos? Este
                    perfil tem grandes chances de desenvolver apatia ao convívio social e, a consequência pode ser o
                    agravamento de quadros de depressão. </p>
                <p>Ao contrário, pessoas com sorriso bonito e saudável têm facilidade na comunicação, conseguem se
                    socializar normalmente e não tem vergonha de se expor. Mas lembre-se: quem desfila por aí com
                    sorrisão de dar inveja, provavelmente seguia à risca os cuidados com a saúde oral. Deve ter dedicado
                    (e ainda decida) alguns minutos sagrados para cada escovação, faz uso do fio dental e mais, não abre
                    mão das consultas periódicas ao dentista. </p>
                <p>O sorriso bonito e saudável pode ser a realidade de todos, até mesmo de quem ignora no período
                    infantil, na adolescência e hoje colhe o que plantou na fase adulta. E, sem dúvida, um dos passos
                    rumo ao sorriso dos sonhos é investir na contratação de um plano de saúde odontológico. </p>
                <h2>Analisar e pesquisar é o melhor caminho </h2>
                <p>Com a grande oferta de plano de saúde odontológico no mercado, o ideal é que os consumidores analisem
                    e pesquisem sobre a qualidade das operadoras antes de fechar o contrato. Dê uma vasculhada na
                    internet, nas redes sociais e converse com amigos e parentes que tem um convênio dentário.</p>
                <p>Como já dissemos acima neste artigo, há diversas vantagens na contratação deste serviço, tanto para
                    crianças, idosos, empresas, bem como para planos individuais ou familiares. Os custos também variam,
                    conforme a cobertura escolhida. É importante verificar também a abrangência territorial e a rede de
                    profissionais credenciados. </p>
                <p>Confira também os períodos de carência para utilização de cada procedimento; no caso do plano de
                    saúde odontológico empresarial, a carência é zero: uma facilidade presente também em algumas outros
                    campos. O pagamento é realizado em parcelas mensais, com cartão de crédito ou boleto bancário. O
                    plano passa a valer assim que o contrato for efetuado e a quitada a primeira parcela. </p>
                <p>Depois de muita análise e pesquisa, é chegado o momento de usar do atendimento. Agora, com acesso ao
                    plano de saúde odontológico, os pacientes podem trocar ideias com o dentista e tirar dúvidas.
                    Especialmente quando crianças fazem parte do convênio, aproveite bem as idas ao consultório para
                    pedir informações e transformar a consulta numa experiência agradável. </p>
                <p>O plano dental, além do mais, tem esse propósito: transformar o profissional em um amigo de todas as
                    horas. Quem cuida do sorriso se importa com qualidade de vida e sabe que uma boca bonita é
                    responsável pelo bem-estar social e profissional. Não abra mão desse cuidado diário. O plano é uma
                    parte fundamental do processo, mas a prevenção feita em casa, com escovação, fio dental e bochechos
                    ainda são o melhor remédio para garantir uma saúde bucal capaz de arrancar elogios. </p>
                <!--EndFragment-->

            </article>
            <?php include('inc/coluna-lateral.php'); ?>
            <?php include('inc/paginas-relacionadas.php'); ?>
            <?php include('inc/regioes.php'); ?>
            <?php include('inc/copyright.php'); ?>
        </div>
    </div>
    <?php include("inc/footer.php"); ?>
</body>

</html>