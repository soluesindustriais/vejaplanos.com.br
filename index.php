<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
$h1 = "Doutores da Web MPI";
$title = "Home";
$desc = "Invista em cuidados profissionais para sua saúde bucal, tenha os melhores serviços odontológicos.";
include("inc/head.php"); ?>

<body>
    <?php include "inc/header.php" ?>
    <?php include "widgets/carousel.php" ?>
    <main>
        <h1 class="d-none">
            <?= $nomeSite . " - " . $slogan ?>
        </h1>
        <?php include "section-4.php" ?>
        <?php include "section-5.php" ?>
        <?php include "inc/vantagens.php" ?>

    </main>
    <?php include "inc/footer.php" ?>
</body>

</html>