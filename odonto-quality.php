<?php
$h1 = "Odonto Quality";
$title = $h1;
$desc = "Conheça o plano Odonto Quality. Feito para você que quer sorrir novamente sem gastar muito. Plano odontológico completo que atende todo o Brasil";
include('inc/head.php');
$pagInterna = "Planos";
?>

<body>

    <?php include 'inc/header.php' ?>
    <main>
        <?php include('inc/vantagens.php'); ?>
        <?php include('inc/cobertura.php'); ?>

        <section class="container my-5">
            <div class="row">
                <div class="col-md-8 col-12  mx-auto">
                    <h2 class="text-center text-uppercase">CONHEÇA OS NOSSO PLANOS</h2>
                    <p class="h4 text-center">Emergência e Urgência liberada em 24 hrs para todos os planos, após confirmação de pagamento.</p>
                </div>
            </div>
            <?php include('inc/planos.php'); ?>
        </section>

    </main>

    <?php include 'inc/footer.php' ?>




</body>

</html>
