<section class="testimonial mt-3 py-5 ">
    <div class="container">
        <h2 class="text-center text-uppercase h1">Informações</h2>
        <p class="text-center">Agora você pode cuidar do seu sorriso por um preço que cabe no seu bolso!</p>


        <div class="col-md-12 mt-5">


            <div class="card-deck">


                <div class="row w-100 m-0 owl-carousel">
                    <?php $palavraEstudo_s7 = array(
                        'plano odontológico',
                        'convênio odontológico',
                        'plano dentário',
                        'plano dental',
                        'convênio odontológico que cobre aparelho',
                        'plano de saúde odontológico'
                    );

                    include 'inc/vetKey.php';
                    asort($vetKey);
                    foreach ($vetKey as $key => $vetor) {

                        if (in_array(strtolower($vetor['key']), $palavraEstudo_s7)) { ?>

                    <div class='p-0 mb-md-4'>
                        <a href='<?= $vetor['url']; ?>' title='<?= $vetor['key']; ?>'>
                            <div class='card'>
                                <img class='card-img-top'
                                    src='<?= $url; ?>assets/img/img-mpi/350x350/<?= $vetor['url']; ?>-1.jpg'
                                    alt='<?= $vetor['key']; ?>' title='<?= $vetor['key']; ?>'>

                                <div class='card-footer p-1 d-flex justify-content-center text-center align-items-center'
                                    style='height:50px'>
                                    <h2 class='h4 text-uppercase m-0'>
                                        <?= $vetor['key']; ?>
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>

                    <?php }
                    }     ?>
                </div>
            </div>

        </div>

    </div>
</section>